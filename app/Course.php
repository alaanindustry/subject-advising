<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

	protected $fillable = [
        'name', 'description','created_at', 'udpated_at'
    ];

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }
}
