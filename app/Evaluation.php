<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = [
        'student_id','teacher_id', 'q_type_id','questionaire_id','rate','sem_id','batch_id','strengths','weaknesses','recommendation','created_at','updated_at'
    ];
}
