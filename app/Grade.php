<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
	protected $fillable = [
        'school_id','equivalent','subject_id','batch_id','year_id','sem_id','grade','status','created_at','updated_at'
    ];
}
