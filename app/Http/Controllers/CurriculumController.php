<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Course;
use App\Subjects;
use App\Prerequisite;

class CurriculumController extends Controller
{

    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\Student']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.curriculums.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Courses\Store $request)
    {
        Course::create([

        'name' => $request->name,
        'description' => $request->description

        ]);

        return $this->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);

        return $course;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Courses\Store $request, $id)
    {
        $course = Course::find($id);
        $course->name = $request->name;
        $course->description = $request->description;
        $course->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::find($id)->delete();
    }

    public function all()
    {
        $courses = Course::paginate(10);
        return $courses;
    }

    public function subjects($course_id)
    {
        $course = Course::find($course_id);
        $course->subjects;
        $subjects = array();

        for ($i=1; $i <= 4; $i++) {
            $subjects_by_year = array();

            $subjects_by_sem = array();
            for ($j=1; $j <= 2; $j++) {
                $subjects_by_sem[$j-1]['data'] = $course->subjects()->where('year_id',$i)->where('sem_id',$j)->get();
                foreach ($subjects_by_sem[$j-1]['data'] as $key => $value) {
                    $value->prerequisites = Prerequisite::leftJoin('subjects as subjects','subjects.id','=','prerequisites.subject_id')->leftJoin('subjects as prerequisite','prerequisites.prerequisite','=','prerequisite.id')->select(\DB::raw('prerequisite.course_no'))->where('subjects.id',$value->id)->get();
                }
                $subjects_by_sem[$j-1]['sem'] = $j.' Semester';
            }
            
            $subjects_by_year['year'] = $i." Year";
            $subjects_by_year['data'] = $subjects_by_sem;
            $subjects[] = $subjects_by_year;
        }
        
        return compact('course', 'subjects');
    }
}
