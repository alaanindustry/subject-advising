<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// Models;
use App\User;
use App\SParent;
use App\Setting;
use App\Student;
use App\Course;

class GuestController extends Controller
{

	public function iloveyoumona($teacher_id,$batch_id,$sem_id)
	{
		$evaluations = \DB::select('select count(*) as count from evaluations where student_id=? and batch_id = ? and sem_id = ? and teacher_id = ?;',[\Auth::user()->id,$batch_id,$sem_id,$teacher_id])[0];

		if ($evaluations->count>0) {
			$qtypes = QType::all();
			foreach ($qtypes as $key => $value) {
				$value->questionaires;
				foreach ($value->questionaires as $key1 => $value1) {
					$value1->answer = $value1->leftJoin('evaluations','evaluations.questionaire_id','=','questionaires.id')->select(\DB::raw('evaluations.strengths,evaluations.weaknesses,evaluations.recommendation,evaluations.rate'))->where('evaluations.questionaire_id',$value1->id)->where('evaluations.student_id',\Auth::user()->id)->where('evaluations.teacher_id',$teacher_id)->where('evaluations.batch_id',$batch_id)->where('evaluations.sem_id',$sem_id)->first();
				}
			}
			return $qtypes;
		}else{
			return "no";
		}
	}

	public function register(Requests\Guest\Store $request)
	{
		try {

			\DB::beginTransaction();

			$user = User::create([

				'role' => 'student',
				'username' => $request->username,
				'password' => bcrypt($request->password)

				]);

			$student = Student::create([

				'school_id' => $request->school_id,
				'firstname' => $request->firstname,
				'middlename' => $request->middlename,
				'lastname' => $request->lastname,
				'status' => $request->status,
				'birthdate' => $request->birthdate,
				'email' => $request->email,
				'sex' => $request->sex,
				'contact' => $request->contact,
				'address' => $request->address,
				'user_id' => $user->id,
				'course_id' => $request->course_id,
				'batch_id' => $request->batch_id,
				'year_level' => $request->year_level

				]);

			$parent = SParent::create([

				'firstname' => $request->parent_firstname,
				'lastname' => $request->parent_lastname,
				'address' => $request->parent_address,
				'contact' => $request->parent_contact,
				'student_id' => $student->id

				]);

			\DB::commit();
			$courses = Course::all();
			return view('auth.register',['success'=>'1','courses'=>$courses,'username'=>$request->username,'password'=>$request->password]);
		} catch (Exception $e) {

			\DB::rollBack();
		}
	}
}
