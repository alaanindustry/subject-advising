<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

// Models
use App\Student;
use App\SParent;
use App\Grade;
use App\Course;
use App\Subject;
use App\Prerequisite;
use App\User;

class StudentController extends Controller
{
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\Student']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        // $grades = Grade::where("course_no","")->get();

        // foreach ($grades as $key => $value) {
        //     $value->course_no = Subject::find($value->subject_id)->course_no;
        //     $value->save();
        // }

        return view('layouts.students.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Student::where('school_id',$id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::leftjoin('parents','students.id','=','parents.student_id')->select(\DB::raw('students.id,students.school_id,students.profile_picture,students.firstname,students.middlename,students.lastname,students.sex,students.status,students.birthdate,students.contact,students.address,students.email,parents.firstname as parent_firstname,parents.lastname as parent_lastname,parents.contact as parent_contact,parents.address as parent_address'))->where('students.id',$id)->first();
        $birthdate = explode('-', $students->birthdate);
        $students->age = Carbon::createFromDate($birthdate[0], $birthdate[1], $birthdate[2])->age;
        return $students;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Student\Update $request, $id)
    {
        $student = Student::find($id);

        $student->sparent;

        $student->school_id = $request->school_id;
        $student->firstname = $request->firstname;
        $student->middlename = $request->middlename;
        $student->lastname = $request->lastname;
        $student->status = $request->status;
        $student->birthdate = $request->birthdate;
        $student->sex = $request->sex;
        $student->contact = $request->contact;
        $student->email = $request->email;
        $student->address = $request->address;
        $student->sparent->firstname = $request->parent_firstname;
        $student->sparent->lastname = $request->parent_lastname;
        $student->sparent->contact = $request->parent_contact;
        $student->sparent->address = $request->parent_address;

        $student->save();
        $student->sparent->save();

        return $this->all();
    }

    public function updateCredential(Requests\Student\UpdateCredential $request)
    {
        $user = User::find($request->id);

        if ($request->password=="") {
            $user->username = $request->username;
            $user->save();
        }else{
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->save();
        }

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $user = User::where('id',$student->user_id);
        $student->delete();
        $user->delete();
    }

    public function all($search="")
    {
        if ($search=="") {
            $students = Student::leftJoin('courses','students.course_id','=','courses.id')->select(\DB::raw('students.id,students.school_id,students.user_id,students.firstname,students.middlename,students.year_level,students.lastname,students.contact,students.email,courses.name,students.course_id'))->orderBy('students.lastname')->paginate(10);
            return $students;
        }

        $students = Student::leftJoin('courses','students.course_id','=','courses.id')->select(\DB::raw('students.id,students.school_id,students.user_id,students.firstname,students.middlename,students.year_level,students.lastname,students.contact,students.email,courses.name,students.course_id'))->where('students.school_id', 'like', '%'.$search.'%')->orWhere('students.firstname', 'like', '%'.$search.'%')->orWhere('students.lastname', 'like', '%'.$search.'%')->orWhere('students.contact', 'like', '%'.$search.'%')->orWhere('students.email', 'like', '%'.$search.'%')->orWhere('courses.name', 'like', '%'.$search.'%')->orWhere(\DB::raw('concat(students.firstname," ",students.middlename," ",students.lastname)'),'like','%'.$search.'%')->orWhere(\DB::raw('concat(students.firstname," ",students.lastname," ",students.middlename)'),'like','%'.$search.'%')->orderBy('students.lastname')->paginate(10);
            return $students;

    }

    public function getUser($user_id)
    {
        return User::find($user_id);
    }

    public function getByCourse($course_id)
    {
        $students = Student::where('course_id',$course_id)->get();
        return $students;
    }

    public function grades($year_id,$sem_id,$student_id,$course_id)
    {
        
        $student = Student::where('school_id',$student_id)->first();

        $batch_id = $student->batch_id;

        for ($i=1; $i < $year_id; $i++) { 
            $batch_id++;
        }

        $grades = array();

        $grade_year = array();

        $grade_school_year = \DB::select('SELECT batch_id FROM `grades` where school_id = ? and year_id = ? and sem_id = ? group by batch_id',[$student_id,$year_id,$sem_id]);


        // foreach ($grade_school_year as $key => $value) {

        //     $existence = \DB::select('select * from grades where batch_id = ? and sem_id = ? and school_id = ?',[$value->batch_id,$sem_id,$student_id]);

        //     if (count($existence)==0) {
        //         $grade_year = $value;
        //     }
        // }


        

        foreach ($grade_school_year as $key => $value) {

            $grade_per_school_year = \DB::select('select a.school_id, a.course_no, a.batch_id, a.year_id, a.sem_id, c.title, a.equivalent,c.units from grades a left join students b on a.school_id = b.school_id left join subjects c on a.course_no = c.course_no where a.school_id = ? and a.sem_id = ? and a.batch_id = ? group by course_no;',[$student_id,$sem_id,$value->batch_id]);

            foreach ($grade_per_school_year as $key => $value) {

                $now = Carbon::now();
                $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
                
                if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
                    $value->equivalent = 5;
                }
            }

            $total_units = 0;

            foreach ($grade_per_school_year as $key1 => $value1) {
                $total_units+=$value1->units;
            }

            $sum_grade = 0;
            $s = "";
            $t = "";

            foreach ($grade_per_school_year as $key1 => $value1) {

                
                $sum_grade+=($value1->equivalent*$value1->units);

                $s.=" ".($value1->equivalent*$value1->units);
                $t.=" ".($value1->units);

                if ($year_id<$value1->year_id || $sem_id<$value1->sem_id) {

                    $value1->advance = "Yes";

                }else{
                    $value1->advance = "No";
                }

            }

            // dd($t);

            $average_gpa = $sum_grade/$total_units;

            // dd($total_units);

            $grades_per_school = $grade_per_school_year;

            $objects = ["gpa"=>number_format($average_gpa,3),"grades"=>$grades_per_school];

            $grades[$value->batch_id]= $objects;


        }

        // $grades_one = \DB::select('select a.id as student_id, a.user_id, a.school_id, b.equivalent, a.firstname, a.middlename, a.lastname, b.subject_id, b.grade, b.status, b.batch_id, c.course_id, c.title,c .year_id,c.sem_id, c.id as subject_id,c.course_no from students a left join grades b on a.school_id = b.school_id left join subjects c on b.subject_id = c.id where a.school_id = ? and b.batch_id = ? and b.sem_id = ?',[$student_id,$batch_id,$sem_id]);

        // if (count($grades_one)>0) {
        //     $grades_one = \DB::select('select a.id as student_id, a.user_id, a.school_id, b.equivalent, a.firstname, a.middlename, a.lastname, b.subject_id, b.grade, b.status, b.batch_id, c.course_id, c.title,c .year_id,c.sem_id, c.id as subject_id,c.course_no from students a left join grades b on a.school_id = b.school_id left join subjects c on b.subject_id = c.id where a.school_id = ? and b.year_id = ? and b.sem_id = ?',[$student_id,$batch_id,$year_id,$sem_id]);
        // }

        // $grades = array();

        // foreach ($grades_one as $key => $value) {
        //     if ($value->batch_id == $batch_id || $value->year_id == $year_id) {
        //         $grades[] = $value;
        //     }
        // }

        
        // $total_gpa = 0;
        // $total_units = 0;

        // $check=0;
        // foreach ($grades as $key => $value) {
        //     $subject_match = Subject::where('course_no',$value->course_no)->where('sem_id',$sem_id)->first();
        //     if (is_null($subject_match)) {
        //         continue;
        //     }

        //     $subject = array();
        //     if (count($subject_match)>0) {
        //         $subject = Subject::where('course_no',$subject_match->course_no)->where('sem_id',$sem_id)->first();
        //     }

        //     if ($value->equivalent!=null&&$value->equivalent!=0&&$value->equivalent!="") {
        //         $total_units+=$subject->units;
        //         $total_gpa+=($value->equivalent*$subject->units);
        //     }
        // }

        // if ($total_units==0) {
        //     $total_units = 1;
        // }


        // foreach ($grades as $key => $value) {

        //     foreach ($value as $key1 => $value1) {
        //         if ($year_id<$value1->year_id || $sem_id<$value1->sem_id) {

        //             $value1->advance = "Yes";

        //         }else{
        //             $value1->advance = "No";
        //         }
        //     }
                
        // }

        // $average_gpa = $total_gpa/$total_units;
        // $average_gpa = number_format($average_gpa,3);
        // $student = Student::where('school_id',$student_id)->first();
        return compact('grades', 'student','average_gpa');
    }

    public function importGrades(Requests\Student\Import $request)
    {
        $full_path = 'public\\storage\\public\\'.$request->grade_file;

        $excel = [];
        Excel::load($full_path, function($reader) use (&$excel){

            $objExcel = $reader->getExcel();
            $sheet = $objExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            //  Loop through each row of the worksheet in turn
            for ($row = 1; $row <= $highestRow; $row++)
            {
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    NULL, TRUE, FALSE);

                if ($rowData[0][0]>0&&$rowData[0][2]!=null) {
                    $excel[] = $rowData[0];
                }
            }
        });
        foreach ($excel as $key => $value) {
            $grade = Grade::where('school_id',$value[2])->where('subject_id',$request->subject_id)->where('batch_id',$request->batch_id)->where('year_id',$request->year_id)->where('sem_id',$request->sem_id)->first();

            if ($value[11]=="") {
                continue;
            }

            $value[11] = strtolower($value[11][0])=='i'?4:$value[11];

            if ((strtolower($value[11][0])!="n")&&(strtolower($value[11][0])!="d")) {
                if (count($grade)==0) {
                    Grade::create([
                        'school_id' => $value[2],
                        'subject_id' => $request->subject_id,
                        'batch_id' => $request->batch_id,
                        'year_id' => $request->year_id,
                        'sem_id' => $request->sem_id,
                        'grade' => $value[10],
                        'equivalent' => $value[11]
                    ]);
                }else{
                    $grade->grade = $value[10];
                    $grade->equivalent = $value[11];
                    $grade->save();
                }
            }
        }
        return $excel;
    }

    public function uploadGrade(Requests\Student\Upload $request)
    {
        $grade_file = $request->grade_file;
        $file_id = uniqid();
        $path = 'grades\\'.$grade_file->getClientOriginalName().'_'.uniqid().'.'. $grade_file->getClientOriginalExtension();
        \Storage::put('public\\'.$path, \Illuminate\Support\Facades\File::get($grade_file));

        return $path;
    }

    public function getEquivalent($grades)
    {

        foreach ($grades as $key => $value) {
            if ($value->grade==null) {
                $value->equivalent = "N/A";
            }else if($value->grade>97&&$value->grade<101){
                $value->equivalent = "1";
            }else if($value->grade>94){
                $value->equivalent = "1.25";
            }else if($value->grade>91){
                $value->equivalent = "1.5";
            }else if($value->grade>88){
                $value->equivalent = "1.75";
            }else if($value->grade>85){
                $value->equivalent = "2.0";
            }else if($value->grade>82){
                $value->equivalent = "2.25";
            }else if($value->grade>79){
                $value->equivalent = "2.5";
            }else if($value->grade>76){
                $value->equivalent = "2.75";
            }else if($value->grade>74){
                $value->equivalent = "3.0";
            }else if($value->grade>71){
                $value->equivalent = "4.0";
            }else if($value->grade<71){
                $value->equivalent = "5.0";
            }else{
                $value->equivalent = "INC";
            }
        }

        return $grades;
    }

    public function getAllSubjectsByCourse($course_id)
    {
        $subjects = Subject::where('subjects.course_id',$course_id)->get();

        foreach ($subjects as $key => $value) {
            $prerequisites = array();
            $prerequisites = Prerequisite::leftJoin('subjects','subjects.id','=','prerequisites.prerequisite')->select(\DB::raw('subjects.id,subjects.course_no,subjects.rest,year_id'))->where('subject_id',$value->id)->get();
            $value->prerequisites = $prerequisites;
        }
        return $subjects;
    }

    public function availableSubjects($school_id,$course_id)
    {
        $subjects = $this->getAllSubjectsByCourse($course_id);

        $grades = \DB::select('select school_id,subject_id,grade,equivalent,batch_id,sem_id from grades where school_id = ?',[$school_id]);

        foreach ($grades as $key => $value) {
            
            $now = Carbon::now();
            $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
            
            if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
                $value->equivalent = 5;
            }
        }
        
        $available_subjects = array();
        $re_enroll_subject = array();

        foreach ($subjects as $key => $subject) {

            $counter = 0;
            $subject->re_enroll = false;
            foreach ($grades as $key1 => $grade) {
                $subject_match = Subject::find($grade->subject_id);
                if ($subject->course_no==$subject_match->course_no) {

                    if ($grade->equivalent<=3.0) {
                        $counter++;
                    }else{
                        $subject->re_enroll = true;
                    }
                }
            }

            if (count($subject->prerequisites)>0) {

                foreach ($subject->prerequisites as $key => $prerequisite) {

                    $isfound = false;
                    foreach ($grades as $key => $grade) {
                        $subject_match = Subject::find($grade->subject_id);
                        if ($prerequisite->course_no==$subject_match->course_no) {
                            if ($grade->equivalent>3.0) {
                                $counter++;
                                $subject->re_enroll = true;
                            }else{
                                $isfound = true;
                            }
                        }
                    }
                    if ($prerequisite->rest!=null&&$prerequisite->rest!=""&&$prerequisite->rest!=0) {
                        $total_passed_grades = 0;

                        $total_subjects_count = \DB::select('select count(*) as count from subjects where year_id < ?',[$prerequisite->year_id]);

                        foreach ($grades as $key => $grade) {
                            if ($grade->equivalent<=3.0) {
                                $total_passed_grades++;
                            }
                        }
                        if ($total_passed_grades!=$total_subjects_count[0]->count) {
                            $counter++;
                        }
                    }
                    if ($isfound==false) {
                        $counter++;
                    }
                }
            }
            
            if ($subject->rest!=null&&$subject->rest!=""&&$subject->rest!=0) {

                $total_passed_grades = 0;

                $total_subjects_count = \DB::select('select count(*) as count from subjects where year_id < ?',[$subject->year_id]);

                foreach ($grades as $key => $grade) {
                    if ($grade->equivalent<=3.0) {
                        $total_passed_grades++;
                    }
                }
                if ($total_passed_grades!=$total_subjects_count[0]->count) {
                    $counter++;
                    $subject->re_enroll = true;
                }
            }

            if ($counter==0) {
                $available_subjects[] = $subject;
            }
        }

        $is_adviced_to_shift = false;
        $failing_units = 0;
        $failed_subjects = array();

        foreach ($subjects as $key => $subject) {
            foreach ($grades as $key1 => $grade) {
                $subject_match = Subject::find($grade->subject_id);
                if ($subject->course_no==$subject_match->course_no) {
                    if ($grade->equivalent<5.0&&$grade->equivalent>4.0) {
                        $subject->grade = $grade->grade;
                        $failed_subjects[] = $subject;
                        $failing_units+=$subject->units;
                    }
                }
            }
        }

        if ($failing_units>8) {
            $is_adviced_to_shift = true;
        }

        $final_available_subjects = [];

        $lowest_year = 4;
        $lowest_sem = 2;

        foreach ($available_subjects as $key => $value) {
            if ($value->year_id < $lowest_year) {
                $lowest_year = $value->year_id;
            }
        }

        foreach ($available_subjects as $key => $value) {
            if ($value->year_id == $lowest_year) {
                if ($value->sem_id<$lowest_sem) {
                    $lowest_sem = $value->sem_id;
                }
            }
        }

        foreach ($available_subjects as $key => $value) {

            if ($value->sem_id == $lowest_sem) {
                $final_available_subjects[] = $value;
            }
        }



        $student = Student::where('school_id',$school_id)->first();

        return compact('is_adviced_to_shift','final_available_subjects','failed_subjects','student');
    }

    public function sendMessage(Request $request)
    {
        $fields = array();
        $fields["api"] = $request->api;
        $fields["number"] = $request->number; //safe use 63
        $fields["message"] = $request->message;
        $fields["from"] = 'DNSC-Education';
        $fields_string = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.semaphore.co/api/sms" );
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        curl_close($ch);
    }

    public function sendBulkMessage(Request $request)
    {
        foreach ($request->sendTo as $key => $value) {
            $fields = array();
            $fields["api"] = $request->api;
            $fields["number"] = $value; //safe use 63
            $fields["message"] = $request->message;
            $fields["from"] = 'DNSC-Education';
            $fields_string = http_build_query($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.semaphore.co/api/sms" );
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $output = curl_exec($ch);
            curl_close($ch);
        }
        
    }

    // public function getGPA($student_id,$batch_id,$sem_id)
    // {
    //     $student = Student::where('school_id',$student_id)->first();

    //     $batch_id = $student->batch_id;

    //     for ($i=1; $i < $year_id; $i++) { 
    //         $batch_id++;
    //     }

    //     $grades = array();

    //     $grade_year = array();

    //     $grade_school_year = \DB::select('SELECT batch_id FROM `grades` where school_id = ? and year_id = ? and sem_id = ? group by batch_id',[$student_id,$year_id,$sem_id]);

    //     foreach ($grade_school_year as $key => $value) {

    //         $grade_per_school_year = \DB::select('select a.school_id, a.course_no, a.batch_id, a.year_id, a.sem_id, c.title, a.equivalent,c.units from grades a left join students b on a.school_id = b.school_id left join subjects c on a.course_no = c.course_no where a.school_id = ? and a.sem_id = ? and a.batch_id = ? group by course_no;',[$student_id,$sem_id,$value->batch_id]);

    //         foreach ($grade_per_school_year as $key => $value) {

    //             $now = Carbon::now();
    //             $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
                
    //             if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
    //                 $value->equivalent = 5;
    //             }
    //         }

    //         $total_units = 0;

    //         foreach ($grade_per_school_year as $key1 => $value1) {
    //             $total_units+=$value1->units;
    //         }

    //         $sum_grade = 0;
    //         $s = "";
    //         $t = "";

    //         foreach ($grade_per_school_year as $key1 => $value1) {

                
    //             $sum_grade+=($value1->equivalent*$value1->units);

    //             $s.=" ".($value1->equivalent*$value1->units);
    //             $t.=" ".($value1->units);

    //             if ($year_id<$value1->year_id || $sem_id<$value1->sem_id) {

    //                 $value1->advance = "Yes";

    //             }else{
    //                 $value1->advance = "No";
    //             }

    //         }

    //         $average_gpa = $sum_grade/$total_units;

    //         $grades_per_school = $grade_per_school_year;

    //         $objects = ["gpa"=>number_format($average_gpa,3),"grades"=>$grades_per_school];

    //         $grades[$value->batch_id]= $objects;


    //     }
    //     return compact('grades', 'student','average_gpa');
    // }

    public function deansLister($batch_id = "",$sem_id = "")
    {
        $deanslisters['first'] = array();
        $deanslisters['second'] = array();

        $students= \DB::select('select a.firstname,a.middlename,a.lastname,a.school_id,b.batch_id,d.name from students a left join grades b on a.school_id = b.school_id left join subjects c on b.subject_id = c.id left join courses d on a.course_id = d.id where b.equivalent <=2.5 group by school_id having SUM(c.units) > 17');

        foreach ($students as $key => $value) {



            $units = \DB::select('select sum(units) as total_units from (select b.units from grades a left join subjects b on a.subject_id = b.id where a.batch_id = ? and a.sem_id = ? and a.school_id = ? group by b.course_no) as a',[$batch_id,$sem_id,$value->school_id])[0];
            

            $grades = \DB::select('select a.equivalent,b.units from grades a left join subjects b on a.subject_id = b.id where a.batch_id = ? and a.sem_id = ? and a.school_id = ? group by b.course_no',[$batch_id,$sem_id,$value->school_id]);

            if ($units->total_units >= 18) {

                $sum_grades = 0;
                $s="";
                $t="";
                $average_gpa = 0;

                foreach ($grades as $key1 => $value1) {

                    $s+=($value1->equivalent * $value1->units);
                    $t+=$value1->units;
                    $sum_grades += ($value1->equivalent * $value1->units);
                }
                $average_gpa = number_format($sum_grades/$units->total_units,3);

                $year_level = $batch_id+1 - ($value->batch_id); 

                if ($average_gpa<=1.5) {
                    $value->gpa = $average_gpa;
                    $value->year = $year_level;
                    $value->sem = $sem_id;
                    $deanslisters['first'][] = $value;
                }else if($average_gpa<=1.75){
                    $value->gpa = $average_gpa;
                    $value->year = $year_level;
                    $value->sem = $sem_id;
                    $deanslisters['second'][] = $value;
                }

            }
        }

        return $deanslisters;
    }

    public function deanslisterPrint($batch_id,$sem_id)
    {
        $deanslister =  $this->deansLister($batch_id,$sem_id);
        return view('layouts.documents.deans_list',['deanslisters'=>$deanslister,'batch'=>$batch_id,'sem'=>$sem_id]);
    }

    public function numberToWord($number)
    {
        $words = ["First","Second","Third","Fourth"];

        return $words[$number];
    }

    public function printGrade($student_id,$course_id)
    {

        $student = Student::leftJoin('courses','students.course_id','=','courses.id')->where('school_id',$student_id)->first();

        $batch_id = $student->batch_id;

        $student_grades = array();

        for ($i=$batch_id; $i < $batch_id+4; $i++) {

            $per_sem_grade = array();
            $per_grades = array();

            for ($j=1; $j <=2 ; $j++) { 

                $sem_grades = array();  

                $sem_grades['data'] = \DB::select('select a.id as student_id, a.user_id, a.school_id, b.equivalent, a.firstname, a.middlename, a.lastname, b.subject_id, b.grade, b.status, b.batch_id, c.course_id, c.title,c .year_id,c.sem_id, c.id as subject_id,c.course_no from students a left join grades b on a.school_id = b.school_id left join subjects c on b.subject_id = c.id where a.school_id = ? and b.batch_id = ? and b.sem_id = ?',[$student_id,$i,$j]);

                //         $sem_grades['data'] = \DB::select('select a.course_id,a.title,a.year_id,a.sem_id,a.id as subject_id,a.course_no,b.school_id,b.firstname,b.middlename,b.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from subjects a left join (select a.id as student_id,a.user_id,a.school_id,a.firstname,a.middlename, a.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from students a left join grades b on a.school_id = b.school_id where a.school_id = ?) b on a.id = b.subject_id left join courses as c on a.course_id = c.id where a.year_id = ? and a.sem_id = ? and course_id=?;',[$student_id,$i,$j,$course_id]);



                // $grades = array();

                // foreach ($sem_grades as $key => $value) {
                //     if ($value->batch_id == $batch_id) {
                //         $grades[] = $value;
                //     }
                // }

                // foreach ($sem_grades as $key => $value) {

                //     $now = Carbon::now();
                //     $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
                    
                //     if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
                //         $value->equivalent = 5;
                //     }
                // }

                $sem_grades['sem'] = $j." Semester";
                $per_grades[] = $sem_grades;

            }


            $per_sem_grade['year'] = $i." Year";
            $per_sem_grade['data'] = $per_grades;
            $student_grades[] = $per_sem_grade;
            
        }

        


        // $student = Student::leftJoin('courses','students.course_id','=','courses.id')->where('school_id',$student_id)->first();
        // $student_grades = array();
        // for ($i=1; $i <= 4; $i++) {
        //     $per_sem_grade = array();
        //     $per_grades = array();
        //     for ($j=1; $j <= 2; $j++) {

        //         $sem_grades = array();  

        //         $sem_grades['data'] = \DB::select('select a.course_id,a.title,a.year_id,a.sem_id,a.id as subject_id,a.course_no,b.school_id,b.firstname,b.middlename,b.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from subjects a left join (select a.id as student_id,a.user_id,a.school_id,a.firstname,a.middlename, a.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from students a left join grades b on a.school_id = b.school_id where a.school_id = ?) b on a.id = b.subject_id left join courses as c on a.course_id = c.id where a.year_id = ? and a.sem_id = ? and course_id=?;',[$student_id,$i,$j,$course_id]);

        //         foreach ($sem_grades['data'] as $key => $value) {
            
        //             $now = Carbon::now();
        //             $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
                    
        //             if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
        //                 $value->equivalent = 5;
        //             }
        //         }

        //         $universal = \DB::select('SELECT * FROM `grades` a left join subjects b on a.subject_id = b.id where a.school_id = ? and a.year_id = ? and a.sem_id=?',[$student_id,$i,$j]);

        //         foreach ($universal as $key => $value) {
        //             foreach ($sem_grades['data'] as $key1 => $value1) {
        //                 if ($value->course_no==$value1->course_no) {
        //                     if ($value->equivalent!=null&&$value->equivalent!="") {
        //                         $value1->equivalent = $value->equivalent;
        //                     }
        //                 }
        //             }
        //         }
        //         $sem_grades['sem'] = $j." Semester";
        //         $per_grades[] = $sem_grades;
        //     }

        //     $per_sem_grade['year'] = $i." Year";
        //     $per_sem_grade['data'] = $per_grades;
        //     $student_grades[] = $per_sem_grade;
        // }
        return view('layouts.documents.student_grades',['student_grades' => $student_grades,'student'=>$student]);
    }

    public function getGrades($year_id)
    {
        $grades = \DB::select('select a.school_id,lastname,firstname,middlename,b.subject_id,b.year_id,b.sem_id,b.grade,b.equivalent from students a
left join (select * from grades where year_id = 1 and sem_id = 1 and subject_id = 12) b on a.school_id = b.school_id;');
    }

    public function printAllGrade($year_id,$sem_id,$subject_id,$course_id)
    {
        $subject = Subject::find($subject_id);

        $grades = \DB::select('select * from (select c.school_id,c.id as student_id, c.user_id, c.firstname, c.middlename, c.lastname,a.equivalent, a.grade, a.status, a.batch_id,b.course_id, b.title,b .year_id,b.sem_id, b.id as subject_id,b.course_no from grades a left join subjects b on a.course_no = b.course_no left join students c on a.school_id = c.school_id where a.batch_id = ? and b.course_id = ? and a.sem_id = ? and a.course_no = ?) a where a.school_id is not null order by a.lastname',[$year_id,$course_id,$sem_id,$subject->course_no]);


//         $grades = \DB::select('select a.school_id,lastname,firstname,middlename,b.grade,b.equivalent,b.course_no,b.batch_id,b.sem_id from students a
// left join (select a.school_id,a.grade,a.subject_id,a.year_id,a.equivalent,a.batch_id,a.sem_id,b.course_no,b.title from grades a left join subjects b on a.subject_id = b.id where a.sem_id = ? and a.subject_id = ? and a.batch_id= ? ) b on a.school_id = b.school_id where a.course_id = ?;',[$sem_id,$subject_id,$year_id,$course_id]);
        foreach ($grades as $key => $value) {
            
            $now = Carbon::now();
            $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
            
            if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
                $value->equivalent = 5;
            }
        }
        return $grades;
    }

    public function printAllGrades($year_id,$sem_id,$subject_id,$course_id)
    {
        $course = Course::find($course_id);
        $subject = Subject::find($subject_id);

        $grades = $this->printAllGrade($year_id,$sem_id,$subject_id,$course_id);
        return view('layouts.documents.students_grades',['grades'=> $grades,'year_id'=>$year_id,'sem_id'=> $sem_id,'subject'=>$subject->title,'course'=>$course->name]);
    }
}
