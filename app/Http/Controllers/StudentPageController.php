<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;
use App\QType;
use App\Questionaire;
use App\Evaluation;
use App\User;
use App\Course;
use App\SParent;
use App\Teacher;
use App\Subject;
use Carbon\Carbon;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;


class StudentPageController extends Controller
{

	public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getAll($search="")
    {
        if ($search=="") {
            $teachers = Teacher::leftJoin('courses','courses.id','=','teachers.course_id')->select(\DB::raw('teachers.firstname,teachers.lastname,teachers.contact,teachers.id,teachers.profile_picture,courses.name'))->paginate(15);
            return $teachers;
        }else{
            $teachers = Teacher::leftJoin('courses','courses.id','=','teachers.course_id')->select(\DB::raw('teachers.firstname,teachers.lastname,teachers.contact,teachers.id,teachers.profile_picture,courses.name'))->where('teachers.lastname','like','%'.$search.'%')->orWhere(\DB::raw('concat(teachers.firstname," ",teachers.lastname)'),'like','%'.$search.'%')->orWhere('contact','like','%'.$search.'%')->orWhere('courses.name','like','%'.$search.'%')->paginate(15);
            return $teachers;
        }
    }

	public function myGrades()
	{
		$user_id = \Auth::user()->id;

		$student = Student::where('user_id',$user_id)->first();

		$student_grades = array();

		for ($i=1; $i <=4 ; $i++) {

			$byYearGrade = array();
			for ($j=1; $j <= 2; $j++) {
				$grades = \DB::select('select a.course_id,a.title,a.year_id,a.sem_id,a.id as subject_id,a.units,a.course_no,b.school_id,b.firstname,b.middlename,b.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from subjects a left join (select a.id as student_id,a.user_id,a.school_id,a.firstname,a.middlename, a.lastname,b.subject_id,b.grade,b.equivalent,b.status,b.batch_id from students a left join grades b on a.school_id = b.school_id where a.id = ?) b on a.id = b.subject_id left join courses as c on a.course_id = c.id where a.year_id = ? and a.sem_id = ? and course_id = ?;', [$student->id,$i,$j,$student->course_id]);
				foreach ($grades as $key => $value) {
            
		            $now = Carbon::now();
		            $dt = Carbon::create($value->batch_id, $value->sem_id==1?5:11, 1, 0);
		            
		            if ($dt->diffInYears($now)>=1&&$value->equivalent==4) {
		                $value->equivalent = 5;
		            }
		        }

				$universal = \DB::select('SELECT * FROM `grades` a left join subjects b on a.subject_id = b.id where a.school_id = ? and a.year_id = ? and a.sem_id=?',[$student->school_id,$i,$j]);

		        foreach ($universal as $key => $value) {
		            foreach ($grades as $key1 => $value1) {
		                if ($value->course_no==$value1->course_no) {
		                    if ($value->equivalent!=null&&$value->equivalent!="") {
		                        $value1->equivalent = $value->equivalent;
		                    }
		                }
		            }
		        }
				$byYearGrade[] = $grades;
			}
			$student_grades[] = $byYearGrade;
		}
		for ($i=0; $i < count($student_grades); $i++) { 
			
			for ($j=0; $j < count($student_grades[$i]); $j++) {
				$student_grades[$i][$j]['gpa'] = $this->getGpa($student_grades[$i][$j]);
				$student_grades[$i][$j]['units'] = $this->getUnits($student_grades[$i][$j]);
				$student_grades[$i][$j]['name'] = $this->numberToWord($j);
			}
			$student_grades[$i]['name'] = $this->numberToWord($i);
		}
		return view('layouts.student.my_grades',['student_grades'=>$student_grades]);
	}

	public function numberToWord($number)
	{
		$words = ["First","Second","Third","Fourth"];

		return $words[$number];
	}

	public function deansLister($batch_id,$sem_id)
	{
		$deanslisters['first'] = array();
        $deanslisters['second'] = array();

        $students= \DB::select('select a.firstname,a.middlename,a.lastname,a.school_id,b.batch_id,d.name from students a left join grades b on a.school_id = b.school_id left join subjects c on b.subject_id = c.id left join courses d on a.course_id = d.id where b.equivalent <=2.5 group by school_id having SUM(c.units) > 17');

        foreach ($students as $key => $value) {



            $units = \DB::select('select sum(units) as total_units from (select b.units from grades a left join subjects b on a.subject_id = b.id where a.batch_id = ? and a.sem_id = ? and a.school_id = ? group by b.course_no) as a',[$batch_id,$sem_id,$value->school_id])[0];
            

            $grades = \DB::select('select a.equivalent,b.units from grades a left join subjects b on a.subject_id = b.id where a.batch_id = ? and a.sem_id = ? and a.school_id = ? group by b.course_no',[$batch_id,$sem_id,$value->school_id]);

            if ($units->total_units >= 18) {

                $sum_grades = 0;
                $s="";
                $t="";
                $average_gpa = 0;

                foreach ($grades as $key1 => $value1) {

                    $s+=($value1->equivalent * $value1->units);
                    $t+=$value1->units;
                    $sum_grades += ($value1->equivalent * $value1->units);
                }
                $average_gpa = number_format($sum_grades/$units->total_units,3);

                $year_level = $batch_id+1 - ($value->batch_id); 

                if ($average_gpa<=1.5) {
                    $value->gpa = $average_gpa;
                    $value->year = $year_level;
                    $value->sem = $sem_id;
                    $deanslisters['first'][] = $value;
                }else if($average_gpa<=1.75){
                    $value->gpa = $average_gpa;
                    $value->year = $year_level;
                    $value->sem = $sem_id;
                    $deanslisters['second'][] = $value;
                }

            }
        }

        return $deanslisters;
	}

	public function deansList()
	{
		return view('layouts.student.deans_list');
	}

	public function profile($id)
	{
		return view('layouts.student.profile');
	}

	public function updatePicture(Requests\Student\UploadImage $request)
	{
		$profile_picture = $request->profile_picture;
        $file_id = uniqid();
        $path = 'pictures\\'.$profile_picture->getClientOriginalName().'_'.uniqid().'.'. $profile_picture->getClientOriginalExtension();
        \Storage::put('public\\'.$path, \Illuminate\Support\Facades\File::get($profile_picture));

        $fs = new Filesystem();
        $fs->symlink(storage_path('app/public'), public_path('storage/public'),true);
        return $path;
	}

	public function studentProfile($username)
	{
		$user = User::where('username',$username)->first();
		
		$student = Student::leftJoin('parents','students.id','=','parents.student_id')->select(\DB::raw('students.address,students.profile_picture,students.batch_id,students.birthdate,students.year_level,students.contact,students.course_id,students.email,students.firstname,students.middlename,students.lastname,students.id,students.school_id,students.sex,students.status,students.user_id,parents.address as parent_address,parents.contact as parent_contact,parents.firstname as parent_firstname,parents.lastname as parent_lastname'))->where('user_id',$user->id)->first();
		$courses = Course::all();
		$birthdate = explode('-', $student->birthdate);
		$student->age = Carbon::createFromDate($birthdate[0], $birthdate[1], $birthdate[2])->age;
		$user_student = compact('user','student','courses');

		return $user_student;
	}

	public function udpateProfile(Requests\Student\Update $request)
	{
		$student = Student::find($request->id);

        $student->sparent;

        $student->school_id = $request->school_id;
        $student->firstname = $request->firstname;
        $student->middlename = $request->middlename;
        $student->lastname = $request->lastname;
        $student->status = $request->status;
        $student->birthdate = $request->birthdate;
        $student->sex = $request->sex;
        $student->profile_picture = $request->profile_picture;
        $student->contact = $request->contact;
        $student->email = $request->email;
        $student->address = $request->address;
        $student->course_id = $request->student_course;
        $student->batch_id = $request->student_year_entry;
        $student->year_level = $request->year_level;
        $student->sparent->firstname = $request->parent_firstname;
        $student->sparent->lastname = $request->parent_lastname;
        $student->sparent->contact = $request->parent_contact;
        $student->sparent->address = $request->parent_address;

        $student->save();
        $student->sparent->save();
        $student = $this->studentProfile($request->username);

        return $student;
	}

	public function updateCredential(Requests\Student\UpdateCredential $request,$user_id)
	{
		$user = User::find($user_id);
		if ($request->password=="") {
			$user->username = $request->username;
			$user->save();
		}else{
			$user->username = $request->username;
			$user->password = bcrypt($request->password);
			$user->save();
		}

		return $user;
	}

	public function teacherPerformanceAppraisal()
	{
		return view('layouts.student.teachers_appraisal');
	}

	public function getQuestionaires()
	{
		$qtypes = QType::all();
		foreach ($qtypes as $key => $value) {
			$value->questionaires;
		}
		return $qtypes;
	}

	public function storeTeachersEvaluation(Request $request)
	{
		$student_id = \Auth::user()->id;

		foreach ($request->answers as $key => $value) {
			if ($value!=null) {
				$split_form_evaluation = explode("_", $value);
				Evaluation::create([
					'student_id' => $student_id,
					'teacher_id' => $request->teacher_id,
					'q_type_id' => $split_form_evaluation[1],
					'questionaire_id' => $split_form_evaluation[0],
					'rate' => $split_form_evaluation[2],
					'sem_id' => $request->sem_id,
					'batch_id' => $request->batch_id,
					'strengths' => $request->strengths,
					'weaknesses' => $request->weaknesses,
					'recommendation' => $request->recommendation,

				]);
			}
		}
	}

	public function checkIfEvaluated($teacher_id,$batch_id,$sem_id)
	{

		$evaluations = \DB::select('select count(*) as count from evaluations where student_id = ? and batch_id = ? and sem_id = ? and teacher_id = ?;',[\Auth::user()->id,$batch_id,$sem_id,$teacher_id])[0];

		if ($evaluations->count>0) {

			$qtypes = QType::all();
			foreach ($qtypes as $key => $value) {
				$value->questionaires;
				foreach ($value->questionaires as $key1 => $value1) {
					$value1->answer = $value1->leftJoin('evaluations','evaluations.questionaire_id','=','questionaires.id')->select(\DB::raw('evaluations.strengths,evaluations.weaknesses,evaluations.recommendation,evaluations.rate'))->where('evaluations.questionaire_id',$value1->id)->where('evaluations.student_id',\Auth::user()->id)->where('evaluations.teacher_id',$teacher_id)->where('evaluations.batch_id',$batch_id)->where('evaluations.sem_id',$sem_id)->first();
				}
			}
			return $qtypes;
		}else{
			return "no";
		}
	}

	public function showThisStudentPage($teacher_id,$batch_id,$sem_id)
	{
		// $teacher = Teacher::where('id',$teacher_id)->first();
		
        $evaluation_results = \DB::select('select b.name,sum(rate) as sum from evaluations a left join q_types b on a.q_type_id = b.id where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ? group by q_type_id;',[\Auth::user()->id,$teacher_id,$batch_id,$sem_id]);
        $sum = \DB::select('select sum(rate) as sum from evaluations where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ?',[\Auth::user()->id,$teacher_id,$batch_id,$sem_id])[0]->sum;
        $average = \DB::select('select avg(rate) as average from evaluations where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ?',[\Auth::user()->id,$teacher_id,$batch_id,$sem_id])[0]->average;
        return compact('evaluation_results','sum','average');
	}

	public function getGpa($grades){

		$units = 0;
		$sum_grades = 0;

		foreach ($grades as $key => $grade) {
			if (!is_null($grade->equivalent)) {
				$sum_grades += $grade->equivalent * $grade->units;
				$units+=$grade->units;
			}
		}
		if ($units==0) {
			$units = 1;
		}

		return round($sum_grades/$units,3);

	}

	public function getUnits($grades)
	{

		$units = 0;

		foreach ($grades as $key => $grade) {
			if (is_object($grade)) {
				if (!is_null($grade->equivalent)) {
					$units+=$grade->units;
				}
			}
		}

		return $units;
	}


	public function iloveyoumona($teacher_id,$batch_id,$sem_id)
	{
		echo json_encode("sdf");
	}

}
