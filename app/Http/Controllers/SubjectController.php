<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Subject;
use App\Course;
use App\Prerequisite;
use App\Http\Controllers\CurriculumController;

class SubjectController extends Controller
{
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\Student']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Subjects\Store $request)
    {
        try {
            \DB::beginTransaction();
            $subject = Subject::create([

                'course_id' => $request->course_id,
                'course_no' => $request->course_no,
                'title' => $request->title,
                'lec' => $request->lec,
                'lab' => $request->lab,
                'units' => $request->units,
                'year_id' => $request->year_id,
                'sem_id' => $request->sem_id,
                'rest' => $request->rest

                ]);

            $prerequisites = array();

            foreach ($request->prerequisites as $key => $value) {
                if ($value!=0) {
                    $prerequisites[] = ['subject_id' => $subject->id,'prerequisite'=>$value];
                }
            }

            Prerequisite::insert($prerequisites);

            \DB::commit();

            return $this->all($request->course_id);
        } catch (Exception $e) {
            \DB::rollBack();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);
        $prerequisites = Prerequisite::leftJoin('subjects','subjects.id','=','prerequisites.prerequisite')->select(\DB::raw('subjects.id'))->where('prerequisites.subject_id',$subject->id)->get();
        return compact('subject','prerequisites');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Subjects\Store $request, $id)
    {
        try {
            \DB::beginTransaction();
            $subject = Subject::where('id',$request->id)->where('course_id',$request->course_id)->first();

            Prerequisite::where('subject_id',$subject->id)->delete();
            $subject->course_no = $request->course_no;
            $subject->title = $request->title;
            $subject->lec = $request->lec;
            $subject->lab = $request->lab;
            $subject->units = $request->units;
            $subject->year_id = $request->year_id;
            $subject->sem_id = $request->sem_id;
            $subject->rest = $request->rest;
        
            $prerequisites = array();

            foreach ($request->prerequisites as $key => $value) {
                if ($value!=0) {
                    $prerequisites[] = ['subject_id' => $subject->id,'prerequisite'=>$value];
                }
            }

            Prerequisite::insert($prerequisites);
            $subject->save();
            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete();
    }

    public function getExceptCurrent($id)
    {
        $subjects = Subject::leftJoin('prerequisites','prerequisites.subject_id','=','subjects.id')->where('subjects.id' ,'!=',$id)->where(function ($query) use($id){
                $query->whereNull('prerequisites.prerequisite')
                      ->orWhere('prerequisites.prerequisite', '!=', $id);
            })->select(\DB::raw('subjects.year_id,subjects.sem_id,subjects.id,subjects.course_no'))->groupBy('subjects.course_no')->get();
        return $subjects;
    }

    public function all($course_id)
    {
        $curriculumcontroller = new CurriculumController();
        return $curriculumcontroller->subjects($course_id);
        // $course = Course::find($id);
        // $course->subjects;
        // $subjects = $course->subjects()->paginate(15);
        // return compact('course', 'subjects');
    }

    public function allSubjectsByCourse($course_id)
    {
        $subjects = Subject::where('course_id',$course_id)->get();
        return $subjects;
    }

    public function byYearSemCourse($course_id,$year_id,$sem_id)
    {
        $subjects = Subject::where('course_id',$course_id)->where('year_id',$year_id)->where('sem_id',$sem_id)->get();
        return $subjects;
    }
}
