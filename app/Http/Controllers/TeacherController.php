<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Teacher;
use App\Course;
use App\QType;
use App\Student;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class TeacherController extends Controller
{
    
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\Student']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.teachers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Teachers\Store $request)
    {
        Teacher::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'degree' => $request->degree,
            'contact' => $request->contact,
            'profile_picture' => $request->profile_picture,
            'last_graduated' => $request->last_graduated,
            'attainment' => $request->attainment,
            'course_id' => $request->course_id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::leftJoin('courses','courses.id','=','teachers.course_id')->select(\DB::raw('teachers.firstname,teachers.lastname,courses.name'))->where('teachers.id',$id)->first();
        return $teacher;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);
        return $teacher;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Teachers\Store $request, $id)
    {
        $teacher = Teacher::find($id);
        $teacher->firstname = $request->firstname;
        $teacher->lastname = $request->lastname;
        $teacher->contact = $request->contact;
        $teacher->degree = $request->degree;
        $teacher->profile_picture = $request->profile_picture;
        $teacher->last_graduated = $request->last_graduated;
        $teacher->attainment = $request->attainment;
        $teacher->course_id = $request->course_id;
        $teacher->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::find($id)->delete();
    }

    public function getAll($search="")
    {
        if ($search=="") {
            $teachers = Teacher::leftJoin('courses','courses.id','=','teachers.course_id')->select(\DB::raw('teachers.firstname,teachers.lastname,teachers.contact,teachers.id,teachers.profile_picture,courses.name'))->paginate(15);
            return $teachers;
        }else{
            $teachers = Teacher::leftJoin('courses','courses.id','=','teachers.course_id')->select(\DB::raw('teachers.firstname,teachers.lastname,teachers.contact,teachers.id,teachers.profile_picture,courses.name'))->where('teachers.lastname','like','%'.$search.'%')->orWhere(\DB::raw('concat(teachers.firstname," ",teachers.lastname)'),'like','%'.$search.'%')->orWhere('contact','like','%'.$search.'%')->orWhere('courses.name','like','%'.$search.'%')->paginate(15);
            return $teachers;
        }
    }

    public function teachersAppraisalList($batch_id,$sem_id)
    {
        $appraisals = \DB::select("select a.id,concat(lastname,', ',firstname) as name,c.name course,(avg(b.rate)) as average from teachers a left join (select a.teacher_id,a.id,a.rate from evaluations a right join users b on a.student_id = b.id where batch_id = ? and sem_id = ? ) b on a.id = b.teacher_id left join courses c on a.course_id = c.id group by a.id;",[$batch_id,$sem_id]);

        return $appraisals;
    }

    public function getByCourse($course_id)
    {
        $teachers = Teacher::where('course_id',$course_id)->get();
        return $teachers;
    }

    public function sendBulkMessage(Request $request)
    {
        foreach ($request->sendTo as $key => $value) {
            $fields = array();
            $fields["api"] = $request->api;
            $fields["number"] = $value; //safe use 63
            $fields["message"] = $request->message;
            $fields["from"] = 'DNSC-Education';
            $fields_string = http_build_query($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.semaphore.co/api/sms" );
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $output = curl_exec($ch);
            curl_close($ch);
        }
    }

    public function printEvaluation($sem_id,$batch_id)
    {
        $appraisals = \DB::select("select a.id,concat(lastname,', ',firstname) as name,c.name course,(avg(b.rate)) as average from teachers a left join (select a.teacher_id,a.id,a.rate from evaluations a right join users b on a.student_id = b.id where batch_id = ? and sem_id = ? ) b on a.id = b.teacher_id left join courses c on a.course_id = c.id group by a.id;",[$batch_id,$sem_id]);

        return view('layouts.documents.appraisal',['appraisals'=>$appraisals,'sem'=>$sem_id,'batch'=>$batch_id]);
    }

    public function updatePicture(Requests\Student\UploadImage $request)
    {
        $profile_picture = $request->profile_picture;
        $file_id = uniqid();
        $path = 'pictures\\'.$profile_picture->getClientOriginalName().'_'.uniqid().'.'. $profile_picture->getClientOriginalExtension();
        \Storage::put('public\\'.$path, \Illuminate\Support\Facades\File::get($profile_picture));

        $fs = new Filesystem();
        $fs->symlink(storage_path('app/public'), public_path('storage/public'),true);
        return $path;
    }

    public function viewEvaluationDetails()
    {
        return view('layouts.teachers.evaluation');
    }

    public function moreEvaluationDetails($batch_id,$sem_id,$teacher_id)
    {
        $students = \DB::select('select b.user_id, concat(b.firstname," ",b.middlename," ",b.lastname) fullname,c.name,avg(a.rate) as rate,a.created_at from evaluations a right join students b on a.student_id = b.user_id left join courses c on b.course_id = c.id where a.teacher_id = ? and a.batch_id = ? and sem_id = ? group by a.student_id ;',[$teacher_id,$batch_id,$sem_id]);
        return $students;
    }

    public function studentTeacherEvaluation($batch_id,$sem_id,$teacher_id,$student_id)
    {
        // $evaluations = \DB::select('select b.id,rate,d.description,a.strengths,a.weaknesses,a.recommendation,a.created_at from evaluations a left join students b on a.student_id = b.user_id left join courses c on b.course_id = c.id left join questionaires d on a.questionaire_id = d.id where a.teacher_id = ? and a.batch_id = ? and sem_id = ? and student_id = ?;',[$teacher_id,$batch_id,$sem_id,$student_id]);
        // return $evaluations;
        $evaluations = \DB::select('select count(*) as count from evaluations where student_id=? and batch_id = ? and sem_id = ? and teacher_id = ?;',[$student_id,$batch_id,$sem_id,$teacher_id])[0];
        $qtypes = QType::all();
        foreach ($qtypes as $key => $value) {
            $value->questionaires;
            foreach ($value->questionaires as $key1 => $value1) {
                $value1->answer = $value1->leftJoin('evaluations','evaluations.questionaire_id','=','questionaires.id')->where('evaluations.questionaire_id',$value1->id)->where('evaluations.student_id',$student_id)->where('evaluations.teacher_id',$teacher_id)->where('evaluations.batch_id',$batch_id)->where('evaluations.sem_id',$sem_id)->first();
            }
        }
        return $qtypes;
    }

    public function showThisStudent($user_id,$teacher_id,$batch_id,$sem_id)
    {
        $student = Student::where('user_id',$user_id)->first();
        $evaluation_results = \DB::select('select b.name,sum(rate) as sum from evaluations a left join q_types b on a.q_type_id = b.id where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ? group by q_type_id;',[$user_id,$teacher_id,$batch_id,$sem_id]);
        $sum = \DB::select('select sum(rate) as sum from evaluations where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ?',[$user_id,$teacher_id,$batch_id,$sem_id])[0]->sum;
        $average = \DB::select('select avg(rate) as average from evaluations where student_id = ? and teacher_id = ? and batch_id = ? and sem_id = ?',[$user_id,$teacher_id,$batch_id,$sem_id])[0]->average;
        return compact('student','evaluation_results','sum','average');
    }
}
