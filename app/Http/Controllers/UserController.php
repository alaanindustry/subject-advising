<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Admin;
use App\Setting;

class UserController extends Controller
{
    /**
     * Create a new middleware instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\Student']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Users\Store $request)
    {
        try {
            \DB::beginTransaction();

            $user = User::create([

                'role' => 'admin',
                'username' => $request->username,
                'password' => bcrypt($request->password)

            ]);

            $admin = Admin::create([

                'user_id' => $user->id,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname 

            ]);

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::leftJoin('admins','users.id','=','admins.user_id')->select(\DB::raw('users.id,admins.firstname,admins.lastname,users.username'))->where('users.id',$id)->first();

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Users\Update $request, $id)
    {
        $user = User::find($id);
        $user->username = $request->username;
        $user->admin->firstname = $request->firstname;
        $user->admin->lastname = $request->lastname;
        $user->save();
        $user->admin->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }

    public function getAll()
    {
        $users = User::leftJoin('admins','users.id','=','admins.user_id')->select(\DB::raw('users.id,users.username,admins.firstname,admins.lastname'))->where('username','!=',\Auth::user()->username)->where('role','admin')->paginate(10);
        return $users;
    }

    public function updatePassword(Requests\Users\UpdatePassword $request)
    {
        $user = User::find($request->id);
        $user->password = bcrypt($request->password);
        $user->save();
    }

    public function settings()
    {
        return view('layouts.settings');
    }

    public function getInstituteCode()
    {
        $settings = Setting::first();
        return $settings;
    }

    public function updateInstituteCode(Request $request)
    {
        $settings = Setting::first();
        $settings->code = $request->code;
        $settings->save();
    }

    public function getAuth(Request $request)
    {
        $auth = \Auth::user();
        $user = User::leftJoin('admins','users.id','=','admins.user_id')->select(\DB::raw('users.id,admins.firstname,admins.lastname,users.username'))->where('users.id',$auth->id)->first();
        return $user;
    }

    public function updateAdminCredential(Requests\Admin\Update $request)
    {
        $user = User::find($request->id);
        $admin = Admin::where('user_id',$user->id)->first();
        $user->username = $request->username;
        if ($request->password!="") {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->save();
    }
}
