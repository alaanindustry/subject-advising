<?php

namespace App\Http\Requests\Guest;

use App\Http\Requests\Request;

class Store extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if (\Auth::user()->role != "student") {
        //     return false;
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required|school_id|unique:students',
            'firstname' => 'required|min:2',
            'middlename' => 'min:1',
            'lastname' => 'required|min:2',
            'birthdate' => 'required|date',
            'contact' => 'required|phone',
            'address' => 'required',
            'email' => 'required|email|unique:students',
            'username' => 'required|unique:users',
            'password' => 'required|confirmed',
            'parent_firstname' => 'required|min:2',
            'parent_lastname' => 'required|min:2',
            'parent_contact' => 'phone',
            'institutecode' => 'required|institutecode'
        ];
    }
}
