<?php

namespace App\Http\Requests\Student;

use App\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required|school_id|unique:students,school_id,'.$this->id,
            'firstname' => 'required|min:2',
            'middlename' => 'required|min:2',
            'lastname' => 'required|min:2',
            'birthdate' => 'required|date',
            'contact' => 'phone',
            'address' => 'required',
            'email' => 'email|unique:students,id,'.$this->email,
            'parent_firstname' => 'required|min:2',
            'parent_lastname' => 'required|min:2',
            'parent_contact' => 'phone'
        ];
    }
}
