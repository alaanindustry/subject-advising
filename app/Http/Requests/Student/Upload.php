<?php

namespace App\Http\Requests\Student;

use App\Http\Requests\Request;

class Upload extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_file' => 'file'
        ];
    }

    protected function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            if($this->hasFile('grade_file')){
                $extension = $this->file('grade_file')->getClientOriginalExtension();
                if (!($extension=='csv'||$extension=='xls'||$extension=="xlsx")) {
                    $validator->errors ()->add('grade_file', 'Invalid file Type. File must be csv,xls,xlsx');
                }
            }
        });

        return $validator;
    }
}
