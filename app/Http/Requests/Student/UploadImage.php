<?php

namespace App\Http\Requests\Student;

use App\Http\Requests\Request;

class UploadImage extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_picture' => 'file'
        ];
    }

    protected function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            if($this->hasFile('profile_picture')){
                $extension = $this->file('profile_picture')->getClientOriginalExtension();
                if (!(strtolower($extension)=='jpg'||strtolower($extension)=='jpeg'||strtolower($extension)=='png'||strtolower($extension)=="gif")) {
                    $validator->errors ()->add('profile_picture', 'Invalid file Type. File must be either jpg,png,gif');
                }
            }
        });

        return $validator;
    }
}
