<?php

namespace App\Http\Requests\Subjects;

use App\Http\Requests\Request;

class Store extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_no' => 'required',
            'title' => 'required',
            'lec' => 'required|integer',
            'lab' => 'required|integer',
            'units' => 'required|integer'
        ];
    }
}
