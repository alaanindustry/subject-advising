<?php 

namespace App\Http\Validators;

use App\Setting;

class InstituteCode {

	/**
     * Determine if the passed phone is valid
     *
     * @return bool
     */
    public function validateInstituteCode($attribute, $value, $parameters, $validator)
    {
        $institutecode = Setting::first();

        if($institutecode->code === $value){
            return true;
        }
        return false;
    }

}