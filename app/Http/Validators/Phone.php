<?php 

namespace App\Http\Validators;

class Phone {

	/**
     * Determine if the passed phone is valid
     *
     * @return bool
     */
    public function validatePhone($attribute, $value, $parameters, $validator)
    {
        if( preg_match('/^(09|\+639)\d{9}$/', $value) ){
            return true;
        }
        return false;
    }

}