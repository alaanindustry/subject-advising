<?php 

namespace App\Http\Validators;

class School_ID {

	/**
     * Determine if the passed phone is valid
     *
     * @return bool
     */
    public function validateSchool_ID($attribute, $value, $parameters, $validator)
    {
        if( preg_match('/^20\d{2}-\d{5}$/', $value) ){
            return true;
        }
        return false;
    }

}