<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about',function()
{
	return view('about');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// Students Routes

Route::resource('/students','StudentController');

Route::get('/students/get/all/{search?}','StudentController@all');
Route::get('/students/get/bycourse/{course_id}','StudentController@getByCourse');

// Guest Routes

Route::post('/guest/register','GuestController@register');

// Curriculum Routes

Route::resource('/courses','CurriculumController');
Route::get('/courses/get/all','CurriculumController@all');
Route::get('/courses/get/subjects/{course_id}','CurriculumController@subjects');

// Subject Routes;

Route::resource('/subjects','SubjectController');

Route::get('/subjects/get/all/byyearsemcourse/{course_id}/{year_id}/{sem_id}','SubjectController@byYearSemCourse');

Route::get('subjects/except/current/{subject_id}','SubjectController@getExceptCurrent');

Route::get('subjects/get/subjects/by/course/{course_id}','SubjectController@allSubjectsByCourse');

// Student Routes;
// 

Route::post('/students/update/credential','StudentController@updateCredential');

Route::get('/students/get/users/by/student/courseid/{user_id}','StudentController@getUser');

Route::get('/students/get/grades/{year_id}/{sem_id}/{student_id}/{course_id}','StudentController@grades');

Route::post('/students/import/grades','StudentController@importGrades');

Route::post('/students/upload/grade','StudentController@uploadGrade');

Route::get('/students/available/subjects/{school_id}/{course_id}','StudentController@availableSubjects');

Route::post('/students/send/message','StudentController@sendMessage');

Route::post('/students/send/bulkmessage','StudentController@sendBulkMessage');

Route::get('/students/get/deanslister/{batch_id}/{sem_id}','StudentController@deansLister');

Route::get('/students/deanslister/print/{batch_id}/{sem_id}','StudentController@deanslisterPrint');

Route::get('/students/printgrade/{student_id}/{course_id}','StudentController@printGrade');

Route::get('/students/printallgrade/{year_id}/{sem_id}/{subject_id}/{course_id}','StudentController@printAllGrade');

Route::get('/students/print/all/grades/{batch_id}/{sem_id}/{subject_id}/{course_id}','StudentController@printAllGrades');

// User Routes

Route::resource('/users','UserController');

Route::get('/users/get/all','UserController@getAll');

Route::post('/users/updatepassword','UserController@updatePassword');
	
// StudentPage Route
// 
Route::get('checkif/evaluated/{teacher_id}/{batch_id}/{sem_id}','StudentPageController@checkIfEvaluated');

Route::post('profile/student/upload/picture','StudentPageController@updatePicture');

Route::post('profile/update/credentials/{user_id}','StudentPageController@updateCredential');

Route::get('/mygrades','StudentPageController@myGrades');

Route::get('/get/deanslister/{batch_id}/{sem_id}','StudentPageController@deansLister');

Route::get('/deanslist','StudentPageController@deansList');

Route::get('/profile/{username}','StudentPageController@profile');

Route::get('/profile/studentinfo/{username}','StudentPageController@studentProfile');

Route::put('/profile/update/user/{student_id}','StudentPageController@udpateProfile');

Route::get('/teachersappraisal','StudentPageController@teacherPerformanceAppraisal');

Route::post('storeevaluation','StudentPageController@storeTeachersEvaluation');

Route::get('/getquestionaires','StudentPageController@getQuestionaires');

Route::get('/getgrades','StudentController@');

// Teachers Route

Route::resource('/teachers','TeacherController');

Route::get('students/{user_id}/{teachers}/{batch_id}/{sem_id}/show/this','TeacherController@showThisStudent');

Route::get('students/{teacher_id}/{batch_id}/{sem_id}/show/this/studentpage','StudentPageController@showThisStudentPage');

Route::get('teachers/get/evaluation/details/{teacher_id}','TeacherController@viewEvaluationDetails');

Route::get('teachers/get/evaluation/more/details/{batch_id}/{sem_id}/{teacher_id}','TeacherController@moreEvaluationDetails');

Route::get('teachers/get/student/teacher/evaluation/{batch_id}/{sem_id}/{teacher_id}/{student_id}','TeacherController@studentTeacherEvaluation');

Route::post('/teachers/upload/picture','TeacherController@updatePicture');

Route::get('/teachers/get/all/{search?}','StudentPageController@getAll');

Route::get('/teachers/appraisal/{batch_id}/{sem_id}','TeacherController@teachersAppraisalList');

Route::get('/teachers/get/bycourse/{course_id}','TeacherController@getByCourse');

Route::post('/teachers/send/bulkmessage','TeacherController@sendBulkMessage');

Route::get('/teachers/print/evaluation/{sem_id}/{batch_id}','TeacherController@printEvaluation');

// Admin Route

Route::get('/settings','UserController@settings');

Route::get('/getinstitutecode','UserController@getInstituteCode');

Route::post('/updateinstitutecode','UserController@updateInstituteCode');

Route::get('/getauth','UserController@getAuth');

Route::post('/updateadmincredential','UserController@updateAdminCredential');

Route::get('/checkif/evaluated/i/love/mona/the/most/since/ever/{teacher_id}/{batch_id}/{sem_id}','GuestController@iloveyoumona');