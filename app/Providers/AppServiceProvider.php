<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Validator;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $fs = new Filesystem();
        $fs->symlink(storage_path('app/public'), public_path('storage/public'),true);

        Validator::extend('school_id', 'App\Http\Validators\School_ID@validateSchool_ID');
        Validator::extend('phone', 'App\Http\Validators\Phone@validatePhone');
        Validator::extend('institutecode', 'App\Http\Validators\InstituteCode@validateInstituteCode');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
