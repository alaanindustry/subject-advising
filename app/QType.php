<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QType extends Model
{
	protected $table = 'q_types';

	public function questionaires()
    {
        return $this->hasMany('App\Questionaire');
    }
}
