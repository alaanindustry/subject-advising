<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SParent extends Model
{
	protected $table = "parents";

   	protected $fillable = [
        'firstname', 'lastname', 'address','contact','student_id'
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }
}
