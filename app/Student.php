<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

	protected $fillable = [
        'profile_picture','course_id','user_id','school_id','batch_id','firstname', 'middlename','lastname','status','birthdate','sex','contact','address','email','year_level'
    ];

    public function sparent()
    {
        return $this->hasOne('App\SParent');
    }
    
}
