<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'course_id','course_no','title','lec','lab','year_id','sem_id','units','prerequisite','rest'
    ];
}
