<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	protected $fillable = [
        'profile_picture','firstname','lastname','contact', 'degree','last_graduated','attainment','course_id','created_at','updated_at'
    ];
}
