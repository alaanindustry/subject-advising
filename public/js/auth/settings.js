Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({
	el:'#settings-component',
	data:{
		institutecode:'',
		admin:{
			firstname:'',
			lastname:'',
			username:'',
			password:'',
			password_confirmation:''
		},
		admin_error:{
			firstname:'',
			lastname:'',
			username:'',
			password:'',
			password_confirmation:''
		}
	},
	mounted:function (argument) {
		var vue = this;
		vue.$http.get('/cas-ied/public/getauth').then(
			(response) => {
				vue.admin = response.data;
			}
		);
	},
	methods:{
		updateAdminCredential:function(){
			var vue = this;
			vue.$http.post('/cas-ied/public/updateadmincredential',vue.admin).then(
				(response) =>{
					vue.admin_error = [];
					vue.admin_error.firstname = '';
					vue.admin_error.lastname = '';
					vue.admin_error.username = '';
					vue.admin_error.password = '';
					vue.admin_error.password_confirmation = '';
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated Settings");
				},(response)=> {
					vue.admin_error = [];
					vue.admin_error.firstname = '';
					vue.admin_error.lastname = '';
					vue.admin_error.username = '';
					vue.admin_error.password = '';
					vue.admin_error.password_confirmation = ''
					var errors = response.data;
					$.each(errors,function(index,value){
						vue.admin_error[index] = value;
					});
				}
			);
		},
		discardChanges:function(){
			var vue = this;
			vue.admin_error = [];
			vue.admin_error.firstname = '';
			vue.admin_error.lastname = '';
			vue.admin_error.username = '';
			vue.admin_error.password = '';
			vue.admin_error.password_confirmation = '';
			vue.$http.get('/cas-ied/public/getauth').then(
				(response) => {
					vue.admin = response.data;
				}
			);
		},
		editInstituteCode:function(){
			var vue = this;
			vue.$http.get('/cas-ied/public/getinstitutecode').then(
				(response) => {
					vue.institutecode = response.data;
				}
			);
			$('#edit-institute-code-modal').modal('show');
		},
		updateInstituteCode:function(){
			var vue = this;
			vue.$http.post('/cas-ied/public/updateinstitutecode',vue.institutecode).then(
				(response) => {
					$('#edit-institute-code-modal').modal('hide');
					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated institute code");
				}
			);
		}
	}
})