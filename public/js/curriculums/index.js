
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');
new Vue({

	el:"#curriculum-component",
	data:{
		course_id:'',
		course_name:'',
		course:{
			name:'',
			description:''
		},
		course_error:{
			name:'',	
			description:'',
		},
		subject:{
			course_no:'',
			title:'',
			lec:'',
			lab:'',
			units:'',
			year_id:'1',
			rest:'',
			sem_id:'1',
			prerequisites:[],
			course_id:''
		},
		prerequisites:[0
		],
		new_prerequisites:[0],
		subject_error:{
			course_no:'',
			title:'',
			rest:'',
			lec:'',
			lab:'',
			units:'',
		},
		courses:[],
		subjects:[],
		subjects_option:[]
	},
	mounted:function () {
		this.getCourses('courses/get/all');
	},
	methods:{
		getCourses:function (url) {
			var vue = this;
			vue.$http.get(url).then(
				(response) =>{
					vue.courses = response.data.data;
					var course_id = vue.courses[0].id;
					vue.course_id = course_id;
					vue.$http.get('courses/get/subjects/'+course_id).then(
						(response) =>{
							vue.course_name = response.data.course.name;
							vue.subjects = response.data.subjects;
							vue.$http.get('subjects/get/subjects/by/course/'+course_id).then(
								(response) => {
									vue.subjects_option = response.data;
								}
							);
						}
					);
				}
			);
		},
		newCourse:function () {
			this.course_error = {};
			$('#new-course-modal').modal('show');
		},
		addCourse:function () {
			var vue = this;
			vue.$http.post('courses',vue.course).then(
				(response)=>{
					vue.courses = response.data.data;
					$('#new-course-modal').modal('hide');
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully added as new course");
				},(response)=>{
					vue.course_error = response.data;
				});
		},
		editCourse:function (course_id) {
			var vue = this;
			vue.course_id = course_id;
			vue.$http.get('courses/'+course_id+'/edit').then(
				(response)=>{
					vue.course = response.data;
					$('#edit-course-modal').modal('show');
				}
			);
			
		},
		updateCourse:function() {
			var vue = this;
			vue.$http.put('courses/'+vue.course_id,vue.course).then(
				(response)=>{
					this.getCourses('courses/get/all');
					$('#edit-course-modal').modal('hide');
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated curriculum");
				},(response)=>{
					vue.course_error = response.data;
				}
			);
		},
		viewSubjects:function (course_id) {
			var vue = this;
			vue.course_id = course_id;
			vue.$http.get('courses/get/subjects/'+course_id).then(
				(response) =>{
					vue.course_name = response.data.course.name;
					vue.subjects = response.data.subjects;
					vue.$http.get('subjects/get/subjects/by/course/'+course_id).then(
						(response) => {
							vue.subjects_option = response.data;
						}
					);
				}
			);
		},
		newSubject:function (argument) {
			var vue = this;
			vue.new_prerequisites = [0];
			vue.subject = {};
			vue.subject_error = {};
			vue.subject.year_id = 1;
			vue.subject.sem_id = 1;
			$('#new-subject-modal').modal('show');
		},
		addSubject:function () {
			var vue = this;
			vue.subject.course_id = vue.course_id;
			vue.subject.prerequisites = vue.new_prerequisites;
			vue.$http.post('subjects',vue.subject).then(
				(response)=>{
					vue.course_name = response.data.course.name;
					vue.subjects = response.data.subjects;
					vue.$http.get('subjects/get/subjects/by/course/'+vue.course_id).then(
						(response) => {
							vue.subjects_option = response.data;
							$('#new-subject-modal').modal('hide');
							var toast = toastr;
							toast.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};

							Command: toast["success"]("Successfully added new Subject");
						}
					);
				}, 
				(response)=>{
					vue.subject_error = response.data;
				});
		},
		deleteCourse:function (course_id) {
			var vue = this;
			if (window.confirm('Are you sure you want to delete this course?')) {
				vue.$http.delete('courses/'+course_id).then(
					(response)=>{
						this.getCourses('courses/get/all');
					}
				);
			}
		},
		editSubject:function (subject_id) {
			var vue = this;
			vue.subject_error = {};
			vue.$http.get('subjects/'+subject_id+'/edit').then(
				(response)=>{
					vue.subject = response.data.subject;
					var prerequisites = response.data.prerequisites;
					
					vue.$http.get('subjects/except/current/'+subject_id).then(
						(response) => {
							
							vue.subjects_option = response.data;
							if (prerequisites.length>0) {
								vue.prerequisites = [];
								$.each(prerequisites,function(index, el) {
									vue.prerequisites.push(el.id);
								});
							}else{
								vue.prerequisites = [0];
							}
							
							$('#edit-subject-modal').modal('show');
						}
					);
				}
			);
		},
		updateSubject:function () {
			var vue = this;
			vue.subject.course_id = vue.course_id;
			vue.subject.prerequisites = vue.prerequisites;
			vue.$http.put('subjects/'+vue.subject.id,vue.subject).then(
				(response)=>{
					vue.viewSubjects(vue.course_id);
					$('#edit-subject-modal').modal('hide');
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated curriculum");
				},(response)=>{
					vue.subject_error = response.data;
				}
			);
		},
		deleteSubject:function (subject_id) {
			var vue = this;
			if (window.confirm("Are you sure you want to delete this subject?")) {
				vue.$http.delete('subjects/'+subject_id).then(
					(response)=>{
						vue.viewSubjects(vue.course_id);
					}
				);
			}
		}
		
	}

});