new Vue({
	el:'#page-deanslist-component',
	data:{
		batch_id:'2016',
		sem_id:'1',
		deansListers:[]
	},
	mounted:function () {
		this.getDeansList();
	},
	methods:{
		getDeansList:function () {
			var vue = this;
			var url = 'get/deanslister/'+vue.batch_id+'/'+vue.sem_id;
			vue.$http.get(url).then(
				(response) => {
					vue.deansListers = response.data;
				}
			);
		}
	}
})