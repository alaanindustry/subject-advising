Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({
	el:'#profile-component',
	data:{
		file: new FormData(),
		username:'',
		basepath:'',
		student_course:'',
		student_year_entry:'',
		year_level:'',
		student:{
			id:'',
			user_id:'',
			school_id:'',
			firstname:'',
			middlename:'',
			lastname:'',
			status:'',
			birthdate:'',
			age:'',
			sex:'',
			contact:'',
			email:'',
			address:'',
			profile_picture:'',
			parent_firstname:'',
			parent_lastname:'',
			parent_address:'',
			parent_contact:'',
			created_at:'',
			updated_at:'',
			username:'',
			password:'',
		},
		student_error:{
			id:'',
			user_id:'',
			school_id:'',
			firstname:'',
			middlename:'',
			lastname:'',
			status:'',
			birthdate:'',
			sex:'',
			contact:'',
			email:'',
			address:'',
			profile_picture:'',
			parent_id:'',
			created_at:'',
			updated_at:'',
			username:'',
			password:'',
		},
		courses:[],
		user:{
			id:'',
			username:''
		},
		credentials:{
			username:'',
			password:'',
			password_confirmation:''
		},
		credentials_error:{
			username:'',
			password:'',
			password_confirmation:''
		}
	},
	mounted:function () {
		var vue = this;
		var url = window.location.href;
		var basepath = window.location.protocol + "//" + window.location.host + "/";
		vue.basepath = basepath;
		var split_url = url.split(basepath+"cas-ied/public/profile/");
		vue.username = split_url[1];
		vue.getStudent(split_url[1]);
	},
	methods:{
		getStudent:function (username) {
			var vue = this;
			vue.$http.get('studentinfo/'+username).then(
				(response) => {
					vue.student = response.data.student;
					vue.student_year_entry = vue.student.batch_id;
					vue.student_course = vue.student.course_id;
					vue.year_level = vue.student.year_level;
					vue.courses = response.data.courses;
					vue.user = response.data.user;
					vue.credentials.username = response.data.user.username;
				}
			);
		},
		updateInfo:function () {
			var vue = this;
			vue.student.username = vue.user.username;
			vue.student.student_course = vue.student_course;
			vue.student.student_year_entry = vue.student_year_entry;
			vue.student.year_level = vue.year_level;
			vue.$http.put('update/user/'+vue.student.id,vue.student).then(
				(response) => {
					vue.student_error = {};
					console.log("Success");
					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("You successfully udpated your profile information");
					// vue.student = response.data.student;
					// vue.courses = response.data.courses;
					// vue.user = response.data.user;
				},(response)=>{
					vue.student_error = response.data;
				}
			);
		},
		changePicture:function (e) {
			var vue = this;
			vue.student_error.profile_picture = '';
			if (e.target.files[0]!=null) {

	            var files = e.target.files;
	            vue.file.append('profile_picture', files[0]);
	            vue.$http.post('student/upload/picture',vue.file).then(
	            	(response) => {
	            		vue.student.profile_picture = response.data;
	            	},(response)=>{
	            		vue.student_error.profile_picture = response.data.profile_picture;
	            		if (vue.student_error.profile_picture==null) {
	            			vue.student_error.profile_picture = ["This file is not acceptable"];
	            		}
	            		vue.student.profile_picture = "";
	            	}
	            );
			}else{
				vue.student.profile_picture = '';
			}
			
		},
		changePassword:function () {
			var vue = this;
			vue.credentials_error = {};
			vue.credentials.password = '';
			vue.credentials.password_confirmation = '';
			vue.$http.get('studentinfo/'+vue.user.username).then(
				(response) => {
					vue.user = response.data.user;
					vue.credentials.username = response.data.user.username;
					$('#new-password-modal').modal('show');
				}
			);
			
		},
		updateCredential:function () {
			var vue = this;
			vue.credentials.id = vue.user.id;
			vue.$http.post('update/credentials/'+vue.user.id,vue.credentials).then(
				(response) => {
					vue.$http.get('studentinfo/'+vue.credentials.username).then(
						(response) => {
							vue.student = response.data.student;
							vue.courses = response.data.courses;
							vue.user = response.data.user;
							vue.credentials.username = response.data.user.username;
							vue.username = response.data.user.username;
							history.pushState(response.data, "", vue.credentials.username);
							$('#new-password-modal').modal('hide');
							var toast = toastr;
							
							toast.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};

							Command: toast["success"]("You successfully udpated your credentials");
						}
					);
					
				},(response) => {
					vue.credentials_error = response.data;
				}
			);
		},
		discardChanges:function () {
			var vue = this;
			vue.getStudent(vue.username);
		}
	}
})