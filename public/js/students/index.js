
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({

	el:'#student-component',
	data:{
		average_gpa:0,
		searchStudent:'',
		deansListYear:'1',
		deansListers:[],
		isSelectAll:false,
		select:'Select All',
		message:{
			api:'MDyoFaq1huko7xLjsnqh',
			number:'',
			message:'',
		},
		bulkmessage:{
			api:'MDyoFaq1huko7xLjsnqh',
			sendTo:[],
			message:''
		},
		sendToStudents:[],
		file: new FormData(),
		file_error:{

		},
		import_grades:{
			course_id:'',
			batch_id:'2016',
			year_id:'1',
			sem_id:'1',
			subject_id:'',
			grade_file:''
		},
		import_grades_error:{
			course_id:'',
			year_id:'1',
			sem_id:'1',
			subject_id:'',
			grade_file:''
		},
		course_id:'',
		student_id:'',
		year_id:'1',
		sem_id:'1',
		student:{
			id:'',
			user_id:'',
			school_id:'',
			firstname:'',
			middlename:'',
			lastname:'',
			status:'',
			birthdate:'',
			age:'',
			sex:'',
			contact:'',
			email:'',
			address:'',
			parent_firstname:'',
			parent_lastname:'',
			parent_address:'',
			parent_contact:'',
			created_at:'',
			updated_at:'',
			username:'',
			password:'',
		},
		student_error:{
			id:'',
			user_id:'',
			school_id:'',
			firstname:'',
			middlename:'',
			lastname:'',
			status:'',
			birthdate:'',
			sex:'',
			contact:'',
			email:'',
			address:'',
			parent_id:'',
			created_at:'',
			updated_at:'',
			username:'',
			password:'',
		},
		grades:{},
		students:[],
		pagination:{
			current_page:1,
			data:[],
			from:1,
			last_page:4,
			next_page_url:"",
			per_page:1,
			prev_page_url:null,
			to:1,
			total:4,
		},
		courses:[],
		subjects:[],
		availables:[],
		forPrintCourse_id:'',
		gradereport:{
			sem_id:'1',
			batch_id:'2016',
			course_id:'',
			subject_id:''
		},
		allgrades:[],
		allgradecourses:[],
		allgradesubjects:[],
		deansList:{
			batch_id:'2016',
			sem_id:'1'
		},
		credentials:{
			id:'',
			username:'',
			password:'',
			password_confirmation:''
		},
		credentials_error:{
			username:'',
			password:'',
			password_confirmation:''
		}
	},
	mounted:function() {
		this.getStudents('students/get/all');
	},
	methods:{
		getStudents:function(url){
			var vue = this;
			vue.$http.get(url).then(
				(response)=>{
					vue.students = response.data.data;
					vue.pagination = response.data;
				}
			);
		},
		editStudent:function (student_id) {
			var vue = this;
			vue.student_error = {};
			vue.$http.get('students/'+student_id+'/edit').then(
				(response) =>{
					vue.student = response.data;
					$('#edit-student-modal').modal('show');
				}
			);
		},
		updateStudent:function () {
			var vue = this;
			vue.$http.put('students/'+vue.student.id,vue.student).then(
				(response) =>{
					vue.students = response.data.data;
					$('#edit-student-modal').modal('hide');

					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated student's Information");

					
				},
				(response)=>{
					var vue = this;
					var errors = response.data;
					$.each(errors, function(index, val) {
						vue.student_error = errors;
					});
				}
			);
		},
		evaluateStudent:function (student_id,course_id,school_id) {
			var vue = this;
			vue.student_id = student_id;
			vue.course_id = course_id;
			vue.forPrintCourse_id = course_id;
			vue.$http.get('students/'+student_id).then(
				(response) => {
					console.log(response.data);
					vue.student = response.data;
					vue.$http.get('students/get/grades/1/1/'+vue.student_id+'/'+course_id).then(

						(response) => {
							vue.grades = response.data.grades;
							vue.average_gpa = response.data.average_gpa;
							vue.$http.get('students/available/subjects/'+school_id+'/'+course_id).then(
								(response) => {
									vue.availables = response.data;
									vue.message.number = vue.student.contact;
									$('#evaluate-student-modal').modal('show');
								}
							);
							console.log(response.data);
						}
					);
				}
			);
			
			
		},
		generateGrade:function () {
			var vue = this;
			vue.$http.get('students/get/grades/'+vue.year_id+'/'+vue.sem_id+'/'+vue.student_id+'/'+vue.forPrintCourse_id).then(

				(response) => {
					vue.average_gpa = response.data.average_gpa;
					vue.grades = response.data.grades;
					console.log(response.data);
				}
			);
		},
		importGrades:function () {
			var vue = this;
			vue.$http.get('courses/get/all').then(

				(response)=>{
					vue.courses = response.data.data;
					vue.import_grades.course_id = response.data.data[0].id;
					vue.$http.get('subjects/get/all/byyearsemcourse/'+vue.import_grades.course_id+'/'+vue.import_grades.year_id+'/'+vue.import_grades.sem_id).then(

						(response)=>{
							vue.subjects = response.data;
							try{
								vue.import_grades.subject_id = response.data[0].id;
							}catch(e){}
							$('#import-grades-modal').modal('show');
						}
					);
				}
			);
		},
		updateOptionSubject:function () {
			var vue = this;
			vue.$http.get('subjects/get/all/byyearsemcourse/'+vue.import_grades.course_id+'/'+vue.import_grades.year_id+'/'+vue.import_grades.sem_id).then(

				(response)=>{
					vue.subjects = response.data;
					try{
						vue.import_grades.subject_id = response.data[0].id;
					}catch(e){
						vue.import_grades.subject_id = '';
					}
					$('#import-grades-modal').modal('show');
				}
			);
		},
		saveImportedGrades:function () {
			var vue = this;
			vue.$http.post('students/import/grades',vue.import_grades).then(
				(response) => {
					vue.import_grades.grade_file = '';
					vue.import_grades_error.grade_file = '';
					$('#import-grades').val(null);
					console.log(response.data);
					$('#import-grades-modal').modal('hide');

					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully imported Grades");
				},
				(response) => {
					vue.import_grades_error = response.data;
				}
			);
		},
		changeFile:function (e) {
			var vue = this;
			$('#btn-save-grades').button('loading');
			vue.import_grades_error.grade_file = '';
			if (e.target.files[0]!=null) {
				var files = e.target.files;
	            vue.file.append('grade_file', files[0]);
	            vue.$http.post('students/upload/grade',vue.file).then(
	            	(response) => {
	            		vue.import_grades.grade_file = response.data;
				$('#btn-save-grades').button('reset');
	            	},(response)=>{
	            		vue.import_grades_error.grade_file = response.data.grade_file;
	            	}
	            );
			}else{
				vue.import_grades.grade_file = '';
			}
		},
		newMessage:function (contact) {
			var vue = this;
			vue.message.number = contact;
			$('#new-message-modal').modal('show');
		},
		sendMessage:function (argument) {
			var vue = this;
			if (vue.message.message!="") {
				vue.$http.post('students/send/message',vue.message).then(
					(response)=>{
						$('#new-message-modal').modal('hide');
						var toast = toastr;

						toast.options = {
						  "closeButton": false,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-bottom-right",
						  "preventDuplicates": false,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};

						Command: toast["success"]("Message successfully sent");
						console.log(response);
					}
				);
			}else{
				alert("Your must enter message befor you can send");
			}
		},
		sendNotification:function (argument) {
			var vue = this;
			if (vue.message.message!="") {
				alert(JSON.stringify(vue.message));
				vue.$http.post('students/send/message',vue.message).then(
					(response)=>{
						$('#new-message-modal').modal('hide');
						var toast = toastr;

						toast.options = {
						  "closeButton": false,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-bottom-right",
						  "preventDuplicates": false,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};

						Command: toast["success"]("Message successfully sent");
						console.log(response);
					}
				);
			}else{
				alert("Your must enter message befor you can send");
			}
		},
		newBulkMessage:function () {
			var vue = this;
			vue.$http.get('courses/get/all').then(
				(response)=>{
					vue.courses = response.data.data;
					vue.course_id = response.data.data[0].id;
					vue.$http.get('students/get/bycourse/'+vue.course_id).then(
						(response) => {
							vue.sendToStudents = response.data;
							$('#new-bulk-message-modal').modal('show');
						}
					);
					
				}
			);
		},
		sendBulkMessage:function () {
			var vue = this;

			if (vue.bulkmessage.sendTo.length>0) {
				if (vue.bulkmessage.message!="") {
					vue.$http.post('students/send/bulkmessage',vue.bulkmessage).then((
						(response)=>{
							$('#new-bulk-message-modal').modal('hide');
							var toast = toastr;

							toast.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};

							Command: toast["success"]("Message successfully sent");
							console.log(response);
						}
					));
				}else{
					alert("Can't send without message");
				}
			}else{
				alert("Cant send without selected conctact");
			}
		},
		changeStudentOptions:function () {
			var vue = this;
			vue.isSelectAll = false;
			vue.select = "Select All";
			vue.bulkmessage.sendTo = [];
			vue.$http.get('students/get/bycourse/'+vue.course_id).then(
				(response) => {
					vue.sendToStudents = response.data;
				}
			);
		},
		selectDiselectStudents:function () {
			var vue = this;
			if (vue.isSelectAll==true) {
					
					vue.isSelectAll = false;
					vue.bulkmessage.sendTo = [];
					vue.select = "Select All";
			}else{
				vue.isSelectAll = true;
				vue.bulkmessage.sendTo = [];
				$.each(vue.sendToStudents, function(index, el) {
					vue.bulkmessage.sendTo.push(el.contact);
				});
				vue.select = "Diselect All";
			}
		},
		showDeansLister:function () {
			var vue = this;
			console.log("sdfsdfsdfsdf");
			vue.$http.get('students/get/deanslister/2016/1').then(
				(response) =>{	
					vue.deansListers = response.data;
					$('#deans-lister-modal').modal('show');
				}
			);
			
		},
		changeDeansLister:function () {
			var vue = this;
			vue.$http.get('students/get/deanslister/'+vue.deansList.batch_id+'/'+vue.deansList.sem_id).then(
				(response) =>{	
					vue.deansListers = response.data;
				}
			);
		},
		printDeansLister:function () {
			var vue = this;
			window.location.replace("students/deanslister/print/"+vue.deansList.batch_id+"/"+vue.deansList.sem_id);
		},
		printGrade:function() {
			var vue = this;
			window.location.replace('students/printgrade/'+vue.student_id+'/'+vue.forPrintCourse_id);
		},
		printGrades:function () {
			var vue = this;
			vue.$http.get('courses/get/all').then(
				(response) => {
					vue.allgradecourses = response.data.data;
					vue.gradereport.course_id = response.data.data[0].id;
					vue.$http.get('subjects/get/subjects/by/course/'+vue.gradereport.course_id).then(
						(response) => {
							vue.allgradesubjects = response.data;
							vue.gradereport.subject_id = vue.allgradesubjects[0].id;
							vue.$http.get('students/printallgrade/'+vue.gradereport.batch_id+'/'+vue.gradereport.sem_id+'/'+vue.gradereport.subject_id+'/'+vue.gradereport.course_id).then(
								(response) => {
									vue.allgrades = response.data;
									$('#generate-grades-report-modal').modal('show');
								}
							);
						}
					);
				}
			);
		},
		changePrintGrades:function (a) {
			var vue = this;
			// vue.$http.get('students/printallgrade/'+vue.gradereport.batch_id+'/'+vue.gradereport.sem_id+'/'+vue.gradereport.subject_id+'/'+vue.gradereport.course_id).then(
			// 	(response) => {
			// 		vue.allgrades = response.data;

			// 		$('#generate-grades-report-modal').modal('show');
			// 	}
			// );
			vue.$http.get('subjects/get/subjects/by/course/'+vue.gradereport.course_id).then(
				(response) => {
					vue.allgradesubjects = response.data;
					if (a!="exception") {
						vue.gradereport.subject_id = vue.allgradesubjects[0].id;
					}
					vue.$http.get('students/printallgrade/'+vue.gradereport.batch_id+'/'+vue.gradereport.sem_id+'/'+vue.gradereport.subject_id+'/'+vue.gradereport.course_id).then(
						(response) => {
							vue.allgrades = response.data;
							$('#generate-grades-report-modal').modal('show');
						}
					);
				}
			);
		},
		printAllGrades:function () {
			var vue = this;
			window.location.replace('students/print/all/grades/'+vue.gradereport.batch_id+'/'+vue.gradereport.sem_id+'/'+vue.gradereport.subject_id+'/'+vue.gradereport.course_id);
		},
		deleteStudent:function (student_id) {
			var vue = this;
			if (window.confirm("Are you sure you want to delete this student?")) {
				vue.$http.delete('students/'+student_id).then(
					(response) => {
						vue.getStudents('students/get/all?page='+vue.pagination.current_page);
					}
				);
			}
		},
		editPassword:function (user_id) {

			var vue = this;
			vue.credentials_error = {};
			vue.credentials.password = '';
			vue.credentials.password_confirmation = '';
			vue.$http.get('students/get/users/by/student/courseid/'+user_id).then(
				(response) => {
					vue.credentials.username = response.data.username;
					vue.credentials.id = response.data.id;
					$('#change-credential-modal').modal('show');
				}
			);
		},
		updateCredential:function (argument) {
			var vue = this;
			vue.$http.post('students/update/credential',vue.credentials).then(
				(response) => {

					$('#change-credential-modal').modal('hide');
					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully Updated Student Credential");
				},(response) =>{
					vue.credentials_error = response.data;
				}
			);
		},
		changePage:function (page) {
			this.getStudents('students/get/all?page='+page);
		},
		previousPage:function () {
			this.getStudents(this.pagination.prev_page_url);
		},
		nextPage:function () {
			this.getStudents(this.pagination.next_page_url);
		},
		search:function () {
			if (this.searchStudent=="") {
				this.getStudents('students/get/all');
			}else{
				this.getStudents('students/get/all/'+this.searchStudent);
			}
		}
	}
});