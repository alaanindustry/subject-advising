new Vue({
	el:'#teacher-evaluation-details-component',
	data:{
		teacher_id:'',
		batch_id:'2016',
		sem_id:'1',
		students:[],
		evaluations:{
			strengths:'',
			weakness:'',
			recommendation:''
		},
		pasts:[],
		teacher:{
			firstname:'',
			lastname:'',
			curriculum:''
		},
		student:{
			firstname:'',
			middlename:'',
			lastname:'',
		},
		evaluation_results:[],
		sum:0,
		average:0,
		basepath:'',
	},
	mounted:function () {
		var url = window.location.href;
		var basepath = window.location.protocol + "//" + window.location.host + "/";
		
		var split_url = url.split(basepath+"cas-ied/public/teachers/get/evaluation/details/");
		var vue = this;
		vue.basepath = basepath;
		vue.teacher_id = split_url[1];
		vue.teacher_id = vue.teacher_id.replace('#', '');
		this.getStudents();
	},
	methods:{
		getStudents:function () {
			var vue = this;
			vue.$http.get('/cas-ied/public/teachers/'+vue.teacher_id).then(
				(response) => {
					vue.teacher = response.data;
					vue.$http.get(vue.basepath+'cas-ied/public/teachers/get/evaluation/more/details/'+vue.batch_id+'/'+vue.sem_id+'/'+vue.teacher_id).then(
						(response) => {
							vue.students = response.data;
						}
					);
				}
			);
		},
		evaluationDetails:function (student_id) {
			var vue = this;
			vue.$http.get(vue.basepath+'cas-ied/public/teachers/get/student/teacher/evaluation/'+vue.batch_id+'/'+vue.sem_id+'/'+vue.teacher_id+'/'+student_id).then(
				(response) => {
					vue.pasts = response.data;
					vue.$http.get(vue.basepath+'cas-ied/public/students/'+student_id+'/'+vue.teacher_id+'/'+vue.batch_id+'/'+vue.sem_id+'/show/this').then(
						(response) => {
							vue.student = response.data.student;
							vue.evaluation_results = response.data.evaluation_results;
							vue.sum = response.data.sum;
							vue.average = response.data.average;
							$('#teacher-evaluation-modal').modal('show');
						}
					);
					
				}
			);
		},
		changeAppraisals:function () {
			this.getStudents();
		}
	}
})