Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({

	el:'#teacher-component',
	data:{
		searchTeacher:'',
		pagination:{
			current_page:1,
			data:[],
			from:1,
			last_page:2,
			next_page_url:null,
			per_page:15,
			prev_page_url:null,
			to:15,
			total:16,
		},
		file:new FormData(),
		batch_id:'2016',
		course_id:'',
		sendToTeachers:[],
		isSelectAll:false,
		select:'Select All',
		bulkmessage:{
			api:'MDyoFaq1huko7xLjsnqh',
			sendTo:[],
			message:''
		},
		sem_id:'1',
		courses:[],
		teacher:{
			id:'',
			profile_picture:'',
			firstname:'',
			lastname:'',
			contact:'',
			degree:'',
			last_graduated:'',
			attainment:'',
			course_id:''
		},
		teacher_error:{
			id:'',
			profile_picture:[],
			firstname:'',
			lastname:'',
			contact:'',
			degree:'',
			last_graduated:'',
			attainment:'',
			course_id:''
		},
		teachers:[],
		appraisals:[]
	},
	mounted:function () {
		this.getTeachers('teachers/get/all');
	},
	methods:{
		resetTeacher:function (argument) {
			this.teacher = {
				id:'',
				profile_picture:'',
				firstname:'',
				lastname:'',
				contact:'',
				degree:'',
				last_graduated:'',
				attainment:'',
				course_id:''
			}
		},
		resetTeacherError:function (argument) {

			this.teacher_error = {
				id:'',
				profile_picture:'',
				firstname:'',
				lastname:'',
				contact:'',
				degree:'',
				last_graduated:'',
				attainment:'',
				course_id:''
			}
		},
		getTeachers:function (url) {
			var vue = this;
			vue.$http.get(url).then(
				(response)=>{
					vue.teachers = response.data.data;
					vue.pagination = response.data;
				}
			);
		},
		newTeacher:function () {
			var vue = this;
			vue.resetTeacher();
			vue.resetTeacherError();
			vue.$http.get('courses/get/all').then(
				(response)=>{
					vue.courses = response.data.data;
					vue.teacher.course_id = response.data.data[0].id;
					$('#new-teacher-modal').modal('show');
				}
			);
		},
		addTeacher:function (argument) {
			var vue = this;
			vue.resetTeacherError();
			if (vue.teacher_error.profile_picture=="") {
				vue.$http.post('teachers',vue.teacher).then(
					(response)=>{
						vue.getTeachers('teachers/get/all');
						$('#new-teacher-modal').modal('hide');
						var toast = toastr;

						toast.options = {
						  "closeButton": false,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-bottom-right",
						  "preventDuplicates": false,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};

						Command: toast["success"]("Successfully added new Teacher");
						vue.resetTeacher();
						
					},(response)=>{
						vue.teacher_error = response.data;
					}
				);
			}else{
				alert(JSON.stringify(vue.teacher_error));
			}
		},
		editTeacher:function (teacher_id) {
			var vue = this;
			vue.teacher_error = {};
			vue.$http.get('teachers/'+teacher_id+'/edit').then(
				(response)=>{
					var course = response.data.course_id;
					vue.teacher = response.data;
					vue.$http.get('courses/get/all').then(
						(response)=>{
							vue.courses = response.data.data;
							vue.teacher.course_id = course;
							$('#edit-teacher-modal').modal('show');
						}
					);
				}
			);
		},
		updateTeacher:function () {
			var vue = this;
			vue.teacher_error = {};
			if (vue.teacher.last_graduated=='0000-00-00') {vue.teacher.last_graduated = '';}
			vue.$http.put('teachers/'+vue.teacher.id,vue.teacher).then(
				(response)=>{
					vue.getTeachers('teachers/get/all?page='+vue.pagination.current_page);
					$('#edit-teacher-modal').modal('hide');
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated Teacher");
					
				},(response)=>{
					vue.teacher_error = response.data;
				}
			);
		},
		viewTeachersEvaluation:function () {
			var vue = this;
			vue.$http.get('teachers/appraisal/'+vue.batch_id+'/'+vue.sem_id).then(
				(response) => {
					vue.appraisals = response.data;
					$('#teacher-appraisal-modal').modal('show');
				}
			);
		},
		changeAppraisals:function () {
			var vue = this;
			vue.$http.get('teachers/appraisal/'+vue.batch_id+'/'+vue.sem_id).then(
				(response) => {
					vue.appraisals = response.data;
				}
			);
		},
		newMessage:function () {
			var vue = this;
			vue.$http.get('courses/get/all').then(
				(response)=>{
					vue.courses = response.data.data;
					vue.course_id = response.data.data[0].id;
					vue.$http.get('teachers/get/bycourse/'+vue.course_id).then(
						(response) => {
							vue.sendToTeachers = response.data;
							$('#new-teacher-message-modal').modal('show');
						}
					);
				}
			);
		},
		sendBulkMessage:function () {
			var vue = this;

			if (vue.bulkmessage.sendTo.length>0) {
				if (vue.bulkmessage.message!="") {
					vue.$http.post('teachers/send/bulkmessage',vue.bulkmessage).then((
						(response)=>{
							$('#new-teacher-message-modal').modal('hide');
							console.log(response);
							var toast = toastr;
							toast.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-bottom-right",
							  "preventDuplicates": false,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};

							Command: toast["success"]("Successfully updated Teacher");
						}
					));
				}else{
					alert("Can't send without message");
				}
			}else{
				alert("Cant send without selected conctact");
			}
		},
		changeTeacherOptions:function () {
			var vue = this;
			vue.isSelectAll = false;
			vue.select = "Select All";
			vue.bulkmessage.sendTo = [];
			vue.$http.get('teachers/get/bycourse/'+vue.course_id).then(
				(response) => {
					vue.sendToTeachers = response.data;
				}
			);
		},
		selectDiselectTeachers:function () {
			var vue = this;
			if (vue.isSelectAll==true) {
					
					vue.isSelectAll = false;
					vue.bulkmessage.sendTo = [];
					vue.select = "Select All";
			}else{
				vue.isSelectAll = true;
				vue.bulkmessage.sendTo = [];
				$.each(vue.sendToTeachers, function(index, el) {
					vue.bulkmessage.sendTo.push(el.contact);
				});
				vue.select = "Diselect All";
			}
		},
		printTeacherEvaluation:function () {
			var vue = this;
			window.location.replace('teachers/print/evaluation/'+vue.sem_id+'/'+vue.batch_id);
		},
		changePicture:function (e) {
			var vue = this;

			vue.teacher_error.profile_picture = '';
			if (e.target.files[0]!=null) {

				var files = e.target.files;
	            vue.file.append('profile_picture', files[0]);
	            vue.$http.post('teachers/upload/picture',vue.file).then(
	            	(response) => {

	            		vue.teacher.profile_picture = response.data;
	            		// alert(vue.teacher.profile_picture);
	            	},(response)=>{
	            		
	            		vue.teacher_error.profile_picture = response.data.profile_picture;

	            		if (vue.teacher_error.profile_picture==null||vue.teacher_error.profile_picture=="") {
	            			vue.teacher_error.profile_picture = ["This file is not acceptable"];
	            		}
	            		vue.teacher.profile_picture = "";
	            		// alert(JSON.stringify(vue.teacher_error));
	            	}
	            );
			}else{
				vue.teacher.profile_picture = '';
				vue.teacher_error.profile_picture = '';
			}

		},
		deleteTeacher:function (teacher_id) {
			var vue = this;
			if (window.confirm("Are you sure you want to delete this teacher")) {
				vue.$http.delete('teachers/'+teacher_id).then(
					(response) =>{
						vue.getTeachers('teachers/get/all');
					}
				);
			}
		},
		teacherEvaluation:function (teacher_id) {
			window.location.replace('teachers/get/evaluation/details/'+teacher_id);
		},
		changePage:function (page) {
			this.getTeachers('teachers/get/all?page='+page);
		},
		previousPage:function () {
			this.getTeachers(this.pagination.prev_page_url);
		},
		nextPage:function () {
			this.getTeachers(this.pagination.next_page_url);
		},
		search:function () {
			if (this.searchTeacher=="") {
				this.getTeachers('teachers/get/all');
			}else{
				this.getTeachers('teachers/get/all/'+this.searchTeacher);
			}
		}
	}

})