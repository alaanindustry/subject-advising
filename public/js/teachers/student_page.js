Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({
	el:'#page-appraisal-component',
	data:{
		teacher_id:'',
		teachers:[],
		evaluations:{
			strengths:'',
			weaknesses:'',
			recommendation:'',
			sem_id:'1',
			batch_id:'2016',
		},
		answers:{},
		questionaires:[],
		batchs:[
			{value:'2014',title:'2014-2015'},
			{value:'2015',title:'2015-2016'},
			{value:'2016',title:'2016-2017'},
			{value:'2017',title:'2017-2018'},
			{value:'2018',title:'2018-2019'},
			{value:'2019',title:'2019-2020'},
		],
		sems:[
			{value:'1',title:'First Semester'},
			{value:'2',title:'Second Semester'},
		],
		pasts:[],
		teachers:{
			firstname:'',
			lastname:'',
			curriculum:'',
		},
		evaluation_results:[],
		sum:'',
		average:''
	},
	mounted:function () {
		this.getTeachers();
	},
	methods:{
		getTeachers:function (url) {
			var vue = this;
			vue.$http.get('teachers/get/all').then(
				(response)=>{
					vue.teachers = response.data.data;
				}
			);
		},
		checkIfEvaluated:function () {
			var vue = this;
			vue.$http.get('checkif/evaluated/'+vue.teacher_id+'/'+vue.evaluations.batch_id+'/'+vue.evaluations.sem_id).then(
				(response) => {
					vue.pasts = response.data;
					if (response.data=='no') {
						vue.pasts = null;
					}else{
						var basepath = window.location.protocol + "//" + window.location.host + "/";
						vue.$http.get(basepath+'cas-ied/public/students/'+vue.teacher_id+'/'+vue.evaluations.batch_id+'/'+vue.evaluations.sem_id+'/show/this/studentpage').then(
							(response) => {
								vue.evaluation_results = response.data.evaluation_results;
								vue.sum = response.data.sum;
								vue.average = response.data.average;
							}
						);
					}
				},(response)=>{
					alert(JSON.stringify(response));
				}
			);

		},
		evaluateTeacher:function (teacher_id) {
			var vue = this;
			$('#btn-evaluate-teacher-'+teacher_id).button('loading');
			vue.answers = [];
			vue.teacher_id = teacher_id;
			vue.$http.get('getquestionaires').then(
				(response) => {
					vue.checkIfEvaluated();
					vue.questionaires = response.data;
					$('#btn-evaluate-teacher-'+teacher_id).button('reset');
					$('#evaluate-teacher-modal').modal('show');
				}
			);
		},
		submitEvaluation:function () {
			var vue = this;
			var counter = 0;
			$.each(vue.answers,function(index, el) {
				if (el!=null) {
					counter++;
				}
			});
			var questionaires_count = 0;
			$.each(vue.questionaires,function(index, el) {
				$.each(el.questionaires,function(index, el) {
					questionaires_count++;
				});
			});

			if (counter==questionaires_count) {
				vue.evaluations.answers = vue.answers;
				vue.evaluations.teacher_id = vue.teacher_id;
				vue.$http.post('storeevaluation',vue.evaluations).then(
					(response) => {
						$('#evaluate-teacher-modal').modal('hide');
						var toast = toastr;
						toast.options = {
						  "closeButton": false,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-bottom-right",
						  "preventDuplicates": false,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};

						Command: toast["success"]("Successfully saved as Evaluation");
					}
				);
			}else{
				alert("Please fill all the questions fields");
			}
		},
		changeScope:function () {
			var vue = this;
			vue.checkIfEvaluated();
		}
	}
})