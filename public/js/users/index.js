
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="_token"]').attr('content');

new Vue({

	el:"#user-component",
	data:{
		user:{
			id:'',
			firstname:'',
			lastname:'',
			username:'',
			password:'',
			password_confirmation:''
		},
		password:{
			password:'',
			password_confirmation:''
		},
		password_error:{
			password:'',
			password_confirmation:''
		},
		user_error:{
			id:'',
			firstname:'',
			lastname:'',
			username:'',
			password:'',
			password_confirmation:''
		},
		users:[]
	},
	mounted:function () {
		this.getUsers();
	},
	methods:{
		getUsers:function () {
			var vue = this;
			vue.$http.get('users/get/all').then(
				(response)=>{
					vue.users = response.data.data;
				}
			);
		},
		editUser:function (user_id) {
			var vue = this;
			vue.user_error = {};
			vue.$http.get('users/'+user_id+'/edit').then(
				(response)=>{
					vue.user = response.data;
					$('#edit-user-modal').modal('show');
				}
			);
		},
		updateUser:function () {
			var vue = this;
			vue.user_error = {};
			vue.$http.put('users/'+vue.user.id,vue.user).then(
				(response)=>{
					vue.getUsers();
					$('#edit-user-modal').modal('hide');
					var toast = toastr;
					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated user's Information");
				},(response)=>{
					vue.user_error = response.data;
				}
			);
		},
		editPassword:function (user_id) {
			var vue = this;
			vue.user.id = user_id;
			$('#edit-password-modal').modal('show');
		},
		updatePassword:function (argument) {
			var vue = this;
			vue.password.id = vue.user.id;
			vue.$http.post('users/updatepassword',vue.password).then(
				(response)=>{
					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated password");
					$('#edit-password-modal').modal('hide');

				},(response)=>{
					vue.password_error = response.data;
				}
			);
		},
		deleteUser:function (user_id) {
			var vue = this;
			if (window.confirm("Are you sure you want to delete this ")) {
				vue.$http.delete('users/'+user_id).then(
					(response)=>{
						vue.getUsers();
					}
				);
			}
		},
		newUser:function () {
			var vue = this;
			$('#new-user-modal').modal('show');
		},
		addUser:function () {
			var vue = this;
			vue.$http.post('users',vue.user).then(
				(response)=>{

					vue.getUsers();
					$('#new-user-modal').modal('hide');
					var toast = toastr;

					toast.options = {
					  "closeButton": false,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-bottom-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					};

					Command: toast["success"]("Successfully updated password");
				},(response)=>{
					vue.user_error = response.data;
				}
			);
		}
	}

})