<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CAS</title>

    <!-- Fonts -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">--}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="shorcut icon" href="img/dnsc_logo.png">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}">
    <meta id="token" name="_token" content="{{ csrf_token() }}">
    <script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .footer {
            padding-top: 38px;
            border-top: 1px solid #b5b5b5;
            height: 300px;
            background-color: #dddddd;
            margin-top: 100px;
            text-align: center;
        }
    </style>
</head>
<body id="app-layout" style="background-image:url({{asset('img/bg.jpg')}})">
    <div class="container-fluid">
        <div class="row">
            <img class="img-fluid" style="width:100%" src="{{asset('img/banner.jpg')}}">
        </div>
    </div>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    CATPES
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if (isset(Auth::user()->role)&&Auth::user()->role=="admin")
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/users') }}">System Users</a></li>
                        <li><a href="{{ url('/students') }}">Students</a></li>
                        <li><a href="{{ url('/courses') }}">Curriculum</a></li>
                        <li><a href="{{ url('/teachers') }}">Teachers</a></li>
                    </ul>
                @elseif(isset(Auth::user()->role)&&Auth::user()->role=="student")
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/mygrades') }}">My Grades</a></li>
                        <li><a href="{{ url('/deanslist') }}">Deans List</a></li>
                        <li><a href="{{ url('/teachersappraisal') }}">Teachers Performance Appraisal</a></li>
                    </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/about') }}">About Us</a></li>
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        @if (isset(Auth::user()->role)&&Auth::user()->role=="admin")
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @elseif(isset(Auth::user()->role)&&Auth::user()->role=="student")
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/profile/'.Auth::user()->username) }}"><i class="glyphicon glyphicon-cog"></i> My Profile</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="container" style="background-color:#f8f8f8;opacity:.8;font-size:15px">
	<div class="page-header" style="text-align:center">
	  	<h1 style="margin-bottom:30px">Bachelor of Secondary Education</h1>
	  	<p>Bachelor of Secondary Education (BSED) is a four year degree program designed to prepare students for becoming high school teachers. The program combines both theory and practice in order to teach students the necessary knowledge and skills a high school teacher needs. The BSEd program trains students to teach one of the different learning areas such as English, Mathematics, Biological Sciences, and Technology and Livelihood Education.</p>
	</div>
	<div class="media">
		<div class="col-md-3">
			<a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/vision.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="col-md-9" style="padding-left:20px">
			<h4 class="media-heading"><b>Vision</b></h4>
		    We envision the Davao del Norte State College:

			A premier institution of higher learning that is imbued with its core values for the development of human resources, and generation and utilization of knowledge and technology for a productive sustainable, just, and humane society.
		</div>
		<!-- <div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/vision.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading"><b>Vision</b></h4>
		    We envision the Davao del Norte State College:

			A premier institution of higher learning that is imbued with its core values for the development of human resources, and generation and utilization of knowledge and technology for a productive sustainable, just, and humane society.
		</div> -->
	</div>
	<hr>
	<div class="media">
		<div class="col-md-3">
			<a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/mission.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="col-md-9" style="padding-left:20px">
			<h4 class="media-heading"><b>Mission</b></h4>
		    As an institution of higher learning and teaching excellence, informed by research and empowered to carry out extension and production services, DNSC shall:
			<br><br>
			1. Provide equitable access quality, relevant, and environment-friendly programs in instruction, research, extension;
			<br>
			2. Promote good governance and adopt mechanisms to continuously upgrade institutional standards;
			<br>
			3. Enhance capabilities and work ethics of the workforce of the institution; and
			<br>
			4. Develop appropriate linkage and network in the implementation of College programs.
		</div>
		<!-- <div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/mission.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading"><b>Mission</b></h4>
		    As an institution of higher learning and teaching excellence, informed by research and empowered to carry out extension and production services, DNSC shall:
			<br><br>
			1. Provide equitable access quality, relevant, and environment-friendly programs in instruction, research, extension;
			<br>
			2. Promote good governance and adopt mechanisms to continuously upgrade institutional standards;
			<br>
			3. Enhance capabilities and work ethics of the workforce of the institution; and
			<br>
			4. Develop appropriate linkage and network in the implementation of College programs.
		</div> -->
	</div>
	<hr>
	<div class="media">
		<div class="col-md-3">
			<a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/goal.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="col-md-9" style="padding-left:20px">
			<h4 class="media-heading"><b>Goal</b></h4>
		    1. Equip teacher education student with requisite knowledge in general education, professional education and specialization courses necessary for effective teaching and learning gared towards the production of a holistically developed Filipino.
			<br>
			2. Provide students with adequate knowledge, skills and values in research and extension to continuously enhance instructional delivery, share best practices and packaged generation technology for commercialization.
		</div>
		<!-- <div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="{{asset('img/goal.jpg')}}" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading"><b>Goal</b></h4>
		    1. Equip teacher education student with requisite knowledge in general education, professional education and specialization courses necessary for effective teaching and learning gared towards the production of a holistically developed Filipino.
			<br>
			2. Provide students with adequate knowledge, skills and values in research and extension to continuously enhance instructional delivery, share best practices and packaged generation technology for commercialization.
		</div> -->
	</div>
</div>
    <div class="footer">
        <div class="container">
            <div class="row">
                Copyright © Davao Del Norte State College 2016
            </div>
        </div>
    </div>
    <!-- JavaScripts -->
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>--}}
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
s