@extends('layouts.app')

@section('content')

<div class="container" id="register-component"> 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(isset($success)&&$success=="1")
                <form role="form" method="POST" action="{{ url('/login ')}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="username" value="{{$username}}">
                    <input type="hidden" name="password" value="{{$password}}">
                    <div class="alert alert-success" role="alert"><p>Succesfully added as new user!<span class="pull-right"><input class="btn btn-default btn-xs" type="submit" value="Go to my home page!"></span></p></div>
                </form>
            @endif
            
            <div class="panel panel-success">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/guest/register') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('school_id') ? ' has-error' : '' }}">
                            <label for="school_id" class="col-md-4 control-label">School ID No.</label>

                            <div class="col-md-6">
                                <input id="school_id" type="text" class="form-control" name="school_id" value="{{ old('school_id') }}" placeholder="Please enter your School Id">

                                @if ($errors->has('school_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('school_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">Firstname</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="Please enter your firstname">

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                            <label for="middlename" class="col-md-4 control-label">Middlename</label>

                            <div class="col-md-6">
                                <input id="middlename" type="text" class="form-control" name="middlename" value="{{ old('middlename') }}" placeholder="Please enter your middlename">

                                @if ($errors->has('middlename'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('middlename') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">Lastname</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="Please enter you lastname">

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sex" class="col-md-4 control-label">Sex</label>

                            <div class="col-md-6">
                                <label class="radio-inline">
                                    <input type="radio" name="sex" checked="" id="inlineRadio1" value="Male"> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="sex" id="inlineRadio2" value="Female"> Female
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <select class="form-control" name="status">
                                    <option selected="">Single</option>
                                    <option>Married</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                            <label for="birthdate" class="col-md-4 control-label">Birthdate</label>

                            <div class="col-md-6">
                                <input id="birthdate" type="date" class="form-control" name="birthdate" value="{{ old('birthdate') }}" >
                                @if ($errors->has('birthdate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Age</label>
                            <div class="col-md-6">
                                <input readonly="" class="form-control" id="age">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                            <label for="contact" class="col-md-4 control-label">Contact No.</label>

                            <div class="col-md-6">
                                <input id="contact" type="text" class="form-control" name="contact" value="{{ old('contact') }}" placeholder="Please enter your contact no.">
                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label"> Address</label>

                            <div class="col-md-6">
                                <input id="address" type="address" class="form-control" name="address" value="{{ old('address') }}" placeholder="Please enter your current adress">

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Please enter your email address">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('parent_firstname') ? ' has-error' : '' }}">
                            <label for="parent_firstname" class="col-md-4 control-label">Parent Firstname</label>

                            <div class="col-md-6">
                                <input id="parent_firstname" type="text" class="form-control" name="parent_firstname" value="{{ old('parent_firstname') }}" placeholder="Please enter your parent firstname">

                                @if ($errors->has('parent_firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('parent_lastname') ? ' has-error' : '' }}">
                            <label for="parent_lastname" class="col-md-4 control-label">Parent Lastname</label>

                            <div class="col-md-6">
                                <input id="parent_lastname" type="text" class="form-control" name="parent_lastname" value="{{ old('parent_lastname') }}" placeholder="Please enter your parent lastname">

                                @if ($errors->has('parent_lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('parent_contact') ? ' has-error' : '' }}">
                            <label for="parent_contact" class="col-md-4 control-label">Parent Contact No.</label>

                            <div class="col-md-6">
                                <input id="parent_contact" type="text" class="form-control" name="parent_contact" value="{{ old('parent_contact') }}" placeholder="Please enter your parent contact no.">

                                @if ($errors->has('parent_contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('parent_address') ? ' has-error' : '' }}">
                            <label for="parent_address" class="col-md-4 control-label">Parent Address</label>

                            <div class="col-md-6">
                                <input id="parent_address" type="text" class="form-control" name="parent_address" value="{{ old('parent_address') }}" placeholder="Please enter your parent address">

                                @if ($errors->has('parent_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Year Entry</label>
                            <div class="col-md-6">
                                <select class="form-control" name="batch_id">
                                    <option value="2014">2014-2015</option>
                                    <option value="2015">2015-2016</option>
                                    <option value="2016">2016-2017</option>
                                    <option value="2017">2017-2018</option>
                                    <option value="2018">2018-2019</option>
                                    <option value="2019">2019-2020</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Year Level</label>
                            <div class="col-md-6">
                                <select class="form-control" name="year_level">
                                    <option value="First Year">First Year</option>
                                    <option value="Second Year">Second Year</option>
                                    <option value="Third Year">Third Year</option>
                                    <option value="Fourth Year">Fourth Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Major</label>

                            <div class="col-md-6">
                                <select class="form-control" name="course_id">
                                    @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Please enter your username">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" value="{{ old('password') }}" name="password" placeholder="Please enter your password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Please confirm your password">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('institutecode') ? ' has-error' : '' }}">
                            <label class="control-label col-md-4">Institute Code</label>
                            <div class="col-md-6">
                                <input  type="text" value="{{ old('institutecode') }}" name="institutecode" class="form-control" placeholder="Please contact your institute chairman for the code">
                                @if ($errors->has('institutecode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('institutecode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript" src="{{asset('js/auth/register.js')}}"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        var dob = new Date($( "#birthdate" ).val());
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        if (age==""||isNaN(age)||age<0) {
            $('#age').val('');
        }else{
            $('#age').val(age);
        }
        $( "#birthdate" ).change(function() {
            var dob = new Date($(this).val());
            var today = new Date();
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
            if (age==""||isNaN(age)||age<0) {
                $('#age').val('');
            }else{
                $('#age').val(age);
            }
            
        });

        // $('#birthdate').datepicker({
        //     onSelect: function(value, ui) {
        //         var today = new Date(),
        //             dob = new Date(value),
        //             age = new Date(today - dob).getFullYear() - 1970;

        //         $('#age').text(age);
        //     },
        //     maxDate: '+0d',
        //     yearRange: '1920:2010',
        //     changeMonth: true,
        //     changeYear: true
        // });

    });
</script>
@endsection
