@extends('layouts.app')

@section('content')
<div id="curriculum-component">
	<div class="all-modals">
		
		<!-- Edit Curriculum -->
		<div class="modal fade" id="edit-course-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">Curriculum Details</h4>
				    </div>
				    <div class="modal-body">
						<form class="form-horizontal" >
							<div class="form-group" v-bind:class="{'has-error':course_error.name}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Curriculum Name</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="course.name" id="inputEmail3" placeholder="Please enter a course name">

							      	<span class="help-block" v-if="course_error.name">
	                                    <strong>@{{ course_error.name[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Curriculum Description</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="course.description" id="inputEmail3" placeholder="Please enter a course description">
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateCourse()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- New Curriculum -->
		<div class="modal fade" id="new-course-modal" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">New Curriculum Form</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal" >
							<div class="form-group" v-bind:class="{'has-error':course_error.name}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Course Name</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="course.name" id="inputEmail3" placeholder="Please enter a course name">

							      	<span class="help-block" v-if="course_error.name">
	                                    <strong>@{{ course_error.name[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Course Description</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="course.description" id="inputEmail3" placeholder="Please enter a course description">
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="addCourse()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Edit Subject -->

		<div class="modal fade" id="edit-subject-modal">
		  <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        <h4 class="modal-title">Subject Details</h4>
			    </div>
			    <div class="modal-body">
			    	<form class="form-horizontal" >
						<div class="form-group" v-bind:class="{'has-error':subject_error.course_no}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Course No.</label>
						    <div class="col-sm-6">
						      <input type="text" class="form-control" v-model="subject.course_no" id="inputEmail3" placeholder="Please enter the course no.">

						      	<span class="help-block" v-if="subject_error.course_no">
                                    <strong>@{{ subject_error.course_no[0] }}</strong>
                                </span>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.title}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Course Title</label>
						    <div class="col-sm-6">
						      <input type="text" class="form-control" v-model="subject.title" id="inputEmail3" placeholder="Please enter a course description">
						      <span class="help-block" v-if="subject_error.title">
                                    <strong>@{{ subject_error.title[0] }}</strong>
                                </span>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.lec}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Lecture</label>
						    <div class="col-sm-6">
						      <input type="text" class="form-control" v-model="subject.lec" id="inputEmail3" placeholder="Please enter lecture units">

						      	<span class="help-block" v-if="subject_error.lec">
                                    <strong>@{{ subject_error.lec[0] }}</strong>
                                </span>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.lab}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Lab</label>
						    <div class="col-sm-6">
						      <input type="text" class="form-control" v-model="subject.lab" id="inputEmail3" placeholder="Please enter lab units">

						      	<span class="help-block" v-if="subject_error.lab">
                                    <strong>@{{ subject_error.lab[0] }}</strong>
                                </span>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.units}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Units</label>
						    <div class="col-sm-6">
						      <input type="text" class="form-control" v-model="subject.units" id="inputEmail3" placeholder="Please enter lab units">
								
						      	<span class="help-block" v-if="subject_error.units">
                                    <strong>@{{ subject_error.units[0] }}</strong>
                                </span>
						    </div>
						</div>
						<div class="form-group">
						    <label for="inputEmail3" class="col-sm-4 control-label">Year</label>
						    <div class="col-sm-6">
						      	<select class="form-control" v-model="subject.year_id">
									<option value="1">First Year</option>
									<option value="2">Second Year</option>
									<option value="3">Third Year</option>
									<option value="4">Fourth Year</option>
								</select>
						    </div>
						</div>
						<div class="form-group">
						    <div class="col-sm-offset-4 col-sm-6">
							    <div class="checkbox">
							        <label>
							          <input v-model="subject.rest" type="checkbox"> Restrict to this year
							        </label>
							    </div>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.lab}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Semester</label>
						    <div class="col-sm-6">
						      	<select class="form-control" v-model="subject.sem_id">
									<option value="1">First Semester</option>
									<option value="2">Second Semester</option>
								</select>
						    </div>
						</div>
						<div class="form-group" v-bind:class="{'has-error':subject_error.prerequisite}">
						    <label for="inputEmail3" class="col-sm-4 control-label">Prerequisits</label>
						    <div class="col-sm-6">
						    	<select class="form-control" multiple v-model="prerequisites">
						      		<option v-bind:value="0">None</option>
									<option v-bind:value="subject1.id" v-for="subject1 in subjects_option">@{{'['+subject1.year_id+' Year,'+subject1.sem_id+' Semester]-'+subject1.course_no}}</option>
								</select>
						    </div>
						</div>
					</form>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary" @click="updateSubject()">Save changes</button>
			    </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- new subject -->
		<div class="modal fade" id="new-subject-modal" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">New Subject Form</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal" >
							<div class="form-group" v-bind:class="{'has-error':subject_error.course_no}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Course No.</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="subject.course_no" id="inputEmail3" placeholder="Please enter the course no.">

							      	<span class="help-block" v-if="subject_error.course_no">
	                                    <strong>@{{ subject_error.course_no[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':subject_error.title}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Course Title</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="subject.title" id="inputEmail3" placeholder="Please enter a course description">
							      <span class="help-block" v-if="subject_error.title">
	                                    <strong>@{{ subject_error.title[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':subject_error.lec}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lecture</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="subject.lec" id="inputEmail3" placeholder="Please enter lecture units">

							      	<span class="help-block" v-if="subject_error.lec">
	                                    <strong>@{{ subject_error.lec[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':subject_error.lab}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lab</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="subject.lab" id="inputEmail3" placeholder="Please enter lab units">

							      	<span class="help-block" v-if="subject_error.lab">
	                                    <strong>@{{ subject_error.lab[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':subject_error.units}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Units</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="subject.units" id="inputEmail3" placeholder="Please enter lab units">
									
							      	<span class="help-block" v-if="subject_error.units">
	                                    <strong>@{{ subject_error.units[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Year</label>
							    <div class="col-sm-6">
							      	<select class="form-control" v-model="subject.year_id">
										<option value="1">First Year</option>
										<option value="2">Second Year</option>
										<option value="3">Third Year</option>
										<option value="4">Fourth Year</option>
									</select>
							    </div>
							</div>
							<div class="form-group">
							    <div class="col-sm-offset-4 col-sm-6">
								    <div class="checkbox">
								        <label>
								          <input v-model="subject.rest" type="checkbox"> Restrict to this year
								        </label>
								    </div>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Semester</label>
							    <div class="col-sm-6">
							      	<select class="form-control" v-model="subject.sem_id">
										<option value="1">First Semester</option>
										<option value="2">Second Semester</option>
									</select>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':subject_error.prerequisite}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Prerequisits</label>
							    <div class="col-sm-6">
							      	<select class="form-control" multiple v-model="new_prerequisites">
							      		<option v-bind:value="0">None</option>
										<option v-bind:value="subject1.id" v-for="subject1 in subjects_option">@{{'['+subject1.year_id+' Year,'+subject1.sem_id+' Semester]-'+subject1.course_no}}</option>
									</select>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="addSubject()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						Curriculums
						<span class="pull-right"><a href="#" @click="newCourse()">Add New</a></span>
					</div>
					<div class="panel-body">
						<table class="table table-reponsive">
							<thead>
								<th></th>
								<th>Curriculum Name</th>
							</thead>
							<tbody>
								<tr v-for="course in courses">
									<td>
										<div class="dropdown">
											<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											    Options
											    <span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
											    <li><a href="#" @click="viewSubjects(course.id)">View Subjects</a></li>
											    <li><a href="#" @click="editCourse(course.id)">Edit Curriculum</a></li>
											    <li role="separator" class="divider"></li>
											    <li><a href="#" @click="deleteCourse(course.id)">Delete Curriculum</a></li>
											</ul>
										</div>
									</td>
									<td>
										@{{course.name}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Subjects - <span class="label label-default">@{{course_name}}</span>
						<input type="hidden" v-model="course_id">
						<span class="pull-right"><a href="#" @click="newSubject()">Add New</a></span>
					</div>
					<div class="panel-body">

						<ul class="nav nav-tabs">
							<li v-bind:class="index==0?'active':''" v-for="(subject_by_year,index) in subjects">
								<a v-bind:href="'#'+(index+1)" data-toggle="tab">@{{subject_by_year.year}}</a>
							</li>
						</ul>
						
						<div class="tab-content ">
							<div class="tab-pane " v-bind:id="(index+1)" v-bind:class="index==0?'active':''" v-for="(subject_by_year,index) in subjects">
								<hr>
								<div class="panel-group" v-bind:id="'accordion'+''+(index+1)" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default"  v-for="(subject_by_sem,index1) in subject_by_year.data">
									    <div class="panel-heading" role="tab" id="headingOne">
										    <h4 class="panel-title">
										        <a role="button" data-toggle="collapse" v-bind:data-parent="'#accordion'+(index+1)" v-bind:href="'#collapseOne'+(index+1)+(index1+1)" v-bind:aria-expanded="false" v-bind:aria-controls="'collapseOne'+(index+1)+(index1+1)">
										        	@{{index1+1}} Semester
										        </a>
										    </h4>
									    </div>
									    <div v-bind:id="'collapseOne'+(index+1)+(index1+1)" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									      	<div class="panel-body">
									        	<table class="table table-reponsive">
													<thead>
														<th></th>
														<th>Course No.</th>
														<th>Course Title</th>
														<th>Lec</th>
														<th>Lab</th>
														<th>Units</th>
														<th>Prerequisite</th>
													</thead>
													<tbody>
														<tr v-for="subject in subject_by_sem.data">
															<td>
																<div class="dropdown">
																	<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																	    Options
																	    <span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
																	    <li><a href="#" @click="editSubject(subject.id)">Edit Subject</a></li>
																	    <li role="separator" class="divider"></li>
																	    <li><a href="#" @click="deleteSubject(subject.id)">Delete Subject</a></li>
																	</ul>
																</div>
															</td>
															<td>@{{subject.course_no}}</td>
															<td>@{{subject.title}}</td>
															<td>@{{subject.lec}}</td>
															<td>@{{subject.lab}}</td>
															<td>@{{subject.units}}</td>
															<td>
																<p v-for="prerequisite in subject.prerequisites">
																	@{{prerequisite.course_no+', '}}
																</p>
																@{{subject.rest==null||subject.rest=='0'?"":subject.year_id+' yr. Standing'}}
															</td>
														</tr>
													</tbody>
												</table>
									      	</div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('js/curriculums/index.js')}}"></script>
@endsection