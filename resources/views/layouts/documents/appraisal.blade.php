<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			font-family: monospace;
		}
		img {
		    width: 215px;
		    margin-top: 20px;
		}
		.header{
			text-align: center;
			padding: 30px;
			font-size: 18px;
		}
		#davao{
			margin-top: 20px;
			font-weight: bold;
			letter-spacing: 3px;
		}
		#evaluation{
			margin-top: 50px;
		}
		.infos{
			margin-top: -7px;
		}
		.info-label{
			width: 80px;
			float: left;
			margin-left: 10px;
			font-weight: bold;
			margin-right: 4px;
		}
		.info-value{
			letter-spacing: 1px;
		}
		.table-grades{
			width: 100%;
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{

			border: 1px solid black;
		}
		.grade-title{
			margin-top: 20px;
		}
		#batch{
			margin-top: 20px;
		}
	</style>
</head>
<body>
	<div class="header">
		<div id="republic"><b>Republic of the Philippines</b></div>
		<img src="{{asset('img/dnsclogo.png')}}">
		<div id="davao">DAVAO DEL NORTE STATE</div>
		<div id="batch"><b>{{$sem.' Semester - '.$batch.'-'.($batch+1)}}</b></div>
		<div id="evaluation">Teachers' List of Evaluation Result</div>
	</div>
	<div id="data">
		<table class="table-grades">
			<thead>
				<th>Teacher Name</th>
				<th>Curriculum</th>
				<th>Average</th>
			</thead>
			<tbody>
				@foreach($appraisals as $appraisal)
					<tr>
						<td>{{$appraisal->name}}</td>
						<td>{{$appraisal->course}}</td>
						<td>{{$appraisal->average==""?"N/A":$appraisal->average}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</body>
</html>