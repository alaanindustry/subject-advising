<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			font-family: monospace;
		}
		img {
		    width: 215px;
		    margin-top: 20px;
		}
		.header{
			font-size: 18px;
			text-align: center;
			padding: 30px;
		}
		#davao{
			margin-top: 20px;
			font-weight: bold;
			letter-spacing: 3px;
		}
		#evaluation{
			margin-top: 50px;
		}
		.infos{
			margin-top: -7px;
		}
		.info-label{
			width: 80px;
			float: left;
			margin-left: 10px;
			font-weight: bold;
			margin-right: 4px;
		}
		.info-value{
			letter-spacing: 1px;
		}
		.table-grades{
			width: 100%;
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{

			border: 1px solid black;
		}
		.grade-title{
			margin-top: 20px;
		}
		#batch{
			margin-top: 10px;
		}
		#course{
			margin-top: 10px;
			font-weight: bold;
		}

		#subject{
			margin-top: 10px;
			font-weight: bold;	
		}
	</style>
</head>
<body>
	<div class="header">
		<div id="republic"><b>Republic of the Philippines</b></div>
		<img src="{{asset('img/dnsclogo.png')}}">
		<div id="davao">DAVAO DEL NORTE STATE</div>
		<div id="batch">{{'Batch '.$year_id.'-'.($year_id+1)}}</div>
		<div id="batch">{{$sem_id.' Semester'}}</div>
		<div id="course">{{'Major: '.$course}}</div>
		<div id="subject">{{'Subject Description: '.$subject}}</div>
		<div id="evaluation">List of Students Grades</div>
	</div>
	<div id="data">
		<table class="table-grades">
			<thead>
				<th>Student ID</th>
				<th>Lastname</th>
				<th>Firstname</th>
				<th>Middlename</th>
				<th>Equivalent</th>
				<th>Remarks</th>
			</thead>
			<tbody>
				@foreach($grades as $grade)
					<tr>
						<td>{{$grade->school_id}}</td>
						<td>{{$grade->lastname}}</td>
						<td>{{$grade->firstname}}</td>
						<td>{{$grade->middlename}}</td>
						<td>{{$grade->equivalent==null?"N\A":$grade->equivalent}}</td>
						<td>{{$grade->equivalent<=3?"Passed":($grade->equivalent==null?"N/A":($grade->equivalent>4?"Failed":"Conditional/INC"))}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</body>
</html>