﻿@extends('layouts.app')

@section('content')
	<div id="settings-component">
		<div class="all-modals">
			<div class="modal fade" id="edit-institute-code-modal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
				    <div class="modal-content">
					    <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Institute Code Details</h4>
					    </div>
					    <div class="modal-body">
					    	<form class="form-horizontal">
					    		<div class="form-group">
					    			<label class="control-label col-md-4">Institute Code</label>
					    			<div class="col-md-6">
						    			<input type="text" v-model="institutecode.code" class="form-control">
						    		</div>
					    		</div>
					    	</form>
					    </div>
					    <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary" @click="updateInstituteCode()">Save changes</button>
					    </div>
				    </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		</div>
		<div class="container">
			<div class="row">
				<div class="panel panel-default ">
					<div class="panel-heading">
						Settings
						<span class="pull-right">
							<button class="btn btn-default btn-xs" @click="editInstituteCode()">Institute Code</button>
						</span>
					</div>
					<div class="panel-body">
						<div class="col-md-6">
							<form class="form-horizontal" v-on:submit.prevent>
					    		<div class="form-group" v-bind:class="{'has-error':admin_error.firstname}">
					    			<label class="control-label col-md-4">Firstname</label>
					    			<div class="col-md-6">
						    			<input type="text" class="form-control" v-model="admin.firstname" placeholder="Please enter your firstname">
						    			<span class="help-block" v-if="admin_error.firstname">
		                                    <strong>@{{ admin_error.firstname[0] }}</strong>
		                                </span>
						    		</div>
					    		</div>
					    		<div class="form-group" v-bind:class="{'has-error':admin_error.lastname}">
					    			<label class="control-label col-md-4">Lastname</label>
					    			<div class="col-md-6">
						    			<input type="text" class="form-control" v-model="admin.lastname" placeholder="Please enter your lastname">
						    			<span class="help-block" v-if="admin_error.lastname">
		                                    <strong>@{{ admin_error.lastname[0] }}</strong>
		                                </span>
						    		</div>
					    		</div>
					    		<div class="form-group" v-bind:class="{'has-error':admin_error.username}">
					    			<label class="control-label col-md-4">Username</label>
					    			<div class="col-md-6">
						    			<input type="text" class="form-control" v-model="admin.username" placeholder="Please enter username">
						    			<span class="help-block" v-if="admin_error.username">
		                                    <strong>@{{ admin_error.username[0] }}</strong>
		                                </span>
						    		</div>
					    		</div>
					    		<div class="form-group" v-bind:class="{'has-error':admin_error.password}">
					    			<label class="control-label col-md-4">New Password</label>
					    			<div class="col-md-6">
						    			<input type="password" class="form-control" v-model="admin.password" placeholder="Please enter new password">
						    			<span class="help-block" v-if="admin_error.password">
		                                    <strong>@{{ admin_error.password[0] }}</strong>
		                                </span>
						    		</div>
					    		</div>
					    		<div class="form-group" v-bind:class="{'has-error':admin_error.password.password_confirmation}">
					    			<label class="control-label col-md-4">Confirm New Password</label>
					    			<div class="col-md-6">
						    			<input type="password" class="form-control" v-model="admin.password_confirmation" placeholder="Please confirm your new password">
						    			<span class="help-block" v-if="admin_error.password_confirmation">
		                                    <strong>@{{ admin_error.password_confirmation[0] }}</strong>
		                                </span>
						    		</div>
					    		</div>
					    		<div class="form-group">
					    			<div class="col-md-6 col-md-offset-4">
						    			<button class="btn btn-primary" @click="updateAdminCredential()">Save Changes</button>
						    			<button class="btn btn-default" @click="discardChanges()">Discard Changes</button>
						    		</div>
					    		</div>
					    	</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ asset('js/auth/settings.js') }}"></script>
@endsection
