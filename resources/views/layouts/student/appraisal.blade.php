@extends('layouts.app')

@section('content')
<div id="page-appraisal-component">
	<!-- all modals -->
	<div class="all-modals">
		<!-- Evalutae Teacher -->
		
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					List of Teachers
				</div>
				<div class="panel-body">
					<table class="table table-responsive">
						<thead>
							<th></th>
							<th>Teachers Picture</th>
							<th>Firstname</th>
							<th>Lastname</th>
						</thead>
						<tbody>
							<tr v-for="teacher in teachers">
								<td>
									<div class="dropdown">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Options
										    <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" @click="evaluateTeacher(teacher.id)">Evaluate Teacher</a></li>
										</ul>
									</div>
								</td>
								<td></td>
								<td>@{{teacher.firstname}}</td>
								<td>@{{teacher.lastname}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('js/teachers/student_page.js')}}"></script>
@endsection