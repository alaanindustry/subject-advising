@extends('layouts.app')

@section('content')
	
<div class="grades-page-component">
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					My Grades
				</div>
				<div class="panel-body">
					@foreach ($student_grades as $student_grade)
						<?php if (is_array($student_grade)): ?>
							<div class="panel panel-default">
								<div class="panel-heading">
									{{$student_grade['name']}} Year
								</div>
								<div class="panel-body">
									@foreach($student_grade as $stud_grade)
										<?php if (is_array($stud_grade)): ?>
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">
														{{$stud_grade['name']}} Semester
													</div>
													<div class="panel-body">
														<table class="table table-responsive">
															<thead>
																<th>Course No.</th>
																<th>Coursen Title</th>
																<th>Grade</th>
																<th>Units</th>
															</thead>
															<tbody>
																@foreach($stud_grade as $grade)
																	<?php if (is_object($grade)): ?>
																		<tr>
																			<td>{{$grade->course_no}}</td>
																			<td>{{$grade->title}}</td>
																			<td>{{$grade->equivalent==null?"None":$grade->equivalent}}</td>
																			<td>{{$grade->units==null?"None":$grade->units}}</td>
																		</tr>
																	<?php endif ?>
																	
																@endforeach
																<tr>
																	<td></td>
																	<td></td>
																	<td></td>
																</tr>
																
																<tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										<?php endif ?>
									@endforeach
								</div>
							</div>
						<?php endif ?>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

@endsection