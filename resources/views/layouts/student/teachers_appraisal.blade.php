@extends('layouts.app')

@section('content')
<div id="page-appraisal-component">
	<div class="all-modals">
		<div class="modal fade" id="evaluate-teacher-modal">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">PERFORMANCE APPRAISAL FOR FACULTY</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-inline" role="form">
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Semester</label>
							    <select class="form-control" @change="changeScope" v-model="evaluations.sem_id">
							    	<option v-bind:value="sem.value" v-for="sem in sems">@{{sem.title}}</option>
							    	<!-- <option v-bind:value="2">Second Semester</option> -->
							    </select>
							</div>
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">School Year</label>
							    <select class="form-control" @change="changeScope" v-model="evaluations.batch_id">
							    	<option v-bind:value="batch.value" v-for="batch in batchs">@{{batch.title}}</option>
							    	<!-- <option v-bind:value="2017">2017-2018</option>
							    	<option v-bind:value="2018">2018-2019</option>
							    	<option v-bind:value="2019">2019-2020</option> -->
							    </select>
							</div>
						</form>
						<hr>
						<div v-show="!pasts">
							<strong>Direction: </strong><ins> Please rate your instruction or professor honestly and sincerely by checking the check box which corresponds to your choice of description as indicated below:</ins>
							<br>
						</div>
				    	<div v-show="pasts">
				    		<div class="alert alert-info" role="alert">Notification: Done Evaluating this teacher in this semester. Please see the rates.</div>
				    	</div>
				    	<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[1] - never (not at all)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[2] - Seldom (rarely)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[3] - Often (Sometimes)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[4] - Very often (Most of the times)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[5] - Always (All the times

						  </label>
						</div>
				    	<hr>
				    	<form role="form" v-if="!pasts">

				    		<div class="form-group">
				    			<label class="control-label">I. Instructional Competence</label>
				    		</div>
				    		<div v-for="questionaire in questionaires">
				    			<hr>
				    			<div class="form-group">
					    			<label class="control-label">@{{questionaire.name}}</label>
					    		</div>
					    		<hr>
					    		<div class="form-group" v-for="(question,index) in questionaire.questionaires">
								    <p class=" control-label"><b>@{{index+1}}. </b>@{{question.description}}</p>
								    <div>
								    	<label class="checkbox-inline">
										  	<input type="radio" v-bind:name="question.id" v-bind:value="question.id+'_'+questionaire.id+'_'+'1'" id="inlineCheckbox1" v-model="answers[question.id]"> 1
										</label>
										<label class="checkbox-inline">
										  	<input type="radio" v-bind:name="question.id" v-bind:value="question.id+'_'+questionaire.id+'_'+'2'" id="inlineCheckbox2" v-model="answers[question.id]"> 2
										</label>
										<label class="checkbox-inline">
										  	<input type="radio" v-bind:name="question.id" v-bind:value="question.id+'_'+questionaire.id+'_'+'3'" id="inlineCheckbox3" v-model="answers[question.id]"> 3
										</label>
										<label class="checkbox-inline">
										  	<input type="radio" v-bind:name="question.id" v-bind:value="question.id+'_'+questionaire.id+'_'+'4'" id="inlineCheckbox3" v-model="answers[question.id]"> 4
										</label>
										<label class="checkbox-inline">
										  	<input type="radio" v-bind:name="question.id" v-bind:value="question.id+'_'+questionaire.id+'_'+'5'" id="inlineCheckbox3" v-model="answers[question.id]"> 5
										</label>
								    </div>
								</div>
				    		</div>
				    		
							<hr>
							<div class="form-group">
				    			<label class="control-label">Additional Comments</label>
				    		</div>
				    		<hr>
							<div class="form-group">
								<label class="control-label">1. Strenghts</label>
								<textarea class="form-control" v-model="evaluations.strengths" rows="4"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">2. Weakness</label>
								<textarea class="form-control" v-model="evaluations.weaknesses" rows="4"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">3. Recommendation</label>
								<textarea class="form-control" v-model="evaluations.recommendation" rows="4"></textarea>
							</div>
				    	</form>
				    	<form v-if="pasts">
				    		<p style="margin-left:20px" v-for="evaluation_result in evaluation_results">@{{evaluation_result.name+': '+evaluation_result.sum}}</p>
					    	<p style="margin-left:20px">Total: @{{sum}} out of 150</p>
					    	<p style="margin-left:20px">Average: @{{average}}</p>
					    	<br><br>
				    		<div class="container">
				    			<div class="form-group">
					    			<label class="control-label">I. Instructional Competence </label>
					    		</div>
				    		</div>
				    		<div v-for="(past,index) in pasts">
		    					<hr>
				    			<div class="form-group">
					    			<label class="control-label">@{{past.name}}</label>
					    		</div>
					    		<hr>
					    		<div class="form-group" v-for="(question,index) in past.questionaires">
					    			<p class=" control-label"><b>@{{index+1}}. </b>@{{question.description}}</p>
					    			<p style="margin:20px"><b>Rate: @{{question.answer.rate}}</b></p>
					    		</div>
				    		</div>
				    		<hr>
							<div class="form-group">
				    			<label class="control-label">Additional Comments</label>
				    		</div>
				    		<hr>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">1. Strenghts</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.strengths}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">2. Weaknesses</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.weaknesses}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">3. Recommendation</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.recommendation}}</textarea>
										</div>
									</div>
								</div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button v-if="!pasts" type="button" class="btn btn-primary" @click="submitEvaluation()">Submit Evaluation</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					List of Teachers
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6 col-md-3" v-for="teacher in teachers">
						    <div class="thumbnail" >
							    <img v-bind:src="'/cas-ied/public/storage/public/'+teacher.profile_picture" alt="...">
							      <div class="caption">
							        <h4>@{{teacher.firstname+' '+teacher.lastname}}</h4>
							        <p>@{{teacher.name}}</p>
							        <hr>
							        <p><a data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Please wait.." v-bind:id="'btn-evaluate-teacher-'+teacher.id" type="button" @click="evaluateTeacher(teacher.id)" class="btn btn-primary" role="button">Evaluate Teacher</a></p>
							    </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('js/teachers/student_page.js')}}"></script>
@endsection