@extends('layouts.app')

@section('content')

<div id="student-component">
	<div class="all-modals">
		
		<!-- Edit Student Credential -->
		<div class="modal fade" id="change-credential-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Change Student Credential</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal">
				    		<div class="form-group" v-bind:class="{'has-error':credentials_error.username}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Username</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="credentials.username" id="inputEmail3" placeholder="Please enter your username">
							      	<span class="help-block" v-if="credentials_error.username">
	                                    <strong>@{{ credentials_error.username[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':credentials_error.password}">
							    <label for="inputEmail3" class="col-sm-4 control-label">New Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="credentials.password" id="inputEmail3" placeholder="Please enter your new password">
							    	<span class="help-block" v-if="credentials_error.password">
	                                    <strong>@{{ credentials_error.password[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':credentials_error.password_confirmation}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Confirm New Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="credentials.password_confirmation" id="inputEmail3" placeholder="Please confirm your new password">
									<span class="help-block" v-if="credentials_error.password_confirmation">
	                                    <strong>@{{ credentials_error.password_confirmation[0] }}</strong>
	                                </span>
							    </div>
							</div>
						</form>	
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateCredential()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Generate Grades report -->
		<div class="modal fade" id="generate-grades-report-modal">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">Generate Grades Report</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-inline" role="form">
						  	<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Sem</label>
							    <select class="form-control input-sm" v-model="gradereport.sem_id" @change="changePrintGrades">
							    	<option v-bind:value="1">First Semester</option>
							    	<option v-bind:value="2">Second Semester</option>
							    </select>
						  	</div>
						  	<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">SY</label>
							    <select class="form-control input-sm" v-model="gradereport.batch_id" @change="changePrintGrades">
							    	<option v-bind:value="2014">2014-2015</option>
							    	<option v-bind:value="2015">2015-2016</option>
							    	<option v-bind:value="2016">2016-2017</option>
								    <option v-bind:value="2017">2017-2018</option>
								    <option v-bind:value="2018">2018-2019</option>
								    <option v-bind:value="2019">2019-2020</option>
							    </select>
						  	</div>
						  	<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Curriculum</label>
							    <select class="form-control input-sm" v-model="gradereport.course_id" @change="changePrintGrades">
							    	<option v-bind:value="allgradecourse.id" v-for="allgradecourse in allgradecourses">@{{allgradecourse.name}}</option>
							    </select>
						  	</div>
						  	<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Subject</label>
							    <select class="form-control input-sm" v-model="gradereport.subject_id" @change="changePrintGrades('exception')">
							    	<option v-bind:value="allgradesubject.id" v-for="allgradesubject in allgradesubjects">@{{allgradesubject.course_no}}</option>
							    </select>
						  	</div>
						  	<div class="form-group">
						  		<a href="#" @click="printAllGrades()">Print</a>
						  	</div>
						</form>
						<hr>
						<table class="table table-reponsive">
							<thead>
								<th>Student ID</th>
								<th>Lastname</th>
								<th>Firstname</th>
								<th>Middlename</th>
								<th>Grade Equivalent</th>
								<th>Remarks</th>
							</thead>
							<tbody>
								<tr v-for="grade in allgrades" v-bind:class="{'warning':grade.equivalent>3.0&&grade.equivalent<5,'danger':grade.equivalent>=5.0&&grade.grade!=null}">
									<td>@{{grade.school_id}}</td>
									<td>@{{grade.lastname}}</td>
									<td>@{{grade.firstname}}</td>
									<td>@{{grade.middlename}}</td>
									<td>@{{grade.equivalent==null?"N\A":grade.equivalent}}</td>
									<td>@{{(grade.equivalent<=3.0&&grade.equivalent!=null)?"Passed":grade.equivalent==null?"N/A":grade.equivalent>4?"Failed":"Conditional/INC"}}</td>
								</tr>
							</tbody>
						</table>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="printGradeReport()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Deans Lister -->
		
		<div class="modal fade" id="deans-lister-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Deans List</h4>
				      </div>
				    <div class="modal-body">
				    	<form class="form-inline">
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail3">Batch Year</label>
							    <select class="form-control" v-model="deansList.batch_id" @change="changeDeansLister">
							    	<option v-bind:value="2014">2014-2015</option>
							    	<option v-bind:value="2015">2015-2016</option>
							    	<option v-bind:value="2016">2016-2017</option>
							    	<option v-bind:value="2017">2017-2018</option>
							    	<option v-bind:value="2018">2018-2019</option>
							    	<option v-bind:value="2019">2019-2020</option>
							    </select>
							</div>
							<div class="form-group">
							    <label class="control-label" for="exampleInputPassword3">Semester</label>
							    <select class="form-control" v-model="deansList.sem_id" @change="changeDeansLister">
							    	<option value="1">First Semester</option>
							    	<option value="2">Second Semester</option>
							    </select>
							</div>
						</form>
						<hr>
				    	<div class="panel panel-default">
				    		<div class="panel-heading">
				    			All Deans Lister
				    			<span class="pull-right"><a href="#" @click="printDeansLister()">Print</a></span>
				    		</div>
				    		<div class="panel-body">
				    			<table class="table table-responsive">
						    		<thead>
						    			<th>Student ID</th>
						    			<th>Name</th>
						    			<th>Major</th>
						    			<th>Year</th>
						    			<!-- <th>Semester</th> -->
						    			<th>GPA</th>
						    			<th>Rank</th>
						    		</thead>
						    		<tbody>
						    			<tr v-for="deanslister in deansListers.first">
						    				<td>@{{deanslister.school_id}}</td>
						    				<td>@{{deanslister.firstname+' '+deanslister.middlename+' '+deanslister.lastname}}</td>
						    				<td>@{{deanslister.name}}</td>
						    				<td>@{{deanslister.year}}</td>
						    				<!-- <td>@{{deanslister.sem}}</td> -->
						    				<td>@{{deanslister.gpa}}</td>
						    				<td>@{{"First Honor"}}</td>
						    			</tr>
						    			<tr v-for="deanslister in deansListers.second">
						    				<td>@{{deanslister.school_id}}</td>
						    				<td>@{{deanslister.firstname+' '+deanslister.middlename+' '+deanslister.lastname}}</td>
						    				<td>@{{deanslister.name}}</td>
						    				<td>@{{deanslister.year}}</td>
						    				<!-- <td>@{{deanslister.sem}}</td> -->
						    				<td>@{{deanslister.gpa}}</td>
						    				<td>@{{"Second Honor"}}</td>
						    			</tr>
						    		</tbody>
						    	</table>
				    		</div>
				    	</div>
				    	
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Bulk Message -->
		<div class="modal fade" tabindex="-1" id="new-bulk-message-modal" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Send Message</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-inline">
							<div class="form-group">
							    <label class="sr-only" for="exampleInputEmail3">Email address</label>
							    <select class="form-control" @change="changeStudentOptions" v-model="course_id">
							    	<option v-bind:value="course.id" v-for="course in courses">@{{course.name}}</option>
							    </select>
							</div>
						</form>
						<hr>
						<div class="form-group">
							<label class="control-label col-xs-2">Message</label>
							<div class="col-xs-10">
								<textarea v-model="bulkmessage.message" class="form-control"></textarea>
							</div>
						</div><br><hr>
						<div style="margin-left:20px">
							<div class="checkbox col-xs-offset-2">
					      <label>
					        <input type="checkbox" v-bind:checked="isSelectAll" @change="selectDiselectStudents"> @{{select}}
					      </label>
					    </div>	
						</div>
						
						<ul class="list-group col-xs-offset-2">
						  <div class="container">
						  	<div class="checkbox" v-for="student in sendToStudents">
							    <label>
							        <input v-bind:value="student.contact" v-model="bulkmessage.sendTo" v-bind:id="student.contact" type="checkbox"> @{{student.firstname+' '+student.middlename+' '+student.lastname+' - '+student.contact}}
							    </label>
						    </div>
						  </div>
						</ul>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="sendBulkMessage()">Send Message </button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
			
		<!-- SMS -->

		<div class="modal fade" id="new-message-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Send a Message</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Number</label>
							    <div class="col-sm-6">
							      <input type="email" readonly="" v-model="message.number" class="form-control" id="inputEmail3" placeholder="Email">
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Message</label>
							    <div class="col-sm-6">
							      	<textarea class="form-control" v-model="message.message">
							      	
							      	</textarea>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="sendMessage()">Send Message</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Edit student -->
		<div class="modal fade" id="edit-student-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Student Details</h4>
				    </div>
				    <div class="modal-body">
				        <form class="form-horizontal">
				        	<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Profile Picture</label>
							    <div class="col-sm-5">
							      <img style="width:150px;height:150px" v-bind:src="'/cas-ied/public/storage/public/'+student.profile_picture"  alt="" id="product-image-preview" class="img-thumbnail">
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':student_error.school_id}">
	                            <label for="school_id" class="col-md-4 control-label">School ID No.</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="school_id" type="text" class="form-control" name="school_id" v-model="student.school_id" placeholder="Please enter your School Id">
									<span class="help-block" v-if="student_error.school_id">
	                                        <strong>@{{ student_error.school_id[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.firstname}">
	                            <label for="firstname" class="col-md-4 control-label">Firstname</label>

	                            <div class="col-md-6">
	                                <input  readonly="" id="firstname" type="text" class="form-control" name="firstname" v-model="student.firstname" placeholder="Please enter your firstname">
									
									<span class="help-block" v-if="student_error.firstname">
	                                        <strong>@{{ student_error.firstname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.middlename}">
	                            <label for="middlename" class="col-md-4 control-label">Middlename</label>

	                            <div class="col-md-6">
	                                <input  readonly="" id="middlename" type="text" class="form-control" name="middlename" v-model="student.middlename" placeholder="Please enter your middlename">

	                                <span class="help-block" v-if="student_error.middlename">
	                                        <strong>@{{ student_error.middlename[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.lastname}">
	                            <label for="lastname" class="col-md-4 control-label">Lastname</label>

	                            <div class="col-md-6">
	                                <input  readonly="" id="lastname" type="text" class="form-control" name="lastname" v-model="student.lastname" placeholder="Please enter you lastname">

	                                <span class="help-block" v-if="student_error.lastname">
	                                        <strong>@{{ student_error.lastname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label for="sex" class="col-md-4 control-label">Sex</label>

	                            <div class="col-md-6">
	                                <input readonly="" type="text" class="form-control" v-model="student.sex">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label for="name" class="col-md-4 control-label">Status</label>

	                            <div class="col-md-6">
	                                <input readonly="" type="text" class="form-control" v-model="student.status">
	                            </div>
	                        </div>

	                        <div class="form-group " v-bind:class="{'has-error':student_error.birthdate}">
	                            <label for="birthdate" class="col-md-4 control-label">Birthdate</label>

	                            <div class="col-md-6">
	                                <input  readonly="" id="birthdate" type="date" class="form-control" name="birthdate" v-model="student.birthdate" >

	                                <span class="help-block" v-if="student_error.birthdate">
	                                        <strong>@{{ student_error.birthdate[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                        	<label class="col-md-4 control-label">Age</label>
	                        	<div class="col-md-6">
	                        		<input type="text" readonly="" class="form-control" v-model="student.age">
	                        	</div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.contact}">
	                            <label for="contact" class="col-md-4 control-label">Contact No.</label>

	                            <div class="col-md-6">
	                                <input  readonly="" id="contact" type="text" class="form-control" name="contact" v-model="student.contact" placeholder="Please enter your contact no.">
	                                
	                                <span class="help-block" v-if="student_error.contact">
	                                        <strong>@{{ student_error.contact[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>


	                        <div class="form-group" v-bind:class="{'has-error':student_error.address}">
	                            <label for="address" class="col-md-4 control-label"> Address</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="address" type="address" class="form-control" name="address" v-model="student.address" placeholder="Please enter your current adress">

	                                
	                                <span class="help-block" v-if="student_error.address">
	                                        <strong>@{{ student_error.address[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.email}">
	                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="email" type="text" class="form-control" name="email" v-model="student.email" placeholder="Please enter your email address">
	                                
	                                <span class="help-block" v-if="student_error.email">
	                                        <strong>@{{ student_error.email[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_firstname}">
	                            <label for="parent_firstname" class="col-md-4 control-label">Parent Firstname</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="parent_firstname" type="text" class="form-control" name="parent_firstname" v-model="student.parent_firstname" placeholder="Please enter your parent firstname">

	                                <span class="help-block" v-if="student_error.parent_firstname">
	                                        <strong>@{{ student_error.parent_firstname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_lastname}">
	                            <label for="parent_lastname" class="col-md-4 control-label">Parent Lastname</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="parent_lastname" type="text" class="form-control" name="parent_lastname" v-model="student.parent_lastname" placeholder="Please enter your parent lastname">

	                                <span class="help-block" v-if="student_error.parent_lastname">
	                                        <strong>@{{ student_error.parent_lastname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_contact}">
	                            <label for="parent_contact" class="col-md-4 control-label">Parent Contact No.</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="parent_contact" type="text" class="form-control" name="parent_contact" v-model="student.parent_contact" placeholder="Please enter your parent contact no.">

	                                <span class="help-block" v-if="student_error.parent_contact">
	                                        <strong>@{{ student_error.parent_contact[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_address}">
	                            <label for="parent_address" class="col-md-4 control-label">Parent Address</label>

	                            <div class="col-md-6">
	                                <input readonly="" id="parent_address" type="text" class="form-control" name="parent_address" v-model="student.parent_address" placeholder="Please enter your parent address">

	                                <span class="help-block" v-if="student_error.parent_address">
	                                        <strong>@{{ student_error.parent_address[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <!-- <button type="button" class="btn btn-primary" @click="updateStudent()">Save changes</button> -->
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Evaluate student -->
		<div class="modal fade" data-focus-on="input:first" id="evaluate-student-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content panel-danger">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Evaluate Student</h4>
				    </div>
				    <div class="modal-body">
				    	<p style="text-align:center;font-size:20px"><b>@{{student.firstname+' '+student.middlename+' '+student.lastname}}</b></p>
				    	<div class="alert alert-danger" role="alert" v-if="availables.is_adviced_to_shift==true">Oh snap! This student had reach a total of 9 units of failed subjects. And therefore is advised to shift course.</div>
				    	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingOne">
								    <h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								          	History of Grades
								        </a>
								        <span class="pull-right"><a href="#" @click="printGrade()">Print</a></span>
								    </h4>
							    </div>
							    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

							      	<div class="panel-body">
							      		<form class="form-inline">
											<div class="form-group">
											    <label class="sr-only" for="exampleInputEmail3">Year</label>
											   	<select class="form-control" v-model="year_id">
												  <option value="1">First Year</option>
												  <option value="2">Second Year</option>
												  <option value="3">Third Year</option>
												  <option value="4">Fourth Year</option>
												</select>
											</div>
											<div class="form-group">
											    <label class="sr-only" for="exampleInputPassword3">Semester</label>
											    <select class="form-control" v-model="sem_id">
												  <option value="1">First Semester</option>
												  <option value="2">Second Semester</option>
												</select>
											</div>
											<button type="button" class="btn btn-default" @click="generateGrade()">Generate</button>
										</form>
										<hr>
										<div v-for="(grade,index) in grades" class="panel panel-default">
											<div class="panel-heading">
												School Year: @{{index}}
											</div>

											<div class="panel-body">
												<div >
												<h4><span class="label label-info">Regular Subjects</span></h4>
									        	<table class="table table-responsive">
									        		<thead>
									        			<th>Course No.</th>
									        			<th>Course Title</th>
									        			<!-- <th>Numerical %</th> -->
									        			<th>Equivalent</th>
									        			<th>Remarks</th>
									        		</thead>
									        		<tbody>
									        			<tr v-for="g in grade.grades" v-bind:class="{'warning':g.equivalent>3.0&&g.equivalent<5,'danger':g.equivalent>4&&g.equivalent!=null}" v-if="g.advance == 'No'">
									        				<td>@{{g.course_no}}</td>
									        				<td>@{{g.title}}</td>
									        				<!-- <td>@{{grade.grade!=null?grade.grade:'NG'}}</td> -->
									        				<td>@{{g.equivalent==null||g.equivalent==0?"NG":g.equivalent}}</td>
									        				<td>@{{(g.equivalent<=3.0&&g.equivalent!=null)?"Passed":g.equivalent==null||g.equivalent==0?"N/A":g.equivalent>4.0?"Failed":"Conditional/INC"}}</td>
									        			</tr>
									        		</tbody>
									        	</table>
											</div>

											<div>
												<h4><span class="label label-info">Advanced Subjects</span></h4>
									        	<table class="table table-responsive">
									        		<thead>
									        			<th>Course No.</th>
									        			<th>Course Title</th>
									        			<!-- <th>Numerical %</th> -->
									        			<th>Equivalent</th>
									        			<th>Remarks</th>
									        		</thead>
									        		<tbody>

									        			<tr v-for="g in grade.grades" v-bind:class="{'warning':g.equivalent>3.0&&g.equivalent<5,'danger':g.equivalent>4&&g.equivalent!=null}" v-if="g.advance == 'Yes'">
									        				<td>@{{g.course_no}}</td>
									        				<td>@{{g.title}}</td>
									        				<!-- <td>@{{grade.grade!=null?grade.grade:'NG'}}</td> -->
									        				<td>@{{g.equivalent==null||g.equivalent==0?"NG":g.equivalent}}</td>
									        				<td>@{{(g.equivalent<=3.0&&g.equivalent!=null)?"Passed":g.equivalent==null||g.equivalent==0?"N/A":g.equivalent>4.0?"Failed":"Conditional/INC"}}</td>
									        			</tr>
									        		</tbody>
									        	</table>
											</div>
											</div>
    										<div class="panel-footer">
    											<form class="form-inline">
													<div class="form-group">
													    <label class="control-label" for="exampleInputEmail3">Grade Point Average/GPA@{{' : '}}</label>
													   	<p class="form-control-static">@{{grade.gpa}}</p>
													</div>
												</form>
    										</div>
								        	
										</div>
							        	<hr>
							        	<!-- <form class="form-inline">
											<div class="form-group">
											    <label class="control-label" for="exampleInputEmail3">Grade Point Average/GPA@{{' : '}}</label>
											   	<p class="form-control-static">@{{average_gpa}}</p>
											</div>
										</form> -->
							      	</div>
							    </div>
							  </div>
							  <div class="panel" :class="availables.is_adviced_to_shift==true?'panel-danger':'panel-default'">
							    <div class="panel-heading" role="tab" id="headingTwo">
								    <h4 class="panel-title">
								        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								        	@{{availables.is_adviced_to_shift==false?"Courses to be Enrolled":"Summary of Failing Grades"}}
								        </a>
								    </h4>
							    </div>
							    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							      	<div class="panel-body">
							      		<h4><span class="label label-info">New Subjects</span></h4>
							      		<table v-if="availables.is_adviced_to_shift==false" class="table table-responsive">
							      			<thead>
							      				<th>Course No.</th>
							      				<th>Course Title</th>
							      				<th>Lecture</th>
							      				<th>Lab</th>
							      				<th>Units</th>
							      				<th>Year</th>
							      				<th>Semester</th>
							      			</thead>
							      			<tbody>
							      				<tr v-if="available.re_enroll==false" v-for="available in availables.final_available_subjects">
							      					<td>@{{available.course_no}}</td>
							      					<td>@{{available.title}}</td>
							      					<td>@{{available.lec}}</td>
							      					<td>@{{available.lab}}</td>
							      					<td>@{{available.units}}</td>
							      					<td>@{{available.year_id}}</td>
							      					<td>@{{available.sem_id}}</td>
							      				</tr>
							      			</tbody>
							      		</table>
							      		<hr>
							      		<h4><span class="label label-info">Re-enroll Subjects</span></h4>
							      		<table v-if="availables.is_adviced_to_shift==false" class="table table-responsive">
							      			<thead>
							      				<th>Course No.</th>
							      				<th>Course Title</th>
							      				<th>Lecture</th>
							      				<th>Lab</th>
							      				<th>Units</th>
							      				<th>Year</th>
							      				<th>Semester</th>
							      			</thead>
							      			<tbody>
							      				<tr v-if="available.re_enroll==true" v-for="available in availables.final_available_subjects">
							      					<td>@{{available.course_no}}</td>
							      					<td>@{{available.title}}</td>
							      					<td>@{{available.lec}}</td>
							      					<td>@{{available.lab}}</td>
							      					<td>@{{available.units}}</td>
							      					<td>@{{available.year_id}}</td>
							      					<td>@{{available.sem_id}}</td>
							      				</tr>
							      			</tbody>
							      		</table>
										<table v-if="availables.is_adviced_to_shift==true" class="table table-responsive">
							      			<thead>
							      				<th>Course No.</th>
							      				<th>Course Title</th>
							      				<th>Lecture</th>
							      				<th>Lab</th>
							      				<th>Units</th>
							      				<th>Year</th>
							      				<th>Semester</th>
							      				<th>Grade</th>
							      			</thead>
							      			<tbody>
							      				<tr class="default" v-for="available in availables.failed_subjects">
							      					<td>@{{available.course_no}}</td>
							      					<td>@{{available.title}}</td>
							      					<td>@{{available.lec}}</td>
							      					<td>@{{available.lab}}</td>
							      					<td>@{{available.units}}</td>
							      					<td>@{{available.year_id}}</td>
							      					<td>@{{available.sem_id}}</td>
							      					<td>@{{available.grade}}</td>
							      				</tr>
							      			</tbody>
							      		</table>
							      		<hr>
							      		<form v-if="availables.is_adviced_to_shift==true" class="form-horizontal">
							      			<div class="form-group">
								      			<div class="col-xs-12">
								      				<p class="form-control-static">You can send @{{student.sex=="Male"?"him":"her"}} a message to notify.</p>
								      			</div>
								      		</div>
							      			<div class="form-group">
							      				<label class="sr-only col-xs-6"></label>
								      			<div class="col-xs-6">
								      				<textarea class="form-control" v-model="message.message" rows="4">
								      					
								      				</textarea>
								      			</div>
								      		</div>
								      		<div class="form-group">
								      			<div class="col-xs-6">
								      				<button class="btn btn-primary btn-xs" type="button" @click="sendNotification()">Send message</button>
								      			</div>
								      		</div>
							      		</form>
							      	</div>
							    </div>
							</div>
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Import Grades -->
		<div class="modal fade" id="import-grades-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Import Grades</h4>
				    </div>
				    <div class="modal-body">
				        <form class="form-horizontal">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Curriculum</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="import_grades.course_id" @change="updateOptionSubject()">
							    		<option v-bind:value="course.id" v-for="course in courses">@{{course.name}}</option>
							    	</select>
							    </div>
							</div>
							<div class="form-group">
							    <label class="control-label col-sm-4" for="exampleInputEmail2">Batch Year</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="import_grades.batch_id">
							    		<option v-bind:value="2014">2014-2015</option>
							    		<option v-bind:value="2015">2015-2016</option>
								    	<option v-bind:value="2016">2016-2017</option>
									    <option v-bind:value="2017">2017-2018</option>
									    <option v-bind:value="2018">2018-2019</option>
									    <option v-bind:value="2019">2019-2020</option>
								    </select>
							    </div>
						  	</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Year</label>
							    <div class="col-sm-6" >
							    	<select class="form-control" v-model="import_grades.year_id" @change="updateOptionSubject()">
							    		<option value="1">First Year</option>
							    		<option value="2">Second Year</option>
							    		<option value="3">Third Year</option>
							    		<option value="4">Fourth Year</option>
							    	</select>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Semester</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="import_grades.sem_id" @change="updateOptionSubject()">
							    		<option value="1">First Semester</option>
							    		<option value="2">Second Semester</option>
							    	</select>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':import_grades_error.subject_id}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Subject</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="import_grades.subject_id">
							    		<option v-bind:value="subject.id" v-for="subject in subjects">@{{subject.course_no}}</option>
							    	</select>
							    	<span class="help-block" v-if="import_grades_error.subject_id">
	                                    <strong>@{{ import_grades_error.subject_id[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':import_grades_error.grade_file}">
							    <label for="inputEmail3" class="col-sm-4 control-label">File</label>
							    <div class="col-sm-6">
							    	<input type="file" name="grade_file" id="import-grades" class="form-control" @change="changeFile" enctype="multipart/form-data">
							    	<span class="help-block" v-if="import_grades_error.grade_file">
	                                    <strong>@{{ import_grades_error.grade_file[0] }}</strong>
	                                </span>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Uploading File" id="btn-save-grades" class="btn btn-primary" @click="saveImportedGrades()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					Students
					<span class="pull-right">
					<button class="btn btn-default btn-xs" @click="showDeansLister()">Show Dean's Lister</button>
					<button class="btn btn-default btn-xs" @click="printGrades()">Print Grades</button>
					<button class="btn btn-default btn-xs" @click="importGrades()">Import Grades</button>
					<button class="btn btn-default btn-xs" @click="newBulkMessage()">Send Message</button></span>
				</div>
				<div class="panel-body">
					<form class="form-inline">
						<div class="form-group">
						    <label class="sr-only" for="exampleInputEmail3">Seach Student</label>
						    <input type="text" v-model="searchStudent" class="form-control" id="exampleInputName2" placeholder="Seach Student...">
						    <button type="button" class="btn btn-default" @click="search()"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</form>
					<hr>
					<table class="table table-responsive">
						<thead>
							<th></th>
							<th>Student Id</th>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<!-- <th>Phone no.</th> -->
							<th>Email Address</th>
							<th>Year Level</th>
							<th>Major</th>
						</thead>
						<tbody>
							<tr v-for="student in students">
								<td>
									<div class="dropdown">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										    Actions
										    <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										    <li><a type="button" @click="editStudent(student.id)">View Info</a></li>
										    <li><a type="button" @click="editPassword(student.user_id)">Update Student Credential</a></li>
										    <li><a type="button" @click="evaluateStudent(student.school_id,student.course_id,student.school_id)">Evaluate Student</a></li>
										    <li role="separator" class="divider"></li>
										    <li><a type="button" @click="newMessage(student.contact)">Send a Message</a></li>
										    <li><a type="button" @click="deleteStudent(student.id)">Delete Student</a></li>
										</ul>
									</div>
								</td>
								<td>@{{student.school_id}}</td>
								<td>@{{student.firstname}}</td>
								<td>@{{student.middlename}}</td>
								<td>@{{student.lastname}}</td>
								<!-- <td>@{{student.contact}}</td> -->
								<td>@{{student.email}}</td>
								<td>@{{student.year_level==0?"N/A":student.year_level}}</td>
								<td>@{{student.name}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<nav aria-label="Page navigation">
						<ul class="pagination">
						    <li v-bind:class="{'disabled':pagination.prev_page_url==null}">
							    <a href="#" @click="previousPage()" aria-label="Previous" >
							        <span aria-hidden="true">&laquo;</span>
							    </a>
						    </li>
						    <li v-bind:class="{'active':pagination.current_page==(index+1)}" v-for="(page,index) in pagination.last_page">
						    	<a href="#" @click="changePage(index+1)">@{{index+1}}</a>
						    </li>
						    <li v-bind:class="{'disabled':pagination.next_page_url==null}">
							    <a href="#" @click="nextPage()" aria-label="Next">
							        <span aria-hidden="true">&raquo;</span>
							    </a>
						    </li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{ asset('js/students/index.js')}}"></script>
@endsection