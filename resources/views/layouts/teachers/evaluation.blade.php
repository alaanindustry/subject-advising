@extends('layouts.app')

@section('content')
<div id="teacher-evaluation-details-component">
	<div class="all-modals">
		<div class="modal fade" id="teacher-evaluation-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Student-Teacher Evaluation</h4>
				    </div>
				    <div class="modal-body">
				    	<h3 style="text-align:center">School Year: @{{batch_id+'-'}}@{{parseInt(batch_id)+1}}</h3>
						<h2 style="text-align:center;margin-top:-10px"><small>@{{sem_id}} Semester</small></h2>
				    	<br>
				    	<p style="margin-left:20px"><b>Student: @{{student.firstname+' '+student.middlename+' '+student.lastname}}</b></p>
				    	<p style="margin-left:20px"><b>Teacher: @{{teacher.firstname+' '+teacher.lastname}}</b></p>
				    	<br>
				    	<p style="margin-left:20px" v-for="evaluation_result in evaluation_results">@{{evaluation_result.name+':        '+evaluation_result.sum}}</p>
				    	<p style="margin-left:20px">Total: @{{sum}} out of 150</p>
				    	<p style="margin-left:20px">Average: @{{average}}</p>
				    	<br><br>
				    	<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[1] - never (not at all)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[2] - Seldom (rarely)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[3] - Often (Sometimes)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[4] - Very often (Most of the times)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[5] - Always (All the times

						  </label>
						</div>
				    	<br>
				    	<form >
				    		<div class="container">
				    			<div class="form-group">
					    			<label class="control-label">I. Instructional Competence </label>
					    		</div>
				    		</div>
				    		<div v-for="(past,index) in pasts">
		    					<hr>
				    			<div class="form-group">
					    			<label class="control-label">@{{past.name}}</label>
					    		</div>
					    		<hr>
					    		<div class="form-group" v-for="(question,index) in past.questionaires">
					    			<p class=" control-label"><b>@{{index+1}}. </b>@{{question.description}}</p>
					    			<p style="margin:20px"><b>Rate: @{{question.answer.rate}}</b></p>
					    		</div>
				    		</div>
				    		<hr>
							<div class="form-group">
				    			<label class="control-label">Additional Comments</label>
				    		</div>
				    		<hr>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">1. Strenghts</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.strengths}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">2. Weaknesses</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.weaknesses}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" v-for="(past,index) in pasts">
								<div v-if="index==0">
									<label class="control-label">3. Recommendation</label>
									<div v-for="(question,index1) in past.questionaires">
										<div v-if="index1==0">
											<textarea readonly="" class="form-control" rows="4">@{{question.answer.recommendation}}</textarea>
										</div>
									</div>
								</div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	
	<div class="container">
		<div class="row">
			<div class="page-header">
			  	<h1><small>@{{teacher.firstname+' '+teacher.lastname+' - '+teacher.name}}</small></h1>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					Teacher's Evaluation Details
				</div>
				<div class="panel-body">

					<form class="form-inline" role="form">
						<div class="form-group">
						    <label class="control-label" for="exampleInputEmail2">Semester</label>
						    <select class="form-control" v-model="sem_id" @change="changeAppraisals">
						    	<option v-bind:value="1">First Semester</option>
						    	<option v-bind:value="2">Second Semester</option>
						    </select>
						</div>
						<div class="form-group">
						    <label class="control-label" for="exampleInputEmail2">Batch Year</label>
						    <select class="form-control" v-model="batch_id" @change="changeAppraisals">
						    	<option v-bind:value="2014">2014-2015</option>
							    <option v-bind:value="2015">2015-2016</option>
						    	<option v-bind:value="2016">2016-2017</option>
						    	<option v-bind:value="2017">2017-2018</option>
						    	<option v-bind:value="2018">2018-2019</option>
						    	<option v-bind:value="2019">2019-2020</option>
						    </select>
						</div>
					</form><hr>
					<table class="table table-responsive">
						<thead>
							<th></th>
							<th>Student Name</th>
							<th>Curriculum</th>
							<th>Average Rating</th>
							<th>Date Evaluated</th>
						</thead>
						<tbody>
							<tr v-for="student in students">
								<td class="col-xs-1">
									<div class="dropdown">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										    Options
										    <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										    <li><a href="#" @click="evaluationDetails(student.user_id)">More Evaluation Details</a></li>
										</ul>
									</div>
								</td>
								<td>@{{student.fullname}}</td>
								<td>@{{student.name}}</td>
								<td>@{{student.rate}}</td>
								<td>@{{student.created_at}}</td>
							</tr>
						</tbody>
					</table>		
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('js/teachers/evaluation.js')}}"></script>
@endsection