@extends('layouts.app')

@section('content')
<div id="user-component">
	<div class="all-modals">
		<!-- edit users -->
		<div class="modal fade" id="edit-user-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">User Details</h4>
				    </div>
				    <div class="modal-body">
						<form class="form-horizontal" role="form">
							<div class="form-group" v-bind:class="{'has-error':user_error.firstname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Firstname</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="user.firstname" id="inputEmail3" placeholder="Please enter user firstname">
							      	<span class="help-block" v-if="user_error.firstname">
	                                    <strong>@{{ user_error.firstname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user_error.lastname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lastname</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" v-model="user.lastname" id="inputEmail3" placeholder="Please enter user lastname">
							      	<span class="help-block" v-if="user_error.lastname">
	                                        <strong>@{{ user_error.lastname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user_error.username}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Username</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="user.username" id="inputEmail3" placeholder="Please enter user username">
							      	<span class="help-block" v-if="user_error.username">
	                                    <strong>@{{ user_error.username[0] }}</strong>
	                                </span>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateUser()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<!-- edit passowrd -->
		<div class="modal fade" id="edit-password-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">Change Password</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal">
				    		<div class="form-group" v-bind:class="{'has-error':password_error.password}">
							    <label for="inputEmail3" class="col-sm-4 control-label">New Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="password.password" id="inputEmail3" placeholder="Please enter user username">
							      	<span class="help-block" v-if="password_error.password">
	                                    <strong>@{{ password_error.password[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':password_error.password_confirmation}">
								    <label for="inputEmail3" class="col-sm-4 control-label">Confirm New Password</label>
								    <div class="col-sm-6">
								      	<input type="password" class="form-control" v-model="password.password_confirmation" id="inputEmail3" placeholder="Please confirm password">
								      	<span class="help-block" v-if="password_error.password_confirmation">
		                                    <strong>@{{ password_error.password_confirmation[0] }}</strong>
		                                </span>
								    </div>
							</div>
				    	</form>
						
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updatePassword()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- new user -->
		<div class="modal fade" id="new-user-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">New User Form</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal">
				    		<div class="form-group" v-bind:class="{'has-error':user_error.firstname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Firstname</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="user.firstname" id="inputEmail3" placeholder="Please enter user firstname">
							      	<span class="help-block" v-if="user_error.firstname">
	                                    <strong>@{{ user_error.firstname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user_error.lastname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lastname</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="user.lastname" id="inputEmail3" placeholder="Please enter user lastname">
							      	<span class="help-block" v-if="user_error.lastname">
	                                    <strong>@{{ user_error.lastname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user_error.username}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Username</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="user.username" id="inputEmail3" placeholder="Please enter user username">
							      	<span class="help-block" v-if="user_error.username">
	                                    <strong>@{{ user_error.username[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user_error.password}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="user.password" id="inputEmail3" placeholder="Please enter user password">
							      	<span class="help-block" v-if="user_error.password">
	                                    <strong>@{{ user_error.password[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':user.password_confirmation}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Confirm Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="user.password_confirmation" id="inputEmail3" placeholder="Please confirm password">
							      	<span class="help-block" v-if="password_error.password_confirmation">
	                                    <strong>@{{ password_error.password_confirmation[0] }}</strong>
	                                </span>
							    </div>
							</div>
				    	</form>
						
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="addUser()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					System Users
					<span class="pull-right"><button class="btn btn-default btn-xs" @click="newUser()">New System User</button></span>
				</div>
				<div class="panel-body">
					<table class="table table-responsive">
						<thead>
							<th></th>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Username</th>
						</thead>
						<tbody>
							<tr v-for="user in users">
								<td>
									<div class="dropdown">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										    Actions
										    <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										    <li><a href="#" @click="editUser(user.id)">Edit User</a></li>
										    <li><a href="#" @click="editPassword(user.id)">Change Password</a></li>
										    <li role="separator" class="divider"></li>
										    <li><a href="#" @click="deleteUser(user.id)">Delete User</a></li>
										</ul>
									</div>
								</td>
								<td>@{{user.firstname}}</td>
								<td>@{{user.lastname}}</td>
								<td>@{{user.username}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ asset('js/users/index.js')}}"></script>
@endsection