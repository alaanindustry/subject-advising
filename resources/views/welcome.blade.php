@extends('layouts.app')

@section('content')
    <div id="myCarousel" style="margin-top: -20px;" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="3" class="active"></li>
        </ol>
        
        <!-- Wrapper for slides -->
      
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{asset('img/two.jpg')}}" width="1355px" height="50px">
            </div>

            <div class="item">
                <img src="{{asset('img/three.jpg')}}" width="1355px" height="50px">
            </div>
            
            <div class="item">
                <img src="{{asset('img/four.jpg')}}" width="1355px" height="50px">
            </div>
            
            <div class="item">
                <img src="{{asset('img/five.jpg')}}" width="1355px" height="50px">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
@endsection
