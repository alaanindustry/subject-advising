<?php $__env->startSection('content'); ?>
<div id="teacher-component">

	<!-- all modals -->
	<div class="all-modals">
		
		<!-- Send SMS -->

		<div class="modal fade" id="new-teacher-message-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">New Message</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-inline">
							<div class="form-group">
							    <label class="sr-only" for="exampleInputEmail3">Email address</label>
							    <select class="form-control" @change="changeTeacherOptions" v-model="course_id">
							    	<option v-bind:value="course.id" v-for="course in courses">{{course.name}}</option>
							    </select>
							</div>
						</form>
						<hr>
						<div class="form-group">
							<label class="control-label col-xs-2">Message</label>
							<div class="col-xs-10">
								<textarea v-model="bulkmessage.message" class="form-control"></textarea>
							</div>
						</div><br><hr>
						<div style="margin-left:20px">
							<div class="checkbox col-xs-offset-2">
					      <label>
					        <input type="checkbox" v-bind:checked="isSelectAll" @change="selectDiselectTeachers"> {{select}}
					      </label>
					    </div>	
						</div>
						
						<ul class="list-group col-xs-offset-2">
						  <div class="container">
						  	<div class="checkbox" v-for="teacher in sendToTeachers">
							    <label>
							        <input v-bind:value="teacher.contact" v-model="bulkmessage.sendTo" v-bind:id="teacher.contact" type="checkbox"> {{teacher.firstname+' '+teacher.lastname+' - '+(teacher.contact==null?"None":teacher.contact)}}
							    </label>
						    </div>
						  </div>
						</ul>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="sendBulkMessage()">Send	Message</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Evaluation -->

		<div class="modal fade" id="teacher-appraisal-modal">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">LIST OF TEACHERS' PERFORMANCE APPRAISAL</h4>
				    </div>
				    <div class="modal-body">
				        <form class="form-inline" role="form">
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Semester</label>
							    <select class="form-control" v-model="sem_id" @change="changeAppraisals">
							    	<option v-bind:value="1">First Semester</option>
							    	<option v-bind:value="2">Second Semester</option>
							    </select>
							</div>
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Batch Year</label>
							    <select class="form-control" v-model="batch_id" @change="changeAppraisals">
							    	<option v-bind:value="2014">2014-2015</option>
							    	<option v-bind:value="2015">2015-2016</option>
							    	<option v-bind:value="2016">2016-2017</option>
							    	<option v-bind:value="2017">2017-2018</option>
							    	<option v-bind:value="2018">2018-2019</option>
							    	<option v-bind:value="2019">2019-2020</option>
							    </select>
							</div>
							<span class="pull-right"><a href="#" @click="printTeacherEvaluation()">Print</a></span>
						</form><hr>
						<table class="table tabl-responsive">
							<thead>
								<th>Teacher Name</th>
								<th>Curriculum</th>
								<th>Average Rate</th>
							</thead>
							<tbody>
								<tr v-for="appraisal in appraisals">
									<td>{{appraisal.name}}</td>
									<td>{{appraisal.course}}</td>
									<td>{{appraisal.average==null?"None":appraisal.average}}</td>
								</tr>
							</tbody>
						</table>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Add new Teacher -->
		<div class="modal fade" id="new-teacher-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">New Teacher Form</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal" role="form">
				    		<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Profile Picture</label>
							    <div class="col-sm-5">
							      <img style="width:150px;height:150px" v-bind:src="'/cas-ied/public/storage/public/'+teacher.profile_picture"  alt="" class="img-thumbnail">
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.profile_picture}">
								<label for="inputEmail3" class="col-sm-4 control-label"></label>
							    <div class="col-sm-5">
							      	<input type="file" @change="changePicture" name="profile_picture" enctype="multipart/form-data" id="teacher_profile_picture">
							      	<span class="help-block" v-if="teacher_error.profile_picture">
	                                    <strong>{{ teacher_error.profile_picture[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.firstname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Firstname</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" id="inputEmail3" v-model="teacher.firstname" placeholder="Please enter teacher firstname">
							      	<span class="help-block" v-if="teacher_error.firstname">
	                                    <strong>{{ teacher_error.firstname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.lastname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lastname</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.lastname" placeholder="Please enter teacher lastname">
							      	<span class="help-block" v-if="teacher_error.lastname">
	                                    <strong>{{ teacher_error.lastname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.contact}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Phone No.</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.contact" placeholder="Please enter teacher contact">
							      	<span class="help-block" v-if="teacher_error.contact">
	                                    <strong>{{ teacher_error.contact[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.degree}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Degree</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.degree" placeholder="Teachers Degree">
							      	<span class="help-block" v-if="teacher_error.degree">
	                                    <strong>{{ teacher_error.degree[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.last_graduated}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Last Graduated</label>
							    <div class="col-sm-6">
							      <input type="date" class="form-control" id="inputEmail3" v-model="teacher.last_graduated" placeholder="">
							      	<span class="help-block" v-if="teacher_error.last_graduated">
	                                    <strong>{{ teacher_error.last_graduated[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.attainment}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Attainment</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.attainment" placeholder="Enter Teaher attainment">
							      	<span class="help-block" v-if="teacher_error.attainment">
	                                    <strong>{{ teacher_error.attainment[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Curriculum</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="teacher.course_id">
							    		<option v-bind:value="course.id" v-for="course in courses">{{course.name}}</option>
							    	</select>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="addTeacher()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Edit teacher -->
		<div class="modal fade" id="edit-teacher-modal">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">Teacher Details</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal" role="form">
				    		<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Profile Picture</label>
							    <div class="col-sm-5">
							      <img style="width:150px;height:150px" v-bind:src="'/cas-ied/public/storage/public/'+teacher.profile_picture"  alt="" class="img-thumbnail">
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.profile_picture}">
								<label for="inputEmail3" class="col-sm-4 control-label"></label>
							    <div class="col-sm-5">
							      	<input type="file" @change="changePicture" name="profile_picture" enctype="multipart/form-data" id="teacher_profile_picture">
							      	<span class="help-block" v-if="teacher_error.profile_picture">
	                                    <strong>{{ teacher_error.profile_picture[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.firstname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Firstname</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" id="inputEmail3" v-model="teacher.firstname" placeholder="Please enter teacher firstname">
							      	<span class="help-block" v-if="teacher_error.firstname">
	                                    <strong>{{ teacher_error.firstname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.lastname}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Lastname</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.lastname" placeholder="Please enter teacher lastname">
							      	<span class="help-block" v-if="teacher_error.lastname">
	                                    <strong>{{ teacher_error.lastname[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.contact}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Phone No.</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.contact" placeholder="Please enter teacher contact">
							      	<span class="help-block" v-if="teacher_error.contact">
	                                    <strong>{{ teacher_error.contact[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.degree}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Degree</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.degree" placeholder="Teachers Degree">
							      	<span class="help-block" v-if="teacher_error.degree">
	                                    <strong>{{ teacher_error.degree[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.last_graduated}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Last Graduated</label>
							    <div class="col-sm-6">
							      <input type="date" class="form-control" id="inputEmail3" v-model="teacher.last_graduated" placeholder="">
							      	<span class="help-block" v-if="teacher_error.last_graduated">
	                                    <strong>{{ teacher_error.last_graduated[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':teacher_error.attainment}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Attainment</label>
							    <div class="col-sm-6">
							      <input type="text" class="form-control" id="inputEmail3" v-model="teacher.attainment" placeholder="Enter Teaher attainment">
							      	<span class="help-block" v-if="teacher_error.attainment">
	                                    <strong>{{ teacher_error.attainment[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Curriculum</label>
							    <div class="col-sm-6">
							    	<select class="form-control" v-model="teacher.course_id">
							    		<option v-bind:value="course.id" v-for="course in courses">{{course.name}}</option>
							    	</select>
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateTeacher()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<!-- Teachers Evaluation -->
		<div class="modal fade">
			<div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">New Teacher Form</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal" role="form">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
							    <div class="col-sm-10">
							      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
							    </div>
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					Teachers
					<span class="pull-right">
						<button class="btn btn-default btn-xs" @click="newTeacher()">
							Add New Teacher
						</button>
						<button class="btn btn-default btn-xs" @click="newMessage()">Send Message</button>
						<button class="btn btn-default btn-xs" @click="viewTeachersEvaluation()">View Teachers Evaluation</button>
					</span>
				</div>
				<div class="panel-body">
					<form class="form-inline">
						<div class="form-group">
						    <label class="sr-only" for="exampleInputEmail3">Search Teacher</label>
						    <input type="email" v-model="searchTeacher" class="form-control" id="exampleInputEmail3" placeholder="Seach Teacher...">
						</div>
						<button type="button" class="btn btn-default" @click="search()"><i class="glyphicon glyphicon-search"></i></button>
					</form><br>
					<table class="table table-responsive">
						<thead>
							<thead><br>
								<th></th>
								<th>Firstname</th>
								<th>Lastname</th>
								<th>Phone No.</th>
								<th>Curriculum</th>
							</thead>
							<tbody>
								<tr v-for="teacher in teachers">
									<td class="col-xs-1">
										<div class="dropdown">
											<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
											    Actions
											    <span class="caret"></span>
											</button>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
											    <li role="presentation"><a role="menuitem" tabindex="-1" type="button" @click="editTeacher(teacher.id)">Teacher Details</a></li>
											    <li role="presentation"><a role="menuitem" tabindex="-1" type="button" @click="teacherEvaluation(teacher.id)">View Evaluation</a></li>
											    <li role="presentation" class="divider"></li>
											    <li role="presentation"><a role="menuitem" tabindex="-1" type="button" @click="deleteTeacher(teacher.id)">Delete Teacher</a></li>
											</ul>
										</div>
									</td>
									<td>{{teacher.firstname}}</td>
									<td>{{teacher.lastname}}</td>
									<td>{{teacher.contact}}</td>
									<td class="col-xs-3">{{teacher.name}}</td>
								</tr>
							</tbody>
						</thead>
					</table>
				</div>
				<div class="panel-footer">
					<nav aria-label="Page navigation">
						<ul class="pagination">
						    <li v-bind:class="{'disabled':pagination.prev_page_url==null}">
							    <a href="#" @click="previousPage()" aria-label="Previous" >
							        <span aria-hidden="true">&laquo;</span>
							    </a>
						    </li>
						    <li v-bind:class="{'active':pagination.current_page==(index+1)}" v-for="(page,index) in pagination.last_page">
						    	<a href="#" @click="changePage(index+1)">{{index+1}}</a>
						    </li>
						    <li v-bind:class="{'disabled':pagination.next_page_url==null}">
							    <a href="#" @click="nextPage()" aria-label="Next">
							        <span aria-hidden="true">&raquo;</span>
							    </a>
						    </li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo e(asset('js/teachers/index.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>