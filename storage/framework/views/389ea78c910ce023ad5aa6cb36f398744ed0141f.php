<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.header{
			text-align: center;
			padding: 30px;
		}
		#davao{
			margin-top: 20px;
			font-weight: bold;
			letter-spacing: 3px;
		}
		#evaluation{
			margin-top: 50px;
		}
		.infos{
			margin-top: -7px;
		}
		.info-label{
			width: 80px;
			float: left;
			margin-left: 10px;
			font-weight: bold;
			margin-right: 4px;
		}
		.info-value{
			letter-spacing: 1px;
		}
		.table-grades{
			width: 100%;
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{

			border: 1px solid black;
		}
		.grade-title{
			margin-top: 20px;
		}
	</style>
</head>
<body>
	<div class="header">
		<div id="republic"><b>Republic of the Philippines</b></div>
		<div id="davao">DAVAO DEL NORTE STATE</div>
		<div id="evaluation">EVALUATION SHEET</div>
	</div>
	<div class="infos">
		<label class="info-label">Name:</label>
		<p class="info-value" type="text"><?php echo e($student->firstname.' '.$student->middlename.' '.$student->lastname); ?></p>
	</div>
	<div class="infos">
		<label class="info-label">Course:</label>
		<p class="info-value" type="text">Bachelor of Schience in Education</p>
	</div>
	<div class="infos">
		<label class="info-label">Major:</label>
		<p class="info-value" type="text"><?php echo e($student->name); ?></p>
	</div>
	<div id="data">
		<table class="table-grades">
			<thead>
				<th>COURSE NUMBER</th>
				<th>DESCRIPTIVE TITLE OF THE COURSE</th>
				<th>FINAL</th>
			</thead>
			<tbody>
				<?php foreach($student_grades as $student_grade): ?>
					
					<?php foreach($student_grade['data'] as $grades): ?>
						<tr class="grade-title">
							<td colspan="3"><b><?php echo e($grades['sem'].' '.$student_grade['year']); ?></b></td>
						</tr>
						<?php foreach($grades['data'] as $grade): ?>
							<tr class="grade-title">
								<td ><?php echo e($grade->course_no); ?></td>
								<td ><?php echo e($grade->title); ?></td>
								<td ><?php echo e($grade->equivalent==null?"None":$grade->equivalent); ?></td>
							</tr>
						<?php endforeach; ?>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</body>
</html>