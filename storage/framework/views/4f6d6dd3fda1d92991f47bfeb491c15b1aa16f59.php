<?php $__env->startSection('content'); ?>
	
<div class="grades-page-component">
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					My Grades
				</div>
				<div class="panel-body">
					<?php foreach($student_grades as $student_grade): ?>
						<?php if (is_array($student_grade)): ?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<?php echo e($student_grade['name']); ?> Year
								</div>
								<div class="panel-body">
									<?php foreach($student_grade as $stud_grade): ?>
										<?php if (is_array($stud_grade)): ?>
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">
														<?php echo e($stud_grade['name']); ?> Semester
													</div>
													<div class="panel-body">
														<table class="table table-responsive">
															<thead>
																<th>Course No.</th>
																<th>Coursen Title</th>
																<th>Grade</th>
																<th>Units</th>
															</thead>
															<tbody>
																<?php foreach($stud_grade as $grade): ?>
																	<?php if (is_object($grade)): ?>
																		<tr>
																			<td><?php echo e($grade->course_no); ?></td>
																			<td><?php echo e($grade->title); ?></td>
																			<td><?php echo e($grade->equivalent==null?"None":$grade->equivalent); ?></td>
																			<td><?php echo e($grade->units==null?"None":$grade->units); ?></td>
																		</tr>
																	<?php endif ?>
																	
																<?php endforeach; ?>
																<tr>
																	<td></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><b>GPA >>></b></td>
																	<td></td>
																	<td><b><?php echo e($stud_grade['gpa']==0?"N/A":$stud_grade['gpa']); ?></b></td>
																</tr>
																<tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										<?php endif ?>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>