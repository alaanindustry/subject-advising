<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			font-family: monospace;
		}
		img {
		    width: 215px;
		    margin-top: 20px;
		}
		.header{
			text-align: center;
			padding: 30px;
			font-size: 18px;
		}
		#davao{
			margin-top: 20px;
			font-weight: bold;
			letter-spacing: 3px;
		}
		#evaluation{
			margin-top: 50px;
		}
		.infos{
			margin-top: -7px;
		}
		.info-label{
			width: 80px;
			float: left;
			margin-left: 10px;
			font-weight: bold;
			margin-right: 4px;
		}
		.info-value{
			letter-spacing: 1px;
		}
		.table-grades{
			width: 100%;
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{

			border: 1px solid black;
		}
		.grade-title{
			margin-top: 20px;
		}
	</style>
</head>
<body>
	<div class="header">

		<div id="republic"><b>Republic of the Philippines</b></div>
		<img src="<?php echo e(asset('img/dnsclogo.png')); ?>">
		<div id="davao">DAVAO DEL NORTE STATE</div>
		<div id="batch"><?php echo e('Batch '.$batch.'-'.($batch+1)); ?></div>
		<div id="course"><?php echo e($sem." Semester"); ?></div>
		<div id="evaluation">List of Deans Lister</div>
	</div>
	<div id="data">
		<table class="table-grades">
			<thead>
				<th>Student Id</th>
				<th>Name</th>
				<th>Year</th>
				<th>GPA</th>
				<th>Rank</th>
			</thead>
			<tbody>
				<?php foreach($deanslisters['first'] as $deanslister): ?>
					<tr>
						<td><?php echo e($deanslister->school_id); ?></td>
						<td><?php echo e($deanslister->firstname.' '.$deanslister->middlename.' '.$deanslister->lastname); ?></td>
						<td><?php echo e($deanslister->year); ?></td>
						<td><?php echo e($deanslister->gpa); ?></td>
						<td>First Honor</td>
					</tr>
				<?php endforeach; ?>
				<?php foreach($deanslisters['second'] as $deanslister): ?>
					<tr>
						<td><?php echo e($deanslister->school_id); ?></td>
						<td><?php echo e($deanslister->firstname.' '.$deanslister->middlename.' '.$deanslister->lastname); ?></td>
						<td><?php echo e($deanslister->year); ?></td>
						<td><?php echo e($deanslister->gpa); ?></td>
						<td>Second Honor</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</body>
</html>