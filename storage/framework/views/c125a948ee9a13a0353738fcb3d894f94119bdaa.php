<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="page-header">
	  	<h1><small>Bachelor of Secondary Education</small></h1>
	</div>
	<div class="media">
		<div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="<?php echo e(asset('img/vision.jpg')); ?>" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading">Vision</h4>
		    We envision the Davao del Norte State College:

			A premier institution of higher learning that is imbued with its core values for the development of human resources, and generation and utilization of knowledge and technology for a productive sustainable, just, and humane society.
		</div>
	</div>
	<hr>
	<div class="media">
		<div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="<?php echo e(asset('img/mission.jpg')); ?>" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading">Mission</h4>
		    As an institution of higher learning and teaching excellence, informed by research and empowered to carry out extension and production services, DNSC shall:
			<br><br>
			1. Provide equitable access quality, relevant, and environment-friendly programs in instruction, research, extension;
			<br>
			2. Promote good governance and adopt mechanisms to continuously upgrade institutional standards;
			<br>
			3. Enhance capabilities and work ethics of the workforce of the institution; and
			<br>
			4. Develop appropriate linkage and network in the implementation of College programs.
		</div>
	</div>
	<hr>
	<div class="media">
		<div class="media-left media-middle">
		    <a href="#">
		      <img style="width:284px;height:200" class="media-object thumbnail" src="<?php echo e(asset('img/goal.jpg')); ?>" alt="...">
		    </a>
		</div>
		<div class="media-body">
		    <h4 class="media-heading">Goal</h4>
		    1. Equip teacher education student with requisite knowledge in general education, professional education and specialization courses necessary for effective teaching and learning gared towards the production of a holistically developed Filipino.
			<br>
			2. Provide students with adequate knowledge, skills and values in research and extension to continuously enhance instructional delivery, share best practices and packaged generation technology for commercialization.
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>