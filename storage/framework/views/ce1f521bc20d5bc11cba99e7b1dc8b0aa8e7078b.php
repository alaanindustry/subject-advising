<?php $__env->startSection('content'); ?>
<div id="page-appraisal-component">
	<!-- all modals -->
	<div class="all-modals">
		<!-- Evalutae Teacher -->
		<div class="modal fade" id="evaluate-teacher-modal">
			<div class="modal-dialog modal-lg">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title">PERFORMANCE APPRAISAL FOR FACULTY</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-inline" role="form">
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">Semester</label>
							    <select class="form-control" v-model="evaluation.sem_id">
							    	<option v-bind:value="1">First Semester</option>
							    	<option v-bind:value="2">Second Semester</option>
							    </select>
							</div>
							<div class="form-group">
							    <label class="control-label" for="exampleInputEmail2">School Year</label>
							    <select class="form-control" v-model="evaluation.batch_id">
							    	<option v-bind:value="2016">2016-2017</option>
							    	<option v-bind:value="2017">2017-2018</option>
							    	<option v-bind:value="2018">2018-2019</option>
							    	<option v-bind:value="2019">2019-2020</option>
							    </select>
							</div>
						</form>
						<hr>
				    	<strong>Direction: </strong><ins> Please rate your instruction or professor honestly and sincerely by checking the check box which corresponds to your choice of description as indicated below:</ins>
				    	<br>
				    	<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[1] - never (not at all)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[2] - Seldom (rarely)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[3] - Often (Sometimes)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[4] - Very often (Most of the times)
						  </label>
						</div>
						<div class="checkbox">
						  <label>
						    <input disabled="" type="radio" value="">
						   	[5] - Always (All the times

						  </label>
						</div>
				    	<hr>
				    	<form role="form">
				    		<div class="form-group">
				    			<label class="control-label">I. Instructional Competence</label>
				    		</div>
				    		<div class="form-group">
				    			<label class="control-label">Teaching Skills</label>
				    		</div>
				    		<hr>
				    		<div class="form-group">
							    <p class=" control-label"><b>1.</b> Present and explain objectives of the lesson at the start of the class.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="oneone" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.oneone"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneone" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.oneone"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneone" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.oneone"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneone" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.oneone"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneone" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.oneone"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>2.</b> Keeps learning tasks an activities in agreement with the objectives course content indicated in the syllabus.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onetwo" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onetwo"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onetwo" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onetwo"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onetwo" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onetwo"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onetwo" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onetwo"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onetwo" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onetwo"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>3.</b> Shows evidence of mastery of the subject matter.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onethree" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onethree"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onethree" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onethree"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onethree" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onethree"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onethree" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onethree"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onethree" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onethree"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>4.</b> Provides learning experiences for the development of the communication, thinking skills, work thics and other basic skills.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onefour" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onefour"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefour" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onefour"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefour" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onefour"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefour" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onefour"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefour" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onefour"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>5.</b> Selects, prepares and utilizes instructional materials effectively in achieving teaching goals.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onefive" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onefive"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefive" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onefive"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefive" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onefive"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefive" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onefive"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onefive" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onefive"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>6.</b> Motivates the lesson and ask questions effectively to elecit positive answer.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onesix" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onesix"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onesix" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onesix"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onesix" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onesix"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onesix" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onesix"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onesix" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onesix"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>7.</b> Uses appropriate styles that meet the student' needs and problems.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="oneseven" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.oneseven"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneseven" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.oneseven"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneseven" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.oneseven"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneseven" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.oneseven">4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneseven" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.oneseven"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>8.</b> Uses English and Filipino as medium of instruction.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="oneeight" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.oneeight"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneeight" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.oneeight"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneeight" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.oneeight"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneeight" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.oneeight"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneeight" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.oneeight"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>9.</b> Considers tactfully questions asked by the students to enhance learning.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="onenine" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.onenine"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onenine" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.onenine"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onenine" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.onenine"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onenine" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.onenine"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="onenine" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.onenine"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>10.</b> Shows interest in student' problems and needs and helps meet them.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="oneten" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.oneten"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneten" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.oneten"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneten" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.oneten"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneten" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.oneten"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="oneten" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.oneten"> 5
									</label>
							    </div>
							</div>
							<hr>
							<div class="form-group">
				    			<label class="control-label">II. MANAGEMENT SKILLS </label>
				    		</div>
				    		<hr>
				    		<div class="form-group">
							    <p class=" control-label"><b>1.</b> Repairs to class regularly and promptly.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="twoone" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.twoone"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twoone" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.twoone"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twoone" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.twoone"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twoone" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.twoone"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twoone" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.twoone"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>2.</b> Prepares adequately for the days learning activities.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="twotwo" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.twotwo"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twotwo" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.twotwo"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twotwo" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.twotwo"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twotwo" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.twotwo"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twotwo" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.twotwo"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>3.</b> Provides stimuliting classroom atmosphere that encourages students' involvement in the learning.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="twothree" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.twothree"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twothree" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.twothree"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twothree" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.twothree"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twothree" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.twothree"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twothree" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.twothree"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>4.</b> Administers tests effectively and returns corrected paper and other student's work w/ comments and suggstions for improvement.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="twofour" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.twofour"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofour" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.twofour"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofour" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.twofour"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofour" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.twofour"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofour" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.twofour"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>5.</b> Uses instructional time for proper and relevent learning activities.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="twofive" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.twofive"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofive" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.twofive"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofive" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.twofive"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofive" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.twofive"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="twofive" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.twofive"> 5
									</label>
							    </div>
							</div>
							<hr>
							<div class="form-group">
				    			<label class="control-label">III. COMMUNICATIONS SKILLS  </label>
				    		</div>
				    		<hr>
				    		<div class="form-group">
							    <p class=" control-label"><b>1.</b> Speaks clearly and loudly enough to be heard by the class.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="threeone" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.threeone"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threeone" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.threeone"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threeone" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.threeone"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threeone" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.threeone"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threeone" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.threeone"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>2.</b> Uses correct pronunciation and grammar in speaking.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="threetwo" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.threetwo"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threetwo" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.threetwo"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threetwo" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.threetwo"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threetwo" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.threetwo"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threetwo" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.threetwo"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>3.</b> Maintain eye contact w/ his/her students while speaking.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="threethree" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.threethree"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threethree" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.threethree"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threethree" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.threethree"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threethree" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.threethree"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threethree" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.threethree"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>4.</b> Writes on the board legibly.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="threefour" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.threefour"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefour" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.threefour"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefour" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.threefour"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefour" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.threefour"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefour" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.threefour"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>5.</b> Uses the most effective type of communicative relationship in the class wherein the teacher facilities class discussions and allows more students interaction.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="threefive" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.threefive"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefive" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.threefive"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefive" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.threefive"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefive" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.threefive"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="threefive" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.threefive"> 5
									</label>
							    </div>
							</div>
							<hr>
							<div class="form-group">
				    			<label class="control-label">IV. PERSONAL AND SOCIAL SKILLS  </label>
				    		</div>
				    		<hr>
				    		<div class="form-group">
							    <p class=" control-label"><b>1.</b> Observes proper grooming and attaire at all times.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourone" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourone"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourone" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourone"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourone" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourone"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourone" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourone"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourone" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourone"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>2.</b>  Is free from mannerisms and other defects that tent to disturb students' attention.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourtwo" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourtwo"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourtwo" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourtwo"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourtwo" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourtwo"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourtwo" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourtwo"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourtwo" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourtwo"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>3.</b> Has a pleasing personal appearance and pleasing voice.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourthree" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourthree"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourthree" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourthree"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourthree" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourthree"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourthree" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourthree"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourthree" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourthree"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>4.</b> Has sense of humor, cheerful and enthusiastic.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourfour" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourfour"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfour" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourfour"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfour" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourfour"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfour" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourfour"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfour" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourfour"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>5.</b> shows emotional stability and self-control.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourfive" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourfive"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfive" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourfive"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfive" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourfive"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfive" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourfive"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourfive" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourfive"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>6.</b> Sets an example of moral and ethical behavior to the students.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="foursix" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.foursix"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foursix" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.foursix"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foursix" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.foursix"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foursix" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.foursix"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foursix" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.foursix"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>7.</b> Shows honestly and integrity in all his activities.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourseven" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourseven"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourseven" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourseven"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourseven" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourseven"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourseven" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourseven"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourseven" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourseven"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>8.</b> Shows fairness, impartiality and tolerance in his dealing with students and peers.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="foureight" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.foureight"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foureight" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.foureight"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foureight" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.foureight"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foureight" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.foureight"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="foureight" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.foureight"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>9.</b> Shows evidence of sound mental and physical health.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fournine" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fournine"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fournine" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fournine"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fournine" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fournine"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fournine" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fournine"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fournine" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fournine"> 5
									</label>
							    </div>
							</div>
							<div class="form-group">
							    <p class=" control-label"><b>10.</b> Shows certain quality of being professional and highly developed person.</p>
							    <div style="margin-left:20px">
							    	<label class="checkbox-inline">
									  	<input type="radio" name="fourten" v-bind:value="1" id="inlineCheckbox1" v-model="evaluation.fourten"> 1
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourten" v-bind:value="2" id="inlineCheckbox2" v-model="evaluation.fourten"> 2
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourten" v-bind:value="3" id="inlineCheckbox3" v-model="evaluation.fourten"> 3
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourten" v-bind:value="4" id="inlineCheckbox3" v-model="evaluation.fourten"> 4
									</label>
									<label class="checkbox-inline">
									  	<input type="radio" name="fourten" v-bind:value="5" id="inlineCheckbox3" v-model="evaluation.fourten"> 5
									</label>
							    </div>
							</div>
							<hr>
							<div class="form-group">
				    			<label class="control-label">Additional Comments</label>
				    		</div>
				    		<hr>
							<div class="form-group">
								<label class="control-label">1. Strenghts</label>
								<textarea class="form-control" v-model="evaluation.strengths" rows="4"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">2. Weakness</label>
								<textarea class="form-control" v-model="evaluation.weakness" rows="4"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">3. Recommendation</label>
								<textarea class="form-control" v-model="evaluation.recommendation" rows="4"></textarea>
							</div>
				    	</form>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="submitEvaluation()">Submit Evaluation</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					List of Teachers
				</div>
				<div class="panel-body">
					<table class="table table-responsive">
						<thead>
							<th></th>
							<th>Teachers Picture</th>
							<th>Firstname</th>
							<th>Lastname</th>
						</thead>
						<tbody>
							<tr v-for="teacher in teachers">
								<td>
									<div class="dropdown">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Options
										    <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" @click="evaluateTeacher(teacher.id)">Evaluate Teacher</a></li>
										</ul>
									</div>
								</td>
								<td></td>
								<td>{{teacher.firstname}}</td>
								<td>{{teacher.lastname}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo e(asset('js/teachers/student_page.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>