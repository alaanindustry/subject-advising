<?php $__env->startSection('content'); ?>
<div id="profile-component">
	<div class="all-modal">
		<div class="modal fade" id="new-password-modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Change My Credentials</h4>
				    </div>
				    <div class="modal-body">
				    	<form class="form-horizontal">
				    		<div class="form-group" v-bind:class="{'has-error':credentials_error.username}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Username</label>
							    <div class="col-sm-6">
							      	<input type="text" class="form-control" v-model="credentials.username" id="inputEmail3" placeholder="Please enter your username">
							      	<span class="help-block" v-if="credentials_error.username">
	                                    <strong>{{ credentials_error.username[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':credentials_error.password}">
							    <label for="inputEmail3" class="col-sm-4 control-label">New Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="credentials.password" id="inputEmail3" placeholder="Please enter your new password">
							    	<span class="help-block" v-if="credentials_error.password">
	                                    <strong>{{ credentials_error.password[0] }}</strong>
	                                </span>
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':credentials_error.password_confirmation}">
							    <label for="inputEmail3" class="col-sm-4 control-label">Confirm New Password</label>
							    <div class="col-sm-6">
							      	<input type="password" class="form-control" v-model="credentials.password_confirmation" id="inputEmail3" placeholder="Please confirm your new password">
									<span class="help-block" v-if="credentials_error.password_confirmation">
	                                    <strong>{{ credentials_error.password_confirmation[0] }}</strong>
	                                </span>
							    </div>
							</div>
						</form>	
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" @click="updateCredential()">Save changes</button>
				    </div>
			    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						My Profile Information
						<span class="pull-right">
							<button class="btn btn-default btn-xs" @click="changePassword()">Change My Credential</button>
						</span>
					</div>
					<div class="panel-body">
						<form class="form-horizontal">

							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Profile Picture</label>
							    <div class="col-sm-5">
							      <img style="width:150px;height:150px" v-bind:src="'/storage/public/'+student.profile_picture"  alt="" id="product-image-preview" class="img-thumbnail">
							    </div>
							</div>
							<div class="form-group" v-bind:class="{'has-error':student_error.profile_picture}">
								<label for="inputEmail3" class="col-sm-4 control-label"></label>
							    <div class="col-sm-5">
							      	<input type="file" @change="changePicture" name="profile_picture" enctype="multipart/form-data" id="profile_picture">
							      	<span class="help-block" v-if="student_error.profile_picture">
	                                    <strong>{{ student_error.profile_picture[0] }}</strong>
	                                </span>
							    </div>
							</div>
							
							<div class="form-group" v-bind:class="{'has-error':student_error.school_id}">
	                            <label for="school_id" class="col-md-4 control-label">School ID No.</label>

	                            <div class="col-md-6">
	                                <input id="school_id" type="text" class="form-control" name="school_id" v-model="student.school_id" placeholder="Please enter your School Id">
									<span class="help-block" v-if="student_error.school_id">
	                                        <strong>{{ student_error.school_id[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.firstname}">
	                            <label for="firstname" class="col-md-4 control-label">Firstname</label>

	                            <div class="col-md-6">
	                                <input id="firstname" type="text" class="form-control" name="firstname" v-model="student.firstname" placeholder="Please enter your firstname">
									
									<span class="help-block" v-if="student_error.firstname">
	                                        <strong>{{ student_error.firstname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.middlename}">
	                            <label for="middlename" class="col-md-4 control-label">Middlename</label>

	                            <div class="col-md-6">
	                                <input id="middlename" type="text" class="form-control" name="middlename" v-model="student.middlename" placeholder="Please enter your middlename">

	                                <span class="help-block" v-if="student_error.middlename">
	                                        <strong>{{ student_error.middlename[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.lastname}">
	                            <label for="lastname" class="col-md-4 control-label">Lastname</label>

	                            <div class="col-md-6">
	                                <input id="lastname" type="text" class="form-control" name="lastname" v-model="student.lastname" placeholder="Please enter you lastname">

	                                <span class="help-block" v-if="student_error.lastname">
	                                        <strong>{{ student_error.lastname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label for="sex" class="col-md-4 control-label">Sex</label>

	                            <div class="col-md-6">
	                                <label class="radio-inline">
	                                    <input type="radio" name="sex" v-model="student.sex" id="inlineRadio1" value="Male"> Male
	                                </label>
	                                <label class="radio-inline">
	                                    <input type="radio" name="sex" v-model="student.sex" id="inlineRadio2" value="Female"> Female
	                                </label>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label for="name" class="col-md-4 control-label">Status</label>

	                            <div class="col-md-6">
	                                <select class="form-control" name="status" v-model="student.status">
	                                    <option>Single</option>
	                                    <option>Married</option>
	                                </select>
	                            </div>
	                        </div>

	                        <div class="form-group " v-bind:class="{'has-error':student_error.birthdate}">
	                            <label for="birthdate" class="col-md-4 control-label">Birthdate</label>

	                            <div class="col-md-6">
	                                <input id="birthdate" type="date" class="form-control" name="birthdate" v-model="student.birthdate" >

	                                <span class="help-block" v-if="student_error.birthdate">
	                                        <strong>{{ student_error.birthdate[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                        	<label for="birthdate" class="col-md-4 control-label">Age</label>
	                        	<div class="col-md-6">
	                        		<input readonly="" id="age" type="text" v-model="student.age" class="form-control">
	                        	</div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.contact}">
	                            <label for="contact" class="col-md-4 control-label">Contact No.</label>

	                            <div class="col-md-6">
	                                <input id="contact" type="text" class="form-control" name="contact" v-model="student.contact" placeholder="Please enter your contact no.">
	                                
	                                <span class="help-block" v-if="student_error.contact">
	                                        <strong>{{ student_error.contact[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>


	                        <div class="form-group" v-bind:class="{'has-error':student_error.address}">
	                            <label for="address" class="col-md-4 control-label"> Address</label>

	                            <div class="col-md-6">
	                                <input id="address" type="address" class="form-control" name="address" v-model="student.address" placeholder="Please enter your current adress">

	                                
	                                <span class="help-block" v-if="student_error.address">
	                                        <strong>{{ student_error.address[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.email}">
	                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

	                            <div class="col-md-6">
	                                <input id="email" type="text" class="form-control" name="email" v-model="student.email" placeholder="Please enter your email address">
	                                
	                                <span class="help-block" v-if="student_error.email">
	                                        <strong>{{ student_error.email[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_firstname}">
	                            <label for="parent_firstname" class="col-md-4 control-label">Parent Firstname</label>

	                            <div class="col-md-6">
	                                <input id="parent_firstname" type="text" class="form-control" name="parent_firstname" v-model="student.parent_firstname" placeholder="Please enter your parent firstname">

	                                <span class="help-block" v-if="student_error.parent_firstname">
	                                        <strong>{{ student_error.parent_firstname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_lastname}">
	                            <label for="parent_lastname" class="col-md-4 control-label">Parent Lastname</label>

	                            <div class="col-md-6">
	                                <input id="parent_lastname" type="text" class="form-control" name="parent_lastname" v-model="student.parent_lastname" placeholder="Please enter your parent lastname">

	                                <span class="help-block" v-if="student_error.parent_lastname">
	                                        <strong>{{ student_error.parent_lastname[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_contact}">
	                            <label for="parent_contact" class="col-md-4 control-label">Parent Contact No.</label>

	                            <div class="col-md-6">
	                                <input id="parent_contact" type="text" class="form-control" name="parent_contact" v-model="student.parent_contact" placeholder="Please enter your parent contact no.">

	                                <span class="help-block" v-if="student_error.parent_contact">
	                                        <strong>{{ student_error.parent_contact[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="form-group" v-bind:class="{'has-error':student_error.parent_address}">
	                            <label for="parent_address" class="col-md-4 control-label">Parent Address</label>

	                            <div class="col-md-6">
	                                <input id="parent_address" type="text" class="form-control" name="parent_address" v-model="student.parent_address" placeholder="Please enter your parent address">

	                                <span class="help-block" v-if="student_error.parent_address">
	                                        <strong>{{ student_error.parent_address[0] }}</strong>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                        	<div class="col-md-6 col-md-offset-4">
	                        		<button class="btn btn-primary" type="button" @click="updateInfo()">
	                        			Save Changes
	                        		</button>
	                        		<button class="btn btn-default" type="button" @click="discardChanges()">
	                        			Discard all changes
	                        		</button>
	                        	</div>
	                        </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo e(asset('js/student/profile.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var dob = new Date($( "#birthdate" ).val());
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        if (age==""||isNaN(age)||age<0) {
            $('#age').val('');
        }else{
            $('#age').val(age);
        }
        $( "#birthdate" ).change(function() {
            var dob = new Date($(this).val());
            var today = new Date();
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
            if (age==""||isNaN(age)||age<0) {
                $('#age').val('');
            }else{
                $('#age').val(age);
            }
            
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>