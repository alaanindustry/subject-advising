<?php $__env->startSection('content'); ?>
<div id="page-deanslist-component">
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					Deans Lister
				</div>
				<div class="panel-body">
					<form class="form-inline">
						<div class="form-group">
						    <label class="control-label" for="exampleInputEmail3">Batch Year</label>
						    <select class="form-control" v-model="batch_id" @change="getDeansList">
						    	<option v-bind:value="2014">2014-2015</option>
							    <option v-bind:value="2015">2015-2016</option>
						    	<option v-bind:value="2016">2016-2017</option>
						    	<option v-bind:value="2017">2017-2018</option>
						    	<option v-bind:value="2018">2018-2019</option>
						    	<option v-bind:value="2019">2019-2020</option>
						    </select>
						</div>
						<div class="form-group">
						    <label class="control-label" for="exampleInputPassword3">Semester</label>
						    <select class="form-control" v-model="sem_id" @change="getDeansList">
						    	<option value="1">First Semester</option>
						    	<option value="2">Second Semester</option>
						    </select>
						</div>
					</form>
					<hr>
					<div class="panel-body">
		    			<table class="table table-responsive">
				    		<thead>
				    			<th>Student ID</th>
				    			<th>Name</th>
				    			<th>Major</th>
				    			<th>Year</th>
				    			<th>Semester</th>
				    			<th>GPA</th>
				    			<th>Rank</th>
				    		</thead>
				    		<tbody>
				    			<tr v-for="deanslister in deansListers.first">
				    				<td>{{deanslister.school_id}}</td>
				    				<td>{{deanslister.firstname+' '+deanslister.middlename+' '+deanslister.lastname}}</td>
				    				<td>{{deanslister.name}}</td>
				    				<td>{{deanslister.year}}</td>
				    				<td>{{deanslister.sem}}</td>
				    				<td>{{deanslister.gpa}}</td>
				    				<td>{{"First Honor"}}</td>
				    			</tr>
				    			<tr v-for="deanslister in deansListers.second">
				    				<td>{{deanslister.school_id}}</td>
				    				<td>{{deanslister.firstname+' '+deanslister.middlename+' '+deanslister.lastname}}</td>
				    				<td>{{deanslister.name}}</td>
				    				<td>{{deanslister.year}}</td>
				    				<td>{{deanslister.sem}}</td>
				    				<td>{{deanslister.gpa}}</td>
				    				<td>{{"Second Honor"}}</td>
				    			</tr>
				    		</tbody>
				    	</table>
		    		</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo e(asset('js/student/deanslist.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>